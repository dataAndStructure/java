package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class SpecialIndexTest {

    private final SpecialIndex specialIndex = new SpecialIndex();

    @Test
    void test1() {
        Assert.assertEquals(specialIndex.solve(new ArrayList<>(Arrays.asList(2, 1, 6, 4))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(specialIndex.solve(new ArrayList<>(Arrays.asList(1, 1, 1))), 3);
    }

}