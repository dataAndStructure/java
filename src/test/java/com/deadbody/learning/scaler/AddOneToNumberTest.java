package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class AddOneToNumberTest {

    private final AddOneToNumber addOneToNumber = new AddOneToNumber();

    @Test
    void test1() {
        Assert.assertEquals(addOneToNumber.plusOne(new ArrayList<>(List.of(1, 2, 3))), new ArrayList<>(List.of(1, 2, 4)));
    }

}