package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ElementsRemovalTest {
    private final ElementsRemoval elementsRemoval = new ElementsRemoval();

    @Test
    void test1() {
        Assert.assertEquals(elementsRemoval.solve(new ArrayList<>(List.of(2, 1))), 4);
    }

    @Test
    void test2() {
        Assert.assertEquals(elementsRemoval.solve(new ArrayList<>(List.of(5))), 5);
    }
}