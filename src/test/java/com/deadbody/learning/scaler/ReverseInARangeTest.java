package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ReverseInARangeTest {
    private final ReverseInARange reverseInARange = new ReverseInARange();

    @Test
    void test1() {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1, 2, 4, 3)),
                reverseInARange.solve(new ArrayList<>(Arrays.asList(1, 2, 3, 4)), 2, 3));
    }

    @Test
    void test2() {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(6, 5, 2)),
                reverseInARange.solve(new ArrayList<>(Arrays.asList(2, 5, 6)), 0, 2));
    }
}