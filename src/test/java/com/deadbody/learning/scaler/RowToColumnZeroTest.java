package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RowToColumnZeroTest {

    private final RowToColumnZero rowToColumnZero = new RowToColumnZero();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3, 4)));
        input.add(new ArrayList<>(List.of(5, 6, 7, 0)));
        input.add(new ArrayList<>(List.of(9, 2, 0, 4)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 2, 0, 0)));
        output.add(new ArrayList<>(List.of(0, 0, 0, 0)));
        output.add(new ArrayList<>(List.of(0, 0, 0, 0)));
        Assert.assertEquals(rowToColumnZero.solve(input), output);
    }

}