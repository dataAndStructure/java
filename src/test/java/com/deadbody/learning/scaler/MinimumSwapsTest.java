package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MinimumSwapsTest {

    private final MinimumSwaps minimumSwaps = new MinimumSwaps();

    @Test
    void test1() {
        Assert.assertEquals(minimumSwaps.solve(new ArrayList<>(List.of(1, 12, 10, 3, 14, 10, 5)), 8), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(minimumSwaps.solve(new ArrayList<>(List.of(5, 17, 100, 11)), 20), 1);
    }

}