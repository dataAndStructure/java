package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class NextPermutationTest {

    private final NextPermutation nextPermutation = new NextPermutation();

    @Test
    void test1() {
        Assert.assertEquals(nextPermutation.nextPermutation(new ArrayList<>(List.of(1, 2, 3))),
                new ArrayList<>(List.of(1, 3, 2)));
    }

    @Test
    void test2() {
        Assert.assertEquals(nextPermutation.nextPermutation(new ArrayList<>(List.of(3, 2, 1))),
                new ArrayList<>(List.of(1, 2, 3)));
    }

}