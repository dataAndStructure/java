package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ReverseTheStringTest {

    private final ReverseTheString reverseTheString = new ReverseTheString();

    @Test
    void test1() {
        Assert.assertEquals(reverseTheString.solve("the sky is blue"), "blue is sky the");
    }

    @Test
    void test2() {
        Assert.assertEquals(reverseTheString.solve("this is ib"), "ib is this");
    }

}