package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ToggleCaseTest {

    private final ToggleCase toggleCase = new ToggleCase();

    @Test
    void test1() {
        Assert.assertEquals(toggleCase.solve("Hello"), "hELLO");
    }

    @Test
    void test2() {
        Assert.assertEquals(toggleCase.solve("tHiSiSaStRiNg"), "ThIsIsAsTrInG");
    }

}