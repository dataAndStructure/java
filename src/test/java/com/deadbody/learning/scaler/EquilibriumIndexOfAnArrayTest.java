package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class EquilibriumIndexOfAnArrayTest {

    private final EquilibriumIndexOfAnArray equilibriumIndexOfAnArray = new EquilibriumIndexOfAnArray();

    @Test
    void test1() {
        Assert.assertEquals(equilibriumIndexOfAnArray.solve(new ArrayList<>(Arrays.asList(-7, 1, 5, 2, -4, 3, 0))), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(equilibriumIndexOfAnArray.solve(new ArrayList<>(Arrays.asList(1, 2, 3))), -1);
    }

}