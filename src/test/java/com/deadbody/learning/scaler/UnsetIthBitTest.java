package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UnsetIthBitTest {

    private final UnsetIthBit unsetIthBit = new UnsetIthBit();

    @Test
    void test1() {
        Assert.assertEquals(unsetIthBit.solve(4, 1), 4);
    }

    @Test
    void test2() {
        Assert.assertEquals(unsetIthBit.solve(5, 2), 1);
    }

}