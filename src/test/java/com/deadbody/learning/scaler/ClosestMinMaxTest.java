package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ClosestMinMaxTest {

    private final ClosestMinMax closestMinMax = new ClosestMinMax();

    @Test
    void test1() {
        Assert.assertEquals(closestMinMax.solve(new ArrayList<>(Arrays.asList(1, 3, 2))), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(closestMinMax.solve(new ArrayList<>(Arrays.asList(2, 6, 1, 6, 9))), 3);
    }

}