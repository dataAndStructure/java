package com.deadbody.learning.scaler.prime.numbers;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class PrimeSumTest {

    private final PrimeSum primeSum = new PrimeSum();

    @Test
    void test1() {
        Assert.assertEquals(primeSum.primesum(4), new ArrayList<>(List.of(2, 2)));
    }


    @Test
    void test2() {
        Assert.assertEquals(primeSum.primesum(100), new ArrayList<>(List.of(3, 97)));
    }

    @Test
    void test3() {
        Assert.assertEquals(primeSum.primesum(1048574), new ArrayList<>(List.of(3, 1048571)));
    }

}