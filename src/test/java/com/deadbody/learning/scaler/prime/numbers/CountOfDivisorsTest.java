package com.deadbody.learning.scaler.prime.numbers;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountOfDivisorsTest {

    private final CountOfDivisors countOfDivisors = new CountOfDivisors();

    @Test
    void test1() {
        Assert.assertEquals(countOfDivisors.solve(new ArrayList<>(List.of(2, 3, 4, 5))), new ArrayList<>(List.of(2, 2
                , 3, 2)));
    }


    @Test
    void test2() {
        Assert.assertEquals(countOfDivisors.solve(new ArrayList<>(List.of(8, 9, 10))), new ArrayList<>(List.of(4, 3,
                4)));
    }

}