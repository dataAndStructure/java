package com.deadbody.learning.scaler.prime.numbers;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LuckyNumbersTest {

    private final LuckyNumbers luckyNumbers = new LuckyNumbers();

    @Test
    void test1() {
        Assert.assertEquals(luckyNumbers.solve(8), 1);
    }


    @Test
    void test2() {
        Assert.assertEquals(luckyNumbers.solve(12), 3);
    }

}