package com.deadbody.learning.scaler.prime.numbers;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FindAllPrimesTest {

    private final FindAllPrimes findAllPrimes = new FindAllPrimes();

    @Test
    void test1() {
        Assert.assertEquals(findAllPrimes.solve(7), new ArrayList<>(List.of(2, 3, 5, 7)));
    }


    @Test
    void test2() {
        Assert.assertEquals(findAllPrimes.solve(12), new ArrayList<>(List.of(2, 3, 5, 7, 11)));
    }

}