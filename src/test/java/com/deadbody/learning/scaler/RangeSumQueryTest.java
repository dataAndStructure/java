package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class RangeSumQueryTest {

    private final RangeSumQuery rangeSumQuery = new RangeSumQuery();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> queries = new ArrayList<>();
        ArrayList<Integer> query1 = new ArrayList<>(Arrays.asList(0, 3));
        queries.add(query1);
        ArrayList<Integer> query2 = new ArrayList<>(Arrays.asList(1, 2));
        queries.add(query2);
        Assert.assertEquals(rangeSumQuery.rangeSum(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5)), queries),
                new ArrayList<>(Arrays.asList(10L, 5L)));
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> queries = new ArrayList<>();
        ArrayList<Integer> query1 = new ArrayList<>(Arrays.asList(0, 0));
        queries.add(query1);
        ArrayList<Integer> query2 = new ArrayList<>(Arrays.asList(1, 2));
        queries.add(query2);
        Assert.assertEquals(rangeSumQuery.rangeSum(new ArrayList<>(Arrays.asList(2, 2, 2)), queries),
                new ArrayList<>(Arrays.asList(2L, 4L)));
    }

}