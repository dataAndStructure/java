package com.deadbody.learning.scaler;

import com.deadbody.learning.util.Interval;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class MergeIntervalsTest {

    private final MergeIntervals mergeIntervals = new MergeIntervals();

    @Test
    void test1() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(6, 9));
        Interval newInterval = new Interval(2, 5);
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 5));
        output.add(new Interval(6, 9));
        Assert.assertEquals(mergeIntervals.insert(intervals, newInterval), output);
    }

    @Test
    void test2() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(6, 9));
        Interval newInterval = new Interval(2, 6);
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 9));
        Assert.assertEquals(mergeIntervals.insert(intervals, newInterval), output);
    }

    @Test
    void test3() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(4, 7));
        intervals.add(new Interval(10, 14));
        intervals.add(new Interval(16, 19));
        intervals.add(new Interval(21, 24));
        intervals.add(new Interval(27, 30));
        intervals.add(new Interval(32, 35));
        intervals.add(new Interval(38, 41));
        intervals.add(new Interval(43, 50));
        Interval newInterval = new Interval(12, 22);
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 3));
        output.add(new Interval(4, 7));
        output.add(new Interval(10, 24));
        output.add(new Interval(27, 30));
        output.add(new Interval(32, 35));
        output.add(new Interval(38, 41));
        output.add(new Interval(43, 50));
        Assert.assertEquals(mergeIntervals.insert(intervals, newInterval), output);
    }

}