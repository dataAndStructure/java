package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NumberOfDigitOneTest {

    private final NumberOfDigitOne numberOfDigitOne = new NumberOfDigitOne();

    @Test
    void test1() {
        Assert.assertEquals(numberOfDigitOne.solve(926), 293);
    }

    @Test
    void test2() {
        Assert.assertEquals(numberOfDigitOne.solve(10), 2);
    }

    @Test
    void test3() {
        Assert.assertEquals(numberOfDigitOne.solve(11), 4);
    }

}