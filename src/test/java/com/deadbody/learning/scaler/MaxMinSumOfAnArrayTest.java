package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class MaxMinSumOfAnArrayTest {

    private final MaxMinSumOfAnArray maxMinSumOfAnArray = new MaxMinSumOfAnArray();

    @Test
    void test1() {
        Assert.assertEquals(1, maxMinSumOfAnArray.solve(new ArrayList<>(Arrays.asList(-2, 1, -4, 5, 3))));
    }

    @Test
    void test2() {
        Assert.assertEquals(5, maxMinSumOfAnArray.solve(new ArrayList<>(Arrays.asList(1, 3, 4, 1))));
    }

}