package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CountOfPrimesTest {

    private final CountOfPrimes countOfPrimes = new CountOfPrimes();

    @Test
    void test1() {
        Assert.assertEquals(countOfPrimes.solve(19), 8);
    }

    @Test
    void test2() {
        Assert.assertEquals(countOfPrimes.solve(1), 0);
    }

}