package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NumberOf1BitsTest {

    private final NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
    private final HelpFromSam helpFromSam = new HelpFromSam();

    @Test
    void test1() {
        Assert.assertEquals(numberOf1Bits.numSetBits(11), 3);
        Assert.assertEquals(helpFromSam.solve(11), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(numberOf1Bits.numSetBits(6), 2);
        Assert.assertEquals(helpFromSam.solve(6), 2);
    }

    @Test
    void test3() {
        Assert.assertEquals(numberOf1Bits.numSetBits(5), 2);
        Assert.assertEquals(helpFromSam.solve(5), 2);
    }

    @Test
    void test4() {
        Assert.assertEquals(numberOf1Bits.numSetBits(3), 2);
        Assert.assertEquals(helpFromSam.solve(3), 2);
    }

}