package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AddBinaryStringTest {

    private final AddBinaryString addBinaryString = new AddBinaryString();

    @Test
    void test1() {
        Assert.assertEquals(addBinaryString.addBinary("100", "11"), "111");
    }


    @Test
    void test2() {
        Assert.assertEquals(addBinaryString.addBinary("110", "10"), "1000");
    }

}