package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SingleNumberIITest {

    private final SingleNumberII singleNumberII = new SingleNumberII();

    @Test
    void test1() {
        Assert.assertEquals(singleNumberII.singleNumber(List.of(1, 2, 4, 3, 3, 2, 2, 3, 1, 1)), 4);
    }

    @Test
    void test2() {
        Assert.assertEquals(singleNumberII.singleNumber(List.of(0, 0, 0, 1)), 1);
    }

}