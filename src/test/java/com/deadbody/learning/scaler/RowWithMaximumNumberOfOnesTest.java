package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RowWithMaximumNumberOfOnesTest {

    private final RowWithMaximumNumberOfOnes rowWithMaximumNumberOfOnes = new RowWithMaximumNumberOfOnes();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(0, 1, 1)));
        input.add(new ArrayList<>(List.of(0, 0, 1)));
        input.add(new ArrayList<>(List.of(0, 1, 1)));
        Assert.assertEquals(rowWithMaximumNumberOfOnes.solve(input), 0);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(0, 0, 0, 0)));
        input.add(new ArrayList<>(List.of(0, 0, 0, 1)));
        input.add(new ArrayList<>(List.of(0, 0, 1, 1)));
        input.add(new ArrayList<>(List.of(0, 1, 1, 1)));
        Assert.assertEquals(rowWithMaximumNumberOfOnes.solve(input), 3);
    }

}