package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TimeToEqualityTest {

    private final TimeToEquality timeToEquality = new TimeToEquality();

    @Test
    void test1() {
        Assert.assertEquals(8, timeToEquality.solve(new ArrayList<>(Arrays.asList(2, 4, 1, 3, 2))));
    }

}