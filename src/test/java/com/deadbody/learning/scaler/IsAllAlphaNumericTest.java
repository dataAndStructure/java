package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class IsAllAlphaNumericTest {
    private final IsAllAlphaNumeric isAllAlphaNumeric = new IsAllAlphaNumeric();

    @Test
    void test1() {
        ArrayList<Character> input = new ArrayList<>(List.of('S', 'c', 'a', 'l', 'e', 'r', 'A', 'c', 'a', 'd', 'e', 'm', 'y', '2', '0', '2', '0'));
        Assert.assertEquals(isAllAlphaNumeric.solve(input), 1);
    }

    @Test
    void test2() {
        ArrayList<Character> input = new ArrayList<>(List.of('S', 'c', 'a', 'l', 'e', 'r', '#', '2', '0', '2', '0'));
        Assert.assertEquals(isAllAlphaNumeric.solve(input), 0);
    }

}