package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SpecialSubsequencesAGTest {

    private final SpecialSubsequencesAG specialSubsequencesAG = new SpecialSubsequencesAG();

    @Test
    void test1() {
        Assert.assertEquals(specialSubsequencesAG.solve("ABCGAG"), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(specialSubsequencesAG.solve("GAB"), 0);
    }

}