package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LengthOfLongestConsecutiveOnesTest {

    private final LengthOfLongestConsecutiveOnes lengthOfLongestConsecutiveOnes = new LengthOfLongestConsecutiveOnes();

    @Test
    void test1() {
        Assert.assertEquals(lengthOfLongestConsecutiveOnes.solve("11010011011011"), 5);
    }


    @Test
    void test2() {
        Assert.assertEquals(lengthOfLongestConsecutiveOnes.solve("111000"), 3);
    }


    @Test
    void test3() {
        Assert.assertEquals(lengthOfLongestConsecutiveOnes.solve("111011101"), 7);
    }

    @Test
    void test4() {
        Assert.assertEquals(lengthOfLongestConsecutiveOnes.solve("111"), 3);
    }

    @Test
    void test5() {
        Assert.assertEquals(lengthOfLongestConsecutiveOnes.solve("000"), 0);
    }

}