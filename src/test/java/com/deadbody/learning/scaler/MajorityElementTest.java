package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MajorityElementTest {

    private final MajorityElement majorityElement = new MajorityElement();

    @Test
    void test1() {
        Assert.assertEquals(majorityElement.majorityElement(new ArrayList<>(List.of(2, 1, 2))), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(majorityElement.majorityElement(new ArrayList<>(List.of(1, 1, 1))), 1);
    }

}