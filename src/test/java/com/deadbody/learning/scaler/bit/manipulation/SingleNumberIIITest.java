package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SingleNumberIIITest {

    private final SingleNumberIII singleNumberIII = new SingleNumberIII();

    @Test
    void test1() {
        Assert.assertEquals(singleNumberIII.solve(new ArrayList<>(List.of(1, 2, 3, 1, 2, 4))),
                new ArrayList<>(List.of(3, 4)));
    }


    @Test
    void test2() {
        Assert.assertEquals(singleNumberIII.solve(new ArrayList<>(List.of(1, 2))),
                new ArrayList<>(List.of(1, 2)));
    }

}