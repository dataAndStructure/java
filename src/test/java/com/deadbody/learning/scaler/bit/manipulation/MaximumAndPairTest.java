package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MaximumAndPairTest {

    private final MaximumAndPair maximumAndPair = new MaximumAndPair();

    @Test
    void test1() {
        Assert.assertEquals(maximumAndPair.solve(new ArrayList<>(List.of(53, 39, 88))), 37);
    }

    @Test
    void test2() {
        Assert.assertEquals(maximumAndPair.solve(new ArrayList<>(List.of(38, 44, 84, 12))), 36);
    }

    @Test
    void test3() {
        Assert.assertEquals(maximumAndPair.solve(new ArrayList<>(List.of(13, 18, 23, 56, 81, 20, 4, 24, 93))), 81);
    }

}