package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StrangeEqualityTest {

    private final StrangeEquality strangeEquality = new StrangeEquality();

    @Test
    void test1() {
        Assert.assertEquals(strangeEquality.solve(5), 10);
    }

}