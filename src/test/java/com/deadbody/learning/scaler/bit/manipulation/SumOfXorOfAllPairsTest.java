package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SumOfXorOfAllPairsTest {

    private final SumOfXorOfAllPairs sumOfXorOfAllPairs = new SumOfXorOfAllPairs();

    @Test
    void test1() {
        Assert.assertEquals(sumOfXorOfAllPairs.solve(new ArrayList<>(List.of(1, 2, 3))), 6);
    }

    @Test
    void test2() {
        Assert.assertEquals(sumOfXorOfAllPairs.solve(new ArrayList<>(List.of(3, 4, 2))), 14);
    }

}