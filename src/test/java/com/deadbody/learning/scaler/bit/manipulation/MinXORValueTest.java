package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MinXORValueTest {

    private final MinXORValue minXORValue = new MinXORValue();

    @Test
    void test1() {
        Assert.assertEquals(minXORValue.findMinXor(new ArrayList<>(List.of(0, 2, 5, 7))), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(minXORValue.findMinXor(new ArrayList<>(List.of(0, 4, 7, 9))), 3);
    }

}