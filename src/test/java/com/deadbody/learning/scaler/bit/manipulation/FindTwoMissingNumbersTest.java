package com.deadbody.learning.scaler.bit.manipulation;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FindTwoMissingNumbersTest {

    private final FindTwoMissingNumbers findTwoMissingNumbers = new FindTwoMissingNumbers();

    @Test
    void test1() {
        Assert.assertEquals(findTwoMissingNumbers.solve(new ArrayList<>(List.of(3, 2, 4))), new ArrayList<>(List.of(1
                , 5)));
    }

    @Test
    void test2() {
        Assert.assertEquals(findTwoMissingNumbers.solve(new ArrayList<>(List.of(5, 1, 3, 6))),
                new ArrayList<>(List.of(2, 4)));
    }

}