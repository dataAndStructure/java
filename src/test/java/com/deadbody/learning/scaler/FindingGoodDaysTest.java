package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindingGoodDaysTest {

    private final FindingGoodDays findingGoodDays = new FindingGoodDays();

    @Test
    void test1() {
        Assert.assertEquals(findingGoodDays.solve(5), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(findingGoodDays.solve(8), 1);
    }

}