package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StringOperationsTest {

    private final StringOperations stringOperations = new StringOperations();

    @Test
    void test1() {
        Assert.assertEquals(stringOperations.solve("aeiOUz"), "###z###z");
    }

    @Test
    void test2() {
        Assert.assertEquals(stringOperations.solve("AbcaZeoB"), "bc###bc###");
    }

}