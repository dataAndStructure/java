package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountSubarrayZeroSumTest {

    private final CountSubarrayZeroSum countSubarrayZeroSum = new CountSubarrayZeroSum();
    private final com.deadbody.learning.scaler.hashing1.CountSubarrayZeroSum c =
            new com.deadbody.learning.scaler.hashing1.CountSubarrayZeroSum();

    @Test
    void test1() {
        ArrayList<Integer> input = new ArrayList<>(List.of(30, -30, 30, -30));
        Assert.assertEquals(countSubarrayZeroSum.solve(input), 4);
        Assert.assertEquals(c.solve(input), 4);
    }

    @Test
    void test2() {
        ArrayList<Integer> input = new ArrayList<>(List.of(1, -1, -2, 2));
        Assert.assertEquals(countSubarrayZeroSum.solve(input), 3);
        Assert.assertEquals(c.solve(input), 3);
    }

    @Test
    void test3() {
        ArrayList<Integer> input = new ArrayList<>(List.of(-1, 2, -1));
        Assert.assertEquals(countSubarrayZeroSum.solve(input), 1);
        Assert.assertEquals(c.solve(input), 1);
    }

}