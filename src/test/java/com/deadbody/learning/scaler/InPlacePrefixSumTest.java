package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class InPlacePrefixSumTest {

    private final InPlacePrefixSum inPlacePrefixSum = new InPlacePrefixSum();

    @Test
    void test1() {
        Assert.assertEquals(inPlacePrefixSum.solve(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5))), new ArrayList<>(Arrays.asList(1, 3, 6, 10, 15)));
    }

    @Test
    void test2() {
        Assert.assertEquals(inPlacePrefixSum.solve(new ArrayList<>(Arrays.asList(4, 3, 2))), new ArrayList<>(Arrays.asList(4, 7, 9)));
    }

}