package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FirstMissingIntegerTest {

    private final FirstMissingInteger firstMissingInteger = new FirstMissingInteger();

    @Test
    void test1() {
        Assert.assertEquals(firstMissingInteger.firstMissingPositive(new ArrayList<>(List.of(1, 2, 0))), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(firstMissingInteger.firstMissingPositive(new ArrayList<>(List.of(3, 4, -1, 1))), 2);
    }

    @Test
    void test3() {
        Assert.assertEquals(firstMissingInteger.firstMissingPositive(new ArrayList<>(List.of(-8, -7, -6))), 1);
    }

}