package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FirstRepeatingElementTest {

    private final FirstRepeatingElement firstRepeatingElement = new FirstRepeatingElement();

    @Test
    void test1() {
        ArrayList<Integer> input = new ArrayList<>(List.of(8, 15, 1, 10, 5, 19, 19, 3, 5, 6, 6, 2, 8, 2, 12, 16, 3));
        Assert.assertEquals(firstRepeatingElement.solve(input), 8);
    }
}