package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SpiralOrderMatrixIITest {

    private final SpiralOrderMatrixII spiralOrderMatrixII = new SpiralOrderMatrixII();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1)));
        Assert.assertEquals(spiralOrderMatrixII.generateMatrix(1), output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 2)));
        output.add(new ArrayList<>(List.of(4, 3)));
        Assert.assertEquals(spiralOrderMatrixII.generateMatrix(2), output);
    }

    @Test
    void test3() {
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 2, 3, 4, 5)));
        output.add(new ArrayList<>(List.of(16, 17, 18, 19, 6)));
        output.add(new ArrayList<>(List.of(15, 24, 25, 20, 7)));
        output.add(new ArrayList<>(List.of(14, 23, 22, 21, 8)));
        output.add(new ArrayList<>(List.of(13, 12, 11, 10, 9)));
        Assert.assertEquals(spiralOrderMatrixII.generateMatrix(5), output);
    }

}