package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ToggleIthBitTest {

    private final ToggleIthBit toggleIthBit = new ToggleIthBit();

    @Test
    void test1() {
        Assert.assertEquals(toggleIthBit.solve(4, 1), 6);
    }

    @Test
    void test2() {
        Assert.assertEquals(toggleIthBit.solve(5, 2), 1);
    }

}