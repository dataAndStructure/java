package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SetBitTest {

    private final SetBit setBit = new SetBit();

    @Test
    void test1() {
        Assert.assertEquals(setBit.solve(3, 5), 40);
    }

    @Test
    void test2() {
        Assert.assertEquals(setBit.solve(4, 4), 16);
    }

}