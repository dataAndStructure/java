package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LongestCommonPrefixTest {

    private final LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();

    @Test
    void test1() {
        ArrayList<String> input = new ArrayList<>(List.of("abcdefgh", "aefghijk", "abcefgh"));
        Assert.assertEquals(longestCommonPrefix.longestCommonPrefix(input), "a");
    }


    @Test
    void test2() {
        ArrayList<String> input = new ArrayList<>(List.of("abab", "ab", "abcd"));
        Assert.assertEquals(longestCommonPrefix.longestCommonPrefix(input), "ab");
    }

}