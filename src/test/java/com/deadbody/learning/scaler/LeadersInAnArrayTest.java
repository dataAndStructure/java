package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LeadersInAnArrayTest {
    private final LeadersInAnArray leadersInAnArray = new LeadersInAnArray();

    @Test
    void test1() {
        ArrayList<Integer> input = leadersInAnArray.solve(new ArrayList<>(List.of(16, 17, 4, 3, 5, 2)));
        Collections.sort(input);
        ArrayList<Integer> output = new ArrayList<>(List.of(17, 2, 5));
        Collections.sort(output);
        Assert.assertEquals(input, output);
    }

    @Test
    void test2() {
        ArrayList<Integer> input = leadersInAnArray.solve(new ArrayList<>(List.of(5, 4)));
        Collections.sort(input);
        ArrayList<Integer> output = new ArrayList<>(List.of(5, 4));
        Collections.sort(output);
        Assert.assertEquals(input, output);
    }
}