package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SingleNumberTest {

    private final SingleNumber singleNumber = new SingleNumber();

    @Test
    void test1() {
        Assert.assertEquals(singleNumber.singleNumber(List.of(1, 2, 2, 3, 1)), 3);
    }


    @Test
    void test2() {
        Assert.assertEquals(singleNumber.singleNumber(List.of(1, 2, 2)), 1);
    }

}