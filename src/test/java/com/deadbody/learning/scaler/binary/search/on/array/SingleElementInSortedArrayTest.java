package com.deadbody.learning.scaler.binary.search.on.array;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SingleElementInSortedArrayTest {

    private final SingleElementInSortedArray singleElementInSortedArray = new SingleElementInSortedArray();

    @Test
    void test1() {
        Assert.assertEquals(singleElementInSortedArray.solve(new int[]{1, 1, 7}), 7);
    }

    @Test
    void test2() {
        Assert.assertEquals(singleElementInSortedArray.solve(new int[]{2, 3, 3}), 2);
    }

}