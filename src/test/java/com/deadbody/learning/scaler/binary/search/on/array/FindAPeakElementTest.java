package com.deadbody.learning.scaler.binary.search.on.array;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindAPeakElementTest {

    private final FindAPeakElement findAPeakElement = new FindAPeakElement();

    @Test
    void test1() {
        Assert.assertEquals(findAPeakElement.solve(new int[]{1, 2, 3, 4, 5}), 5);
    }

    @Test
    void test2() {
        Assert.assertEquals(findAPeakElement.solve(new int[]{5, 17, 100, 11}), 100);
    }

    @Test
    void test3() {
        Assert.assertEquals(findAPeakElement.solve(new int[]{1, 10, 10}), 10);
    }

}