package com.deadbody.learning.scaler.binary.search.on.array;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SortedInsertPositionTest {

    private final SortedInsertPosition sortedInsertPosition = new SortedInsertPosition();

    @Test
    void test1() {
        Assert.assertEquals(sortedInsertPosition.searchInsert(new int[]{1, 3, 5, 6}, 5), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(sortedInsertPosition.searchInsert(new int[]{1, 4, 9}, 3), 1);
    }

}