package com.deadbody.learning.scaler.binary.search.on.array;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchForARangeTest {

    private final SearchForARange searchForARange = new SearchForARange();

    @Test
    void test1() {
        Assert.assertEquals(searchForARange.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8), new int[]{3, 4});
    }

    @Test
    void test2() {
        Assert.assertEquals(searchForARange.searchRange(new int[]{5, 17, 100, 111}, 3), new int[]{-1, -1});
    }


    @Test
    void test3() {
        Assert.assertEquals(searchForARange.searchRange(new int[]{5, 7, 7, 7, 7, 7, 7, 7, 7, 7, 9}, 7), new int[]{1,
                9});
    }

}