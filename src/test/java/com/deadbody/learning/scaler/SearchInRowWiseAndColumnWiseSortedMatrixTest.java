package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SearchInRowWiseAndColumnWiseSortedMatrixTest {

    private final SearchInRowWiseAndColumnWiseSortedMatrix searchInRowWiseAndColumnWiseSortedMatrix = new SearchInRowWiseAndColumnWiseSortedMatrix();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3)));
        input.add(new ArrayList<>(List.of(4, 5, 6)));
        input.add(new ArrayList<>(List.of(7, 8, 9)));
        Assert.assertEquals(searchInRowWiseAndColumnWiseSortedMatrix.solve(input, 2), 1011);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2)));
        input.add(new ArrayList<>(List.of(3, 3)));
        Assert.assertEquals(searchInRowWiseAndColumnWiseSortedMatrix.solve(input, 3), 2019);
    }

}