package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class AntiDiagonalsTest {

    private final AntiDiagonals antiDiagonals = new AntiDiagonals();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2)));
        input.add(new ArrayList<>(List.of(3, 4)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 0)));
        output.add(new ArrayList<>(List.of(2, 3)));
        output.add(new ArrayList<>(List.of(4, 0)));
        Assert.assertEquals(antiDiagonals.diagonal(input), output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3)));
        input.add(new ArrayList<>(List.of(4, 5, 6)));
        input.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 0, 0)));
        output.add(new ArrayList<>(List.of(2, 4, 0)));
        output.add(new ArrayList<>(List.of(3, 5, 7)));
        output.add(new ArrayList<>(List.of(6, 8, 0)));
        output.add(new ArrayList<>(List.of(9, 0, 0)));
        Assert.assertEquals(antiDiagonals.diagonal(input), output);
    }

}