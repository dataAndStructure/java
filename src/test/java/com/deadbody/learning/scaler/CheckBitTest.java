package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckBitTest {

    private final CheckBit checkBit = new CheckBit();

    @Test
    void test1() {
        Assert.assertEquals(checkBit.solve(4, 1), 0);
    }

    @Test
    void test2() {
        Assert.assertEquals(checkBit.solve(5, 2), 1);
    }

}