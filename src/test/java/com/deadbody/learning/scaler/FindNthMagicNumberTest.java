package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindNthMagicNumberTest {

    private final FindNthMagicNumber findNthMagicNumber = new FindNthMagicNumber();

    @Test
    void test1() {
        Assert.assertEquals(findNthMagicNumber.solve(3), 30);
    }

    @Test
    void test2() {
        Assert.assertEquals(findNthMagicNumber.solve(10), 650);
    }

}