package com.deadbody.learning.scaler.count.sort.merge.sort;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountSortTest {
    private final CountSort countSort = new CountSort();

    @Test
    void test1() {
        Assert.assertEquals(countSort.solve(new ArrayList<>(List.of(1, 3, 1))), new ArrayList<>(List.of(1, 1, 3)));
    }

    @Test
    void test2() {
        Assert.assertEquals(countSort.solve(new ArrayList<>(List.of(4, 2, 1, 3))), new ArrayList<>(List.of(1, 2, 3,
                4)));
    }
}