package com.deadbody.learning.scaler.count.sort.merge.sort;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class
MinimumAbsoluteDifferenceTest {

    private final MinimumAbsoluteDifference minimumAbsoluteDifference = new MinimumAbsoluteDifference();

    @Test
    void test1() {
        Assert.assertEquals(minimumAbsoluteDifference.solve(new ArrayList<>(List.of(1, 2, 3, 4, 5))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(minimumAbsoluteDifference.solve(new ArrayList<>(List.of(5, 17, 100, 11))), 6);
    }
}