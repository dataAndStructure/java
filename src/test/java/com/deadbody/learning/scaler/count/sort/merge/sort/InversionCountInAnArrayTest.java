package com.deadbody.learning.scaler.count.sort.merge.sort;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class InversionCountInAnArrayTest {
    private final InversionCountInAnArray inversionCountInAnArray = new InversionCountInAnArray();

    @Test
    void test1() {
        Assert.assertEquals(inversionCountInAnArray.solve(new ArrayList<>(List.of(1, 3, 2))), 1);
    }


    @Test
    void test2() {
        Assert.assertEquals(inversionCountInAnArray.solve(new ArrayList<>(List.of(3, 4, 1, 2))), 4);
    }

    @Test
    void test3() {
        Assert.assertEquals(inversionCountInAnArray.solve(new ArrayList<>(List.of(6, 12, 10, 17, 10, 22, 9, 19, 21,
                31, 26, 8))), 21);
    }
}