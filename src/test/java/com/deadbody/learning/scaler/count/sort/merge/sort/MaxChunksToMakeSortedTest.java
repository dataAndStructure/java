package com.deadbody.learning.scaler.count.sort.merge.sort;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MaxChunksToMakeSortedTest {
    private final MaxChunksToMakeSorted maxChunksToMakeSorted = new MaxChunksToMakeSorted();

    @Test
    void test1() {
        Assert.assertEquals(maxChunksToMakeSorted.solve(new ArrayList<>(List.of(1, 2, 3, 4, 0))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(maxChunksToMakeSorted.solve(new ArrayList<>(List.of(2, 0, 1, 3))), 2);
    }
}