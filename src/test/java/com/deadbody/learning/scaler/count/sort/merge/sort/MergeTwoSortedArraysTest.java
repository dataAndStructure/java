package com.deadbody.learning.scaler.count.sort.merge.sort;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeTwoSortedArraysTest {

    private final MergeTwoSortedArrays mergeTwoSortedArrays = new MergeTwoSortedArrays();

    @Test
    void test1() {
        Assert.assertEquals(mergeTwoSortedArrays.solve(List.of(4, 7, 9), List.of(2, 11, 19)),
                new ArrayList<>(List.of(2, 4, 7, 9, 11, 19)));
    }

    @Test
    void test2() {
        Assert.assertEquals(mergeTwoSortedArrays.solve(List.of(1), List.of(2)), new ArrayList<>(List.of(1, 2)));
    }

}