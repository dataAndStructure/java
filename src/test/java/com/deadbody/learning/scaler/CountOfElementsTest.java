package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CountOfElementsTest {

    private final CountOfElements countOfElements = new CountOfElements();

    @Test
    void test1() {
        Assert.assertEquals(countOfElements.solve(new ArrayList<>(Arrays.asList(3, 1, 2))), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(countOfElements.solve(new ArrayList<>(Arrays.asList(5, 5, 3))), 1);
    }

}