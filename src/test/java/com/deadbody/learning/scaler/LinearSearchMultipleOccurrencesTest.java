package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class LinearSearchMultipleOccurrencesTest {

    private final LinearSearchMultipleOccurrences linearSearchMultipleOccurrences = new LinearSearchMultipleOccurrences();

    @Test
    void test1() {
        Assert.assertEquals(2,
                linearSearchMultipleOccurrences.solve(new ArrayList<>(Arrays.asList(1, 2, 2)), 2));
    }

    @Test
    void test2() {
        Assert.assertEquals(0,
                linearSearchMultipleOccurrences.solve(new ArrayList<>(Arrays.asList(1, 2, 1)), 3));
    }

}