package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class SubarrayInGivenRangeTest {

    private final SubarrayInGivenRange subarrayInGivenRange = new SubarrayInGivenRange();

    @Test
    void test1() {
        Assert.assertEquals(subarrayInGivenRange.solve(new ArrayList<>(Arrays.asList(4, 3, 2, 6)), 1, 3), new ArrayList<>(Arrays.asList(3, 2, 6)));
    }


    @Test
    void test2() {
        Assert.assertEquals(subarrayInGivenRange.solve(new ArrayList<>(Arrays.asList(4, 2, 2)), 0, 1), new ArrayList<>(Arrays.asList(4, 2)));
    }

}