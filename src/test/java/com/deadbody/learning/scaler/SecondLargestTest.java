package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondLargestTest {

    private final SecondLargest secondLargest = new SecondLargest();

    @Test
    void test1() {
        Assert.assertEquals(1, secondLargest.solve(new ArrayList<>(Arrays.asList(2, 1, 2))));
    }

    @Test
    void test2() {
        Assert.assertEquals(-1, secondLargest.solve(new ArrayList<>(List.of(2))));
    }

    @Test
    void test3() {
        Assert.assertEquals(4, secondLargest.solve(new ArrayList<>(Arrays.asList(5, 4, 3, 2, 1))));
    }

    @Test
    void test4() {
        Assert.assertEquals(4, secondLargest.solve(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5))));
    }

}