package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SortedPermutationRankWithRepeatsTest {

    private final SortedPermutationRank sortedPermutationRank = new SortedPermutationRank();

    @Test
    void test1() {
        Assert.assertEquals(sortedPermutationRank.findRank("aba"), 2);
    }


    @Test
    void test2() {
        Assert.assertEquals(sortedPermutationRank.findRank("bca"), 4);
    }

}