package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsecutiveNumbersSumTest {

    private final ConsecutiveNumbersSum consecutiveNumbersSum = new ConsecutiveNumbersSum();

    @Test
    void test1() {
        Assert.assertEquals(consecutiveNumbersSum.solve(5), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(consecutiveNumbersSum.solve(15), 4);
    }

}