package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExcelColumnTitleTest {

    private final ExcelColumnTitle excelColumnTitle = new ExcelColumnTitle();


    @Test
    void test1() {
        Assert.assertEquals(excelColumnTitle.convertToTitle(3), "C");
    }


    @Test
    void test2() {
        Assert.assertEquals(excelColumnTitle.convertToTitle(27), "AA");
    }


    @Test
    void test3() {
        Assert.assertEquals(excelColumnTitle.convertToTitle(28), "AB");
    }


    @Test
    void test4() {
        Assert.assertEquals(excelColumnTitle.convertToTitle(943566), "BAQTZ");
    }
}