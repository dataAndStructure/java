package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExcelColumnNumberTest {

    private final ExcelColumnNumber excelColumnNumber = new ExcelColumnNumber();

    @Test
    void test1() {
        Assert.assertEquals(excelColumnNumber.titleToNumber("ABC"), 731);
    }


    @Test
    void test2() {
        Assert.assertEquals(excelColumnNumber.titleToNumber("AB"), 28);
    }


    @Test
    void test3() {
        Assert.assertEquals(excelColumnNumber.titleToNumber("BB"), 54);
    }

}