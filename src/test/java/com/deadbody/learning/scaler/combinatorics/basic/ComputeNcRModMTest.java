package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ComputeNcRModMTest {

    private final ComputeNcRModM computeNcRModM = new ComputeNcRModM();

    @Test
    void test1() {
        Assert.assertEquals(computeNcRModM.solve(5, 2, 13), 10);
    }

    @Test
    void test2() {
        Assert.assertEquals(computeNcRModM.solve(6, 2, 13), 2);
    }

    @Test
    void test3() {
        Assert.assertEquals(computeNcRModM.solve(96, 21, 123), 0);
    }

}