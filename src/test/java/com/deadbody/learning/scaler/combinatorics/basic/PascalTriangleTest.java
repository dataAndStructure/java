package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangleTest {

    private final PascalTriangle pascalTriangle = new PascalTriangle();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        ans.add(new ArrayList<>(List.of(1, 0, 0)));
        ans.add(new ArrayList<>(List.of(1, 1, 0)));
        ans.add(new ArrayList<>(List.of(1, 2, 1)));
        Assert.assertEquals(pascalTriangle.solve(3), ans);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        ans.add(new ArrayList<>(List.of(1, 0, 0, 0, 0)));
        ans.add(new ArrayList<>(List.of(1, 1, 0, 0, 0)));
        ans.add(new ArrayList<>(List.of(1, 2, 1, 0, 0)));
        ans.add(new ArrayList<>(List.of(1, 3, 3, 1, 0)));
        ans.add(new ArrayList<>(List.of(1, 4, 6, 4, 1)));
        Assert.assertEquals(pascalTriangle.solve(5), ans);
    }

}