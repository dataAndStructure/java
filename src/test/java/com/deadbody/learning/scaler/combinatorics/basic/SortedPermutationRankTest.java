package com.deadbody.learning.scaler.combinatorics.basic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SortedPermutationRankTest {

    private final SortedPermutationRank sortedPermutationRank = new SortedPermutationRank();

    @Test
    void test1() {
        Assert.assertEquals(sortedPermutationRank.findRank("acb"), 2);
    }


    @Test
    void test2() {
        Assert.assertEquals(sortedPermutationRank.findRank("a"), 1);
    }


    @Test
    void test3() {
        Assert.assertEquals(sortedPermutationRank.findRank("view"), 15);
    }

}