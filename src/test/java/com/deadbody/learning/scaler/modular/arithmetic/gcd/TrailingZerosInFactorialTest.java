package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TrailingZerosInFactorialTest {

    private final TrailingZerosInFactorial trailingZerosInFactorial = new TrailingZerosInFactorial();

    @Test
    void test1() {
        Assert.assertEquals(trailingZerosInFactorial.trailingZeroes(5), 1);
    }


    @Test
    void test2() {
        Assert.assertEquals(trailingZerosInFactorial.trailingZeroes(6), 1);
    }

}