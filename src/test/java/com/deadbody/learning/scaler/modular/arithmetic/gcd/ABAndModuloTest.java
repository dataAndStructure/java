package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ABAndModuloTest {

    private final ABAndModulo abAndModulo = new ABAndModulo();

    @Test
    void test1() {
        Assert.assertEquals(abAndModulo.solve(1, 2), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(abAndModulo.solve(5, 10), 5);
    }

    @Test
    void test3() {
        Assert.assertEquals(abAndModulo.solve(6816621, 8157697), 1341076);
    }

}