package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class GreatestCommonDivisorTest {

    private final GreatestCommonDivisor greatestCommonDivisor = new GreatestCommonDivisor();

    @Test
    void test1() {
        Assert.assertEquals(greatestCommonDivisor.gcd(4, 6), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(greatestCommonDivisor.gcd(6, 7), 1);
    }

}