package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PrimeModuloInverseTest {

    private final PrimeModuloInverse primeModuloInverse = new PrimeModuloInverse();

    @Test
    void test1() {
        Assert.assertEquals(primeModuloInverse.solve(3, 5), 2);
    }


    @Test
    void test2() {
        Assert.assertEquals(primeModuloInverse.solve(6, 23), 4);
    }

    @Test
    void test3() {
        Assert.assertEquals(primeModuloInverse.solve(6, 25), 16);
    }

    @Test
    void test4() {
        Assert.assertEquals(primeModuloInverse.solve(12, 55557209), 32408372);
    }

}