package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class PairSumDivisibleByMTest {

    private final PairSumDivisibleByM pairSumDivisibleByM = new PairSumDivisibleByM();

    @Test
    void test1() {
        Assert.assertEquals(pairSumDivisibleByM.solve(new ArrayList<>(List.of(1, 2, 3, 4, 5)), 2), 4);
    }


    @Test
    void test2() {
        Assert.assertEquals(pairSumDivisibleByM.solve(new ArrayList<>(List.of(5, 17, 100, 11)), 28), 1);
    }

}