package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ModSumTest {

    private final ModSum modSum = new ModSum();

    @Test
    void test1() {
        Assert.assertEquals(modSum.solve(new ArrayList<>(List.of(1, 2, 3))), 5);
    }

    @Test
    void test2() {
        Assert.assertEquals(modSum.solve(new ArrayList<>(List.of(17, 100, 11))), 61);
    }

}