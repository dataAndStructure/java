package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LargestCoprimeDivisorTest {

    private final LargestCoprimeDivisor largestCoprimeDivisor = new LargestCoprimeDivisor();

    @Test
    void test1() {
        Assert.assertEquals(largestCoprimeDivisor.cpFact(30, 12), 5);
    }

    @Test
    void test2() {
        Assert.assertEquals(largestCoprimeDivisor.cpFact(5, 10), 1);
    }

    @Test
    void test3() {
        Assert.assertEquals(largestCoprimeDivisor.cpFact(22, 38), 11);
    }

    @Test
    void test4() {
        Assert.assertEquals(largestCoprimeDivisor.cpFact(2, 3), 2);
    }

    @Test
    void test5() {
        Assert.assertEquals(largestCoprimeDivisor.cpFact(24, 36), 1);
    }

}