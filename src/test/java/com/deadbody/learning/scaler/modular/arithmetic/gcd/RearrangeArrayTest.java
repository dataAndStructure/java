package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RearrangeArrayTest {

    private final RearrangeArray rearrangeArray = new RearrangeArray();

    @Test
    void test1() {
        ArrayList<Integer> input = new ArrayList<>(List.of(1, 0));
        rearrangeArray.arrange(input);
        Assert.assertEquals(input, new ArrayList<>(List.of(0, 1)));
    }

    @Test
    void test2() {
        ArrayList<Integer> input = new ArrayList<>(List.of(0, 2, 1, 3));
        rearrangeArray.arrange(input);
        Assert.assertEquals(input, new ArrayList<>(List.of(0, 1, 2, 3)));
    }

}