package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class DeleteOneTest {

    private final DeleteOne deleteOne = new DeleteOne();

    @Test
    void test1() {
        Assert.assertEquals(deleteOne.solve(new ArrayList<>(List.of(12, 15, 18))), 6);
    }

    @Test
    void test2() {
        Assert.assertEquals(deleteOne.solve(new ArrayList<>(List.of(5, 15, 30))), 15);
    }

}