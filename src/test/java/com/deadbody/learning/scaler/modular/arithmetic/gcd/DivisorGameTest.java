package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DivisorGameTest {

    private final DivisorGame divisorGame = new DivisorGame();

    @Test
    void test1() {
        Assert.assertEquals(divisorGame.solve(12, 3, 2), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(divisorGame.solve(6, 1, 4), 1);
    }

    @Test
    void test3() {
        Assert.assertEquals(divisorGame.solve(411753753, 1876, 7430), 59);
    }

}