package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VeryLargePowerTest {

    private final VeryLargePower veryLargePower = new VeryLargePower();

    @Test
    void test1() {
        Assert.assertEquals(veryLargePower.solve(1, 1), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(veryLargePower.solve(2, 2), 4);
    }


    @Test
    void test3() {
        Assert.assertEquals(veryLargePower.solve(2, 27), 666348826);
    }

}