package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UnsetXBitsFromRightTest {

    private final UnsetXBitsFromRight unsetXBitsFromRight = new UnsetXBitsFromRight();

    @Test
    void test1() {
        Assert.assertEquals(unsetXBitsFromRight.solve(25L, 3), 24L);
    }

    @Test
    void test2() {
        Assert.assertEquals(unsetXBitsFromRight.solve(37L, 3), 32L);
    }

}