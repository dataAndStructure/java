package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ContinuousSumQueryTest {

    private final ContinuousSumQuery continuousSumQuery = new ContinuousSumQuery();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> query = new ArrayList<>();
        query.add(new ArrayList<>(List.of(1, 2, 10)));
        query.add(new ArrayList<>(List.of(2, 3, 20)));
        query.add(new ArrayList<>(List.of(2, 5, 25)));
        Assert.assertEquals(continuousSumQuery.solve(5, query), new ArrayList<>(List.of(10, 55, 45, 25, 25)));
    }

}