package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ColorfulNumberTest {

    private final ColorfulNumber colorfulNumber = new ColorfulNumber();

    @Test
    void test1() {
        Assert.assertEquals(colorfulNumber.colorful(23), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(colorfulNumber.colorful(236), 0);
    }

}