package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class RainWaterTrappedTest {

    private final RainWaterTrapped rainWaterTrapped = new RainWaterTrapped();

    @Test
    void test1() {
        List<Integer> input = List.of(0, 1, 0, 2);
        Assert.assertEquals(rainWaterTrapped.trap(input), 1);
        Assert.assertEquals(rainWaterTrapped.twoPointerTrap(input), 1);
    }

    @Test
    void test2() {
        List<Integer> input = List.of(1, 2);
        Assert.assertEquals(rainWaterTrapped.trap(input), 0);
        Assert.assertEquals(rainWaterTrapped.twoPointerTrap(input), 0);
    }

}