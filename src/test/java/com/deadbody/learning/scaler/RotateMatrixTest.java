package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RotateMatrixTest {
    private final RotateMatrix rotateMatrix = new RotateMatrix();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2)));
        input.add(new ArrayList<>(List.of(3, 4)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(3, 1)));
        output.add(new ArrayList<>(List.of(4, 2)));
        rotateMatrix.solve(input);
        Assert.assertEquals(input, output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3)));
        input.add(new ArrayList<>(List.of(4, 5, 6)));
        input.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(7, 4, 1)));
        output.add(new ArrayList<>(List.of(8, 5, 2)));
        output.add(new ArrayList<>(List.of(9, 6, 3)));
        rotateMatrix.solve(input);
        Assert.assertEquals(input, output);
    }
}