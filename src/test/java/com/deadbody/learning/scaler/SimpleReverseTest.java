package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleReverseTest {
    private final SimpleReverse simpleReverse = new SimpleReverse();

    @Test
    void test1() {
        Assert.assertEquals(simpleReverse.solve("scaler"), "relacs");
    }

    @Test
    void test2() {
        Assert.assertEquals(simpleReverse.solve("academy"), "ymedaca");
    }
}