package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class AddTheMatricesTest {

    private final AddTheMatrices addTheMatrices = new AddTheMatrices();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> A = new ArrayList<>();
        A.add(new ArrayList<>(List.of(1, 2, 3)));
        A.add(new ArrayList<>(List.of(4, 5, 6)));
        A.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> B = new ArrayList<>();
        B.add(new ArrayList<>(List.of(9, 8, 7)));
        B.add(new ArrayList<>(List.of(6, 5, 4)));
        B.add(new ArrayList<>(List.of(3, 2, 1)));
        ArrayList<ArrayList<Integer>> C = new ArrayList<>();
        C.add(new ArrayList<>(List.of(10, 10, 10)));
        C.add(new ArrayList<>(List.of(10, 10, 10)));
        C.add(new ArrayList<>(List.of(10, 10, 10)));
        Assert.assertEquals(addTheMatrices.solve(A, B), C);
    }


    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> A = new ArrayList<>();
        A.add(new ArrayList<>(List.of(1, 2, 3)));
        A.add(new ArrayList<>(List.of(4, 1, 2)));
        A.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> B = new ArrayList<>();
        B.add(new ArrayList<>(List.of(9, 9, 7)));
        B.add(new ArrayList<>(List.of(1, 2, 4)));
        B.add(new ArrayList<>(List.of(4, 6, 3)));
        ArrayList<ArrayList<Integer>> C = new ArrayList<>();
        C.add(new ArrayList<>(List.of(10, 11, 10)));
        C.add(new ArrayList<>(List.of(5, 3, 6)));
        C.add(new ArrayList<>(List.of(11, 14, 12)));
        Assert.assertEquals(addTheMatrices.solve(A, B), C);
    }

}