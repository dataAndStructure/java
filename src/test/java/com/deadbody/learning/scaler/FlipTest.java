package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FlipTest {

    private final Flip flip = new Flip();

    @Test
    void test1() {
        Assert.assertEquals(flip.flip("010"), new ArrayList<>(List.of(1, 1)));
    }

    @Test
    void test2() {
        Assert.assertEquals(flip.flip("111"), new ArrayList<>());
    }

    @Test
    void test3() {
        Assert.assertEquals(flip.flip("011"), new ArrayList<>(List.of(1, 1)));
    }

    @Test
    void test4() {
        Assert.assertEquals(flip.flip("010111"), new ArrayList<>(List.of(1, 1)));
    }

    @Test
    void test5() {
        Assert.assertEquals(flip.flip("1101"), new ArrayList<>(List.of(3, 3)));
    }
}