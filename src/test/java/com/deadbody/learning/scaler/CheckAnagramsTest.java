package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckAnagramsTest {

    private final CheckAnagrams checkAnagrams = new CheckAnagrams();

    @Test
    void test1() {
        Assert.assertEquals(checkAnagrams.solve("cat", "bat"), 0);
    }

    @Test
    void test2() {
        Assert.assertEquals(checkAnagrams.solve("secure", "rescue"), 1);
    }

}