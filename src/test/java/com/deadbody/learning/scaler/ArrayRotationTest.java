package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayRotationTest {

    private final ArrayRotation arrayRotation = new ArrayRotation();

    @Test
    void test1() {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(3, 4, 1, 2)),
                arrayRotation.solve(new ArrayList<>(Arrays.asList(1, 2, 3, 4)), 2));
    }

    @Test
    void test2() {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(6, 2, 5)),
                arrayRotation.solve(new ArrayList<>(Arrays.asList(2, 5, 6)), 1));
    }

}