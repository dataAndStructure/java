package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AmazingSubarrayTest {
    private final AmazingSubarray amazingSubarray = new AmazingSubarray();

    @Test
    void test1() {
        Assert.assertEquals(amazingSubarray.solve("ABEC"), 6);
    }
}