package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MaximumSubarrayEasyTest {

    private final MaximumSubarrayEasy maximumSubarrayEasy = new MaximumSubarrayEasy();

    @Test
    void test1() {
        int A = 5;
        int B = 12;
        ArrayList<Integer> list = new ArrayList<>(List.of(2, 1, 3, 4, 5));
        Assert.assertEquals(12, maximumSubarrayEasy.maxSubarray(A, B, list));
    }

    @Test
    void test2() {
        int A = 3;
        int B = 1;
        ArrayList<Integer> list = new ArrayList<>(List.of(2, 2, 2));
        Assert.assertEquals(0, maximumSubarrayEasy.maxSubarray(A, B, list));
    }

    @Test
    void test3() {
        int A = 9;
        int B = 14;
        ArrayList<Integer> list = new ArrayList<>(List.of(1, 2, 4, 4, 5, 5, 5, 7, 5));
        Assert.assertEquals(14, maximumSubarrayEasy.maxSubarray(A, B, list));
    }

}