package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MatrixScalarProductTest {

    public final MatrixScalarProduct matrixScalarProduct = new MatrixScalarProduct();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3)));
        input.add(new ArrayList<>(List.of(4, 5, 6)));
        input.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(2, 4, 6)));
        output.add(new ArrayList<>(List.of(8, 10, 12)));
        output.add(new ArrayList<>(List.of(14, 16, 18)));
        Assert.assertEquals(matrixScalarProduct.solve(input, 2), output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(5)));
        Assert.assertEquals(matrixScalarProduct.solve(input, 5), output);
    }

}