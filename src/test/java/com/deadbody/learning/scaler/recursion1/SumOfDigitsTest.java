package com.deadbody.learning.scaler.recursion1;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SumOfDigitsTest {

    private final SumOfDigits sumOfDigits = new SumOfDigits();

    @Test
    void test1() {
        assertEquals(sumOfDigits.solve(123), 6);
    }

    @Test
    void test2() {
        assertEquals(sumOfDigits.solve(46), 10);
    }

    @Test
    void test3() {
        assertEquals(sumOfDigits.solve(11), 2);
    }

}