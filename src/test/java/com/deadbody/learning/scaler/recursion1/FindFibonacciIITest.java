package com.deadbody.learning.scaler.recursion1;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FindFibonacciIITest {

    private final FindFibonacciII findFibonacciII = new FindFibonacciII();

    @Test
    void test1() {
        assertEquals(findFibonacciII.findAthFibonacci(2), 1);
    }

    @Test
    void test2() {
        assertEquals(findFibonacciII.findAthFibonacci(9), 34);
    }

}