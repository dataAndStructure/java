package com.deadbody.learning.scaler.recursion1;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FindFactorialTest {

    private final FindFactorial findFactorial = new FindFactorial();

    @Test
    void test1() {
        assertEquals(findFactorial.solve(3), 6);
    }

    @Test
    void test2() {
        assertEquals(findFactorial.solve(4), 24);
    }

    @Test
    void test3() {
        assertEquals(findFactorial.solve(1), 1);
    }

}