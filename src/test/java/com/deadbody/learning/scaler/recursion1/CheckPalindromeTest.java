package com.deadbody.learning.scaler.recursion1;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CheckPalindromeTest {

    private final CheckPalindrome checkPalindrome = new CheckPalindrome();

    @Test
    void test1() {
        assertEquals(checkPalindrome.solve("racecar"), 1);
    }

    @Test
    void test2() {
        assertEquals(checkPalindrome.solve("naman"), 1);
    }

    @Test
    void test3() {
        assertEquals(checkPalindrome.solve("strings"), 0);
    }

}