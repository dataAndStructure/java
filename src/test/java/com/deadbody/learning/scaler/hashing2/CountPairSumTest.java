package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountPairSumTest {

    private final CountPairSum countPairSum = new CountPairSum();

    @Test
    void test1() {
        Assert.assertEquals(countPairSum.solve(new ArrayList<>(List.of(3, 5, 1, 2)), 8), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(countPairSum.solve(new ArrayList<>(List.of(1, 2, 1, 2)), 3), 4);
    }
}