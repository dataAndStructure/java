package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CheckPairSumTest {

    private final CheckPairSum checkPairSum = new CheckPairSum();

    @Test
    void test1() {
        Assert.assertEquals(checkPairSum.solve(8, new ArrayList<>(List.of(3, 5, 1, 2, 1, 2))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(checkPairSum.solve(21, new ArrayList<>(List.of(9, 10, 7, 10, 9, 1, 5, 1, 5))), 0);
    }

}