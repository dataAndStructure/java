package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class DistinctNumbersInWindowTest {
    private final DistinctNumbersInWindow distinctNumbersInWindow = new DistinctNumbersInWindow();

    @Test
    void test1() {
        Assert.assertEquals(distinctNumbersInWindow.dNums(new ArrayList<>(List.of(1, 2, 1, 3, 4, 3)), 3),
                new ArrayList<>(List.of(2, 3, 3, 2)));
    }


    @Test
    void test2() {
        Assert.assertEquals(distinctNumbersInWindow.dNums(new ArrayList<>(List.of(1, 1, 2, 2)), 1),
                new ArrayList<>(List.of(1, 1, 1, 1)));
    }
}