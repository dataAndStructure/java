package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LongestSubarrayZeroSumTest {

    private final LongestSubarrayZeroSum longestSubarrayZeroSum = new LongestSubarrayZeroSum();

    @Test
    void test1() {
        Assert.assertEquals(longestSubarrayZeroSum.solve(new ArrayList<>(List.of(1, -2, 1, 2))), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(longestSubarrayZeroSum.solve(new ArrayList<>(List.of(3, 2, -1))), 0);
    }

}