package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SubarrayWithGivenSumTest {

    private final SubarrayWithGivenSum subarrayWithGivenSum = new SubarrayWithGivenSum();

    @Test
    void test1() {
        Assert.assertEquals(subarrayWithGivenSum.solve(new ArrayList<>(List.of(1, 2, 3, 4, 5)), 5),
                new ArrayList<>(List.of(2, 3)));
    }

    @Test
    void test2() {
        Assert.assertEquals(subarrayWithGivenSum.solve(new ArrayList<>(List.of(5, 10, 20, 100, 105)), 110),
                new ArrayList<>(List.of(-1)));
    }

}