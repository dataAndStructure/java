package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountPairDifferenceTest {

    private final CountPairDifference countPairDifference = new CountPairDifference();

    @Test
    void test1() {
        Assert.assertEquals(countPairDifference.solve(new ArrayList<>(List.of(3, 5, 1, 2)), 4), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(countPairDifference.solve(new ArrayList<>(List.of(1, 2, 1, 2)), 1), 4);
    }

}