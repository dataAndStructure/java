package com.deadbody.learning.scaler.hashing2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SubarraySumEqualsKTest {

    private final SubarraySumEqualsK subarraySumEqualsK = new SubarraySumEqualsK();

    @Test
    void test1() {
        Assert.assertEquals(subarraySumEqualsK.solve(new ArrayList<>(List.of(1, 0, 1)), 1), 4);
    }

    @Test
    void test2() {
        Assert.assertEquals(subarraySumEqualsK.solve(new ArrayList<>(List.of(0, 0, 0)), 0), 6);
    }

}