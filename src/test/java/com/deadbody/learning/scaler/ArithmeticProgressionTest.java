package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ArithmeticProgressionTest {
    private final ArithmeticProgression arithmeticProgression = new ArithmeticProgression();

    @Test
    void test1() {
        ArrayList<Integer> input = new ArrayList<>(List.of(3, 5, 1));
        Assert.assertEquals(arithmeticProgression.solve(input), 1);
    }

    @Test
    void test2() {
        ArrayList<Integer> input = new ArrayList<>(List.of(2, 4, 1));
        Assert.assertEquals(arithmeticProgression.solve(input), 0);
    }
}