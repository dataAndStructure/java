package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RowSumTest {
    private final RowSum rowSum = new RowSum();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3, 4)));
        input.add(new ArrayList<>(List.of(5, 6, 7, 8)));
        input.add(new ArrayList<>(List.of(9, 2, 3, 4)));
        ArrayList<Integer> output = new ArrayList<>(List.of(10, 26, 18));
        Assert.assertEquals(rowSum.solve(input), output);
    }
}