package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MainDiagonalSumTest {

    private final MainDiagonalSum mainDiagonalSum = new MainDiagonalSum();

    @Test
    void test1() {
        List<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(3, 2)));
        input.add(new ArrayList<>(List.of(2, 3)));
        Assert.assertEquals(mainDiagonalSum.solve(input), 6);
    }

    @Test
    void test2() {
        List<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, -2, -3)));
        input.add(new ArrayList<>(List.of(-4, 5, -6)));
        input.add(new ArrayList<>(List.of(-7, -8, 9)));
        Assert.assertEquals(mainDiagonalSum.solve(input), 15);
    }

}