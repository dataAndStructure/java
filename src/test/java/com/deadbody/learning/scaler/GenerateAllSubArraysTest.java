package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenerateAllSubArraysTest {

    private final GenerateAllSubArrays generateAllSubArrays = new GenerateAllSubArrays();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        ArrayList<Integer> list1 = new ArrayList<>(List.of(1));
        result.add(list1);
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2));
        result.add(list2);
        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(1, 2, 3));
        result.add(list3);
        ArrayList<Integer> list4 = new ArrayList<>(List.of(2));
        result.add(list4);
        ArrayList<Integer> list5 = new ArrayList<>(Arrays.asList(2, 3));
        result.add(list5);
        ArrayList<Integer> list6 = new ArrayList<>(List.of(3));
        result.add(list6);
        Assert.assertEquals(generateAllSubArrays.solve(new ArrayList<>(Arrays.asList(1, 2, 3))), result);
    }

}