package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

public class WaveArrayTest {

    private final WaveArray waveArray = new WaveArray();

    @Test
    void test1() {
        Assert.assertEquals(waveArray.wave(new int[]{1, 2, 3, 4}), new int[]{2, 1, 4, 3});
    }

    @Test
    void test2() {
        Assert.assertEquals(waveArray.wave(new int[]{1, 2}), new int[]{2, 1});
    }

    @Test
    void test3() {
        Assert.assertEquals(waveArray.wave(new int[]{1, 2, 3}), new int[]{2, 1, 3});
    }

    @Test
    void test4() {
        Assert.assertEquals(waveArray.wave(new int[]{1}), new int[]{1});
    }

}