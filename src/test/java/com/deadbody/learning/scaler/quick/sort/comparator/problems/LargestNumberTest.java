package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LargestNumberTest {

    private final LargestNumber largestNumber = new LargestNumber();

    @Test
    void test1() {
        Assert.assertEquals(largestNumber.largestNumber(new ArrayList<>(List.of(3, 30, 34, 5, 9))), "9534330");
    }

    @Test
    void test2() {
        Assert.assertEquals(largestNumber.largestNumber(new ArrayList<>(List.of(2, 3, 9, 0))), "9320");
    }

    @Test
    void test3() {
        Assert.assertEquals(largestNumber.largestNumber(new ArrayList<>(List.of(0, 0, 0, 0))), "0");
    }

}