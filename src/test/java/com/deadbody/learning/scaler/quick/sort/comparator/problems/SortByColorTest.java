package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SortByColorTest {

    private final SortByColor sortByColor = new SortByColor();

    @Test
    void test1() {
        Assert.assertEquals(sortByColor.sortColors(new int[]{0, 1, 2, 0, 1, 2}), new int[]{0, 0, 1, 1, 2, 2});
    }

    @Test
    void test2() {
        Assert.assertEquals(sortByColor.sortColors(new int[]{0}), new int[]{0});
    }

}