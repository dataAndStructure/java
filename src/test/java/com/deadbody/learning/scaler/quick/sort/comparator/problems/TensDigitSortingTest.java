package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TensDigitSortingTest {

    private final TensDigitSorting tensDigitSorting = new TensDigitSorting();

    @Test
    void test1() {
        Assert.assertEquals(tensDigitSorting.solve(new int[]{15, 11, 7, 19}), new int[]{7, 19, 15, 11});
    }


    @Test
    void test2() {
        Assert.assertEquals(tensDigitSorting.solve(new int[]{2, 24, 22, 19}), new int[]{2, 19, 24, 22});
    }

}