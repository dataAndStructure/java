package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class FactorsSortTest {

    private final FactorsSort factorsSort = new FactorsSort();

    @Test
    void test1() {
        Assert.assertEquals(factorsSort.solve(new ArrayList<>(List.of(6, 8, 9))), new ArrayList<>(List.of(9, 6, 8)));
    }


    @Test
    void test2() {
        Assert.assertEquals(factorsSort.solve(new ArrayList<>(List.of(2, 4, 7))), new ArrayList<>(List.of(2, 7, 4)));
    }

    @Test
    void test3() {
        Assert.assertEquals(factorsSort.solve(new ArrayList<>(List.of(36, 13, 13, 26, 37, 28, 27, 43, 7))),
                new ArrayList<>(List.of(7, 13, 13, 37, 43, 26, 27, 28, 36)));
    }

}