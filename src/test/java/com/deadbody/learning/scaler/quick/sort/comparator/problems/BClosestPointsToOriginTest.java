package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class BClosestPointsToOriginTest {

    BClosestPointsToOrigin bClosestPointsToOrigin = new BClosestPointsToOrigin();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 3)));
        input.add(new ArrayList<>(List.of(-2, 2)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(-2, 2)));
        Assert.assertEquals(bClosestPointsToOrigin.solve(input, 1), output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, -1)));
        input.add(new ArrayList<>(List.of(2, -1)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, -1)));
        Assert.assertEquals(bClosestPointsToOrigin.solve(input, 1), output);
    }

}