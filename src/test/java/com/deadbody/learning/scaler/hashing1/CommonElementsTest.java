package com.deadbody.learning.scaler.hashing1;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommonElementsTest {

    private final CommonElements commonElements = new CommonElements();

    @Test
    void test1() {
        ArrayList<Integer> solve = commonElements.solve(new ArrayList<>(List.of(1, 2, 2, 1)),
                new ArrayList<>(List.of(2, 3, 1, 2)));
        Collections.sort(solve);
        Assert.assertEquals(solve, new ArrayList<>(List.of(1, 2, 2)));
    }

    @Test
    void test2() {
        ArrayList<Integer> solve = commonElements.solve(new ArrayList<>(List.of(2, 1, 4, 10)),
                new ArrayList<>(List.of(3, 6, 2, 10, 10)));
        Collections.sort(solve);
        Assert.assertEquals(solve, new ArrayList<>(List.of(2, 10)));
    }

}