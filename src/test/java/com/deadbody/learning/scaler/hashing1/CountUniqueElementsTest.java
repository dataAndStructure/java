package com.deadbody.learning.scaler.hashing1;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CountUniqueElementsTest {

    private final CountUniqueElements countUniqueElements = new CountUniqueElements();

    @Test
    void test1() {
        Assert.assertEquals(countUniqueElements.solve(new ArrayList<>(List.of(3, 4, 3, 6, 6))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(countUniqueElements.solve(new ArrayList<>(List.of(3, 3, 3, 9, 0, 1, 0))), 2);
    }

}