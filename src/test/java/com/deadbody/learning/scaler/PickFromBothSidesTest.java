package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class PickFromBothSidesTest {
    private final PickFromBothSides pickFromBothSides = new PickFromBothSides();

    @Test
    void test1() {
        Assert.assertEquals(pickFromBothSides.solve(new ArrayList<>(List.of(5, -2, 3, 1, 2)), 3), 8);
    }

    @Test
    void test2() {
        Assert.assertEquals(pickFromBothSides.solve(new ArrayList<>(List.of(2, 3, -1, 4, 2, 1)), 4), 9);
    }
}