package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class GoodPairTest {

    private final GoodPair goodPair = new GoodPair();

    @Test
    void test1() {
        Assert.assertEquals(1, goodPair.solve(new ArrayList<>(Arrays.asList(1, 2, 3, 4)), 7));
    }
    @Test
    void test2() {
        Assert.assertEquals(0, goodPair.solve(new ArrayList<>(Arrays.asList(1, 2, 4)), 4));
    }
    @Test
    void test3() {
        Assert.assertEquals(1, goodPair.solve(new ArrayList<>(Arrays.asList(1, 2, 2)), 4));
    }

}