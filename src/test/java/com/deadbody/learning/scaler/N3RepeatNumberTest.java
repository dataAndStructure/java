package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class N3RepeatNumberTest {
    private final N3RepeatNumber n3RepeatNumber = new N3RepeatNumber();

    @Test
    void test1() {
        Assert.assertEquals(n3RepeatNumber.repeatedNumber(List.of(1, 2, 3, 1, 1)), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(n3RepeatNumber.repeatedNumber(List.of(1, 2, 3)), -1);
    }

    @Test
    void test3() {
        Assert.assertEquals(n3RepeatNumber.repeatedNumber(List.of(1, 1, 1, 2, 3, 5, 7)), 1);
    }
}