package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MaxSumContiguousSubarrayTest {

    private final MaxSumContiguousSubarray maxSumContiguousSubarray = new MaxSumContiguousSubarray();

    @Test
    void test1() {
        Assert.assertEquals(maxSumContiguousSubarray.maxSubArray(new ArrayList<>(List.of(1, 2, 3, 4, -10))), 10);
    }

    @Test
    void test2() {
        Assert.assertEquals(maxSumContiguousSubarray.maxSubArray(
                new ArrayList<>(List.of(-2, 1, -3, 4, -1, 2, 1, -5, 4))), 6);
    }

}