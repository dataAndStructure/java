package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MinorDiagonalSumTest {

    private final MinorDiagonalSum minorDiagonalSum = new MinorDiagonalSum();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> arr = new ArrayList<>();
        arr.add(new ArrayList<>(List.of(1, -2, -3)));
        arr.add(new ArrayList<>(List.of(-4, 5, -6)));
        arr.add(new ArrayList<>(List.of(-7, -8, 9)));
        Assert.assertEquals(minorDiagonalSum.solve(arr), -5);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> arr = new ArrayList<>();
        arr.add(new ArrayList<>(List.of(3, 2)));
        arr.add(new ArrayList<>(List.of(2, 3)));
        Assert.assertEquals(minorDiagonalSum.solve(arr), 4);
    }

}