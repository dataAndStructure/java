package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindPerfectNumbersTest {

    private final FindPerfectNumbers findPerfectNumbers = new FindPerfectNumbers();

    @Test
    void test1() {
        Assert.assertEquals(findPerfectNumbers.solve(4), 0);
    }

    @Test
    void test2() {
        Assert.assertEquals(findPerfectNumbers.solve(6), 1);
    }

}