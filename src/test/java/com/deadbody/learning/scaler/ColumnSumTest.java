package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ColumnSumTest {

    private final ColumnSum columnSum = new ColumnSum();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3, 4)));
        input.add(new ArrayList<>(List.of(5, 6, 7, 8)));
        input.add(new ArrayList<>(List.of(9, 2, 3, 4)));
        ArrayList<Integer> output = new ArrayList<>(List.of(15, 10, 13, 16));
        Assert.assertEquals(columnSum.solve(input), output);
    }

}