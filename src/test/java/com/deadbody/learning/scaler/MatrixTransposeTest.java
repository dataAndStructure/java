package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MatrixTransposeTest {

    private final MatrixTranspose matrixTranspose = new MatrixTranspose();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2, 3)));
        input.add(new ArrayList<>(List.of(4, 5, 6)));
        input.add(new ArrayList<>(List.of(7, 8, 9)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 4, 7)));
        output.add(new ArrayList<>(List.of(2, 5, 8)));
        output.add(new ArrayList<>(List.of(3, 6, 9)));
        ArrayList<ArrayList<Integer>> solve = matrixTranspose.solve(input);
        Assert.assertEquals(solve, output);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(new ArrayList<>(List.of(1, 2)));
        input.add(new ArrayList<>(List.of(1, 2)));
        input.add(new ArrayList<>(List.of(1, 2)));
        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        output.add(new ArrayList<>(List.of(1, 1, 1)));
        output.add(new ArrayList<>(List.of(2, 2, 2)));
        Assert.assertEquals(matrixTranspose.solve(input), output);
    }

}