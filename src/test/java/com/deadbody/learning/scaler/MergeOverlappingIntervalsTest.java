package com.deadbody.learning.scaler;

import com.deadbody.learning.util.Interval;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class MergeOverlappingIntervalsTest {
    private final MergeOverlappingIntervals mergeOverlappingIntervals = new MergeOverlappingIntervals();

    @Test
    void test1() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(8, 10));
        intervals.add(new Interval(15, 18));
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 6));
        output.add(new Interval(8, 10));
        output.add(new Interval(15, 18));
        Assert.assertEquals(mergeOverlappingIntervals.merge(intervals), output);
    }

    @Test
    void test2() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(8, 10));
        intervals.add(new Interval(15, 18));
        intervals.add(new Interval(18, 20));
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 6));
        output.add(new Interval(8, 10));
        output.add(new Interval(15, 20));
        Assert.assertEquals(mergeOverlappingIntervals.merge(intervals), output);
    }

    @Test
    void test3() {
        ArrayList<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(4, 100));
        intervals.add(new Interval(48, 94));
        intervals.add(new Interval(16, 21));
        intervals.add(new Interval(58, 71));
        intervals.add(new Interval(36, 53));
        intervals.add(new Interval(49, 68));
        intervals.add(new Interval(18, 42));
        intervals.add(new Interval(37, 90));
        intervals.add(new Interval(68, 75));
        intervals.add(new Interval(6, 57));
        intervals.add(new Interval(25, 78));
        intervals.add(new Interval(58, 79));
        intervals.add(new Interval(18, 29));
        intervals.add(new Interval(69, 94));
        intervals.add(new Interval(5, 31));
        intervals.add(new Interval(10, 87));
        intervals.add(new Interval(21, 35));
        intervals.add(new Interval(1, 32));
        intervals.add(new Interval(7, 24));
        intervals.add(new Interval(17, 85));
        intervals.add(new Interval(30, 95));
        intervals.add(new Interval(5, 63));
        intervals.add(new Interval(1, 17));
        intervals.add(new Interval(67, 100));
        intervals.add(new Interval(53, 55));
        intervals.add(new Interval(30, 63));
        intervals.add(new Interval(7, 76));
        intervals.add(new Interval(33, 51));
        intervals.add(new Interval(62, 68));
        intervals.add(new Interval(78, 83));
        intervals.add(new Interval(12, 24));
        intervals.add(new Interval(31, 73));
        intervals.add(new Interval(64, 74));
        intervals.add(new Interval(33, 40));
        intervals.add(new Interval(51, 63));
        intervals.add(new Interval(17, 31));
        intervals.add(new Interval(14, 29));
        intervals.add(new Interval(9, 15));
        intervals.add(new Interval(39, 70));
        intervals.add(new Interval(13, 67));
        intervals.add(new Interval(27, 100));
        intervals.add(new Interval(10, 71));
        intervals.add(new Interval(18, 47));
        intervals.add(new Interval(48, 79));
        intervals.add(new Interval(70, 73));
        intervals.add(new Interval(44, 59));
        intervals.add(new Interval(68, 78));
        intervals.add(new Interval(24, 67));
        intervals.add(new Interval(32, 70));
        intervals.add(new Interval(29, 94));
        intervals.add(new Interval(45, 90));
        intervals.add(new Interval(10, 76));
        intervals.add(new Interval(12, 28));
        intervals.add(new Interval(31, 78));
        intervals.add(new Interval(9, 44));
        intervals.add(new Interval(29, 83));
        intervals.add(new Interval(21, 35));
        intervals.add(new Interval(46, 93));
        intervals.add(new Interval(66, 83));
        intervals.add(new Interval(21, 72));
        intervals.add(new Interval(29, 37));
        intervals.add(new Interval(6, 11));
        intervals.add(new Interval(56, 87));
        intervals.add(new Interval(10, 26));
        intervals.add(new Interval(11, 12));
        intervals.add(new Interval(15, 88));
        intervals.add(new Interval(3, 13));
        intervals.add(new Interval(56, 70));
        intervals.add(new Interval(40, 73));
        intervals.add(new Interval(25, 62));
        intervals.add(new Interval(63, 73));
        intervals.add(new Interval(47, 74));
        intervals.add(new Interval(8, 36));
        ArrayList<Interval> output = new ArrayList<>();
        output.add(new Interval(1, 100));
        Assert.assertEquals(mergeOverlappingIntervals.merge(intervals), output);
    }
}