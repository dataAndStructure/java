package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SumOfAllSubMatricesTest {

    private final SumOfAllSubMatrices sumOfAllSubMatrices = new SumOfAllSubMatrices();

    @Test
    void test1() {
        ArrayList<ArrayList<Integer>> arr = new ArrayList<>();
        arr.add(new ArrayList<>(List.of(1, 1)));
        arr.add(new ArrayList<>(List.of(1, 1)));
        int ans = sumOfAllSubMatrices.solve(arr);
        Assert.assertEquals(ans, 16);
    }

    @Test
    void test2() {
        ArrayList<ArrayList<Integer>> arr = new ArrayList<>();
        arr.add(new ArrayList<>(List.of(1, 2)));
        arr.add(new ArrayList<>(List.of(3, 4)));
        int ans = sumOfAllSubMatrices.solve(arr);
        Assert.assertEquals(ans, 40);
    }
}