package com.deadbody.learning.scaler;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CountOccurrenceTest {

    private final CountOccurrence countOccurrence = new CountOccurrence();

    @Test
    void test1() {
        Assert.assertEquals(countOccurrence.solve("abobc"), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(countOccurrence.solve("bobob"), 2);
    }

}