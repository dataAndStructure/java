package com.deadbody.learning.scaler.recursion2;

import org.testng.Assert;
import org.testng.annotations.Test;

public class IsMagicTest {

    private final IsMagic isMagic = new IsMagic();

    @Test
    void test1() {
        Assert.assertEquals(isMagic.solve(83557), 1);
    }


    @Test
    void test2() {
        Assert.assertEquals(isMagic.solve(1291), 0);
    }

}