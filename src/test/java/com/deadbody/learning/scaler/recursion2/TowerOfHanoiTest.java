package com.deadbody.learning.scaler.recursion2;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TowerOfHanoiTest {

    private final TowerOfHanoi towerOfHanoi = new TowerOfHanoi();

    @Test
    void test1() {
        assertEquals(towerOfHanoi.towerOfHanoi(2).toString(), "[[1, 1, 2], [2, 1, 3], [1, 2, 3]]");
    }

    @Test
    void test2() {
        assertEquals(towerOfHanoi.towerOfHanoi(3).toString(),
                "[[1, 1, 3], [2, 1, 2], [1, 3, 2], [3, 1, 3], [1, 2, " + "1], [2, 2, 3], [1, 1, 3]]");
    }

}