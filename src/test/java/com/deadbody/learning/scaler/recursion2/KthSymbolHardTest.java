package com.deadbody.learning.scaler.recursion2;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class KthSymbolHardTest {

    private final KthSymbolHard kthSymbolHard = new KthSymbolHard();

    @Test
    void test2() {
        assertEquals(kthSymbolHard.solve(4, 4), 1);
    }

    @Test
    void test1() {
        assertEquals(kthSymbolHard.solve(3, 0), 0);
    }

    @Test
    void test3() {
        assertEquals(kthSymbolHard.solve(1, 8), 1);
    }

}