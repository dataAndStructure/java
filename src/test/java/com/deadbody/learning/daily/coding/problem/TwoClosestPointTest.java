package com.deadbody.learning.daily.coding.problem;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

public class TwoClosestPointTest {
    private final TwoClosestPoint twoClosestPoint = new TwoClosestPoint();

    @Test
    void test1() {
        List<TwoClosestPoint.Coordinates> input = new LinkedList<>();
        input.add(new TwoClosestPoint.Coordinates(1, 1));
        input.add(new TwoClosestPoint.Coordinates(-1, -1));
        input.add(new TwoClosestPoint.Coordinates(3, 4));
        input.add(new TwoClosestPoint.Coordinates(6, 1));
        input.add(new TwoClosestPoint.Coordinates(-1, -6));
        input.add(new TwoClosestPoint.Coordinates(-4, -3));

        List<TwoClosestPoint.Coordinates> output = new LinkedList<>();
        output.add(new TwoClosestPoint.Coordinates(-1, -1));
        output.add(new TwoClosestPoint.Coordinates(1, 1));

        Assert.assertEquals(output, twoClosestPoint.solution(input));
    }
}