package com.deadbody.learning;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Swiggy3Test {

    @Test
    public void testOne() {
        Swiggy3 swiggy3 = new Swiggy3();
        Assert.assertEquals(swiggy3.solution("abcabcabc", "abc"), 7);
    }

    @Test
    public void testTwo() {
        Swiggy3 swiggy3 = new Swiggy3();
        Assert.assertEquals(swiggy3.solution("satyamjain", "tma"), 0);
    }

}