package com.deadbody.learning.withgoogle;

import org.testng.Assert;
import org.testng.annotations.Test;

public class EnRouteSaluteTest {

    @Test
    void test1() {
        Assert.assertEquals(EnRouteSalute.solution(">----<"), 2);
    }

    @Test
    void test2() {
        Assert.assertEquals(EnRouteSalute.solution("<<>><"), 4);
    }

}