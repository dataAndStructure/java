package com.deadbody.learning.withgoogle;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TheGrandestStaircaseOfThemAllTest {

    @Test
    void test1() {
        assertEquals(TheGrandestStaircaseOfThemAll.solution(3), 1);
    }

    @Test
    void test2() {
        assertEquals(TheGrandestStaircaseOfThemAll.solution(200), 487067745);
    }


    @Test
    void test3() {
        assertEquals(TheGrandestStaircaseOfThemAll.solution(4), 1);
    }

    @Test
    void test4() {
        assertEquals(TheGrandestStaircaseOfThemAll.solution(5), 2);
    }

}