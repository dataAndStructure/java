package com.deadbody.learning.withgoogle;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindTheAccessCodesTest {

    @Test
    void test1() {
        Assert.assertEquals(FindTheAccessCodes.solution(new int[]{1, 2, 3, 4, 5, 6}), 3);
    }

    @Test
    void test2() {
        Assert.assertEquals(FindTheAccessCodes.solution(new int[]{1, 1, 1}), 1);
    }

}