package com.deadbody.learning.withgoogle;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NumbersStationCodedMessagesTest {

    @Test
    void test1() {
        Assert.assertEquals(NumbersStationCodedMessages.solution(new int[]{1, 2, 3, 4}, 15), new int[]{-1, -1});
    }


    @Test
    void test2() {
        Assert.assertEquals(NumbersStationCodedMessages.solution(new int[]{4, 3, 10, 2, 8}, 12), new int[]{2, 3});
    }

    @Test
    void test3() {
        Assert.assertEquals(NumbersStationCodedMessages.solution(new int[]{4, 3, 5, 7, 8}, 12), new int[]{0, 2});
    }

}