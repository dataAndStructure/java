package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.TreeNode;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddOneRowToTreeTest {

    private AddOneRowToTree addOneRowToTree;

    @BeforeMethod
    public void setup() {
        addOneRowToTree = new AddOneRowToTree();
    }

    @Test
    public void test1() {
        TreeNode node = new TreeNode(4);
        node.left = new TreeNode(2);
        node.right = new TreeNode(6);
        node.left.left = new TreeNode(3);
        node.left.right = new TreeNode(1);
        node.right.left = new TreeNode(5);
        TreeNode node1 = addOneRowToTree.addOneRow(node, 1, 1);
        Assert.assertTrue(1==1);
    }

}