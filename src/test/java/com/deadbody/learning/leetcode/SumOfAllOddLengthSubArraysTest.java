package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SumOfAllOddLengthSubArraysTest {
    private final SumOfAllOddLengthSubArrays sumOfAllOddLengthSubArrays = new SumOfAllOddLengthSubArrays();

    @Test
    void test1() {
        Assert.assertEquals(sumOfAllOddLengthSubArrays.sumOddLengthSubarrays(new int[]{1, 4, 2, 5, 3}), 58);
    }

    @Test
    void test2() {
        Assert.assertEquals(sumOfAllOddLengthSubArrays.sumOddLengthSubarrays(new int[]{1, 2}), 3);
    }

    @Test
    void test3() {
        Assert.assertEquals(sumOfAllOddLengthSubArrays.sumOddLengthSubarrays(new int[]{10, 11, 12}), 66);
    }
}