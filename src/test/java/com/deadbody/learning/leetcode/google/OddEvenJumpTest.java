package com.deadbody.learning.leetcode.google;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class OddEvenJumpTest {

    OddEvenJump oddEvenJump;

    @BeforeTest
    void setup() {
        oddEvenJump = new OddEvenJump();
    }

    @Test
    void test1() {
        int[] arr = {10, 13, 12, 14, 15};
        Assert.assertEquals(oddEvenJump.oddEvenJumps(arr), 2);
    }

}