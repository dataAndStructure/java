package com.deadbody.learning.leetcode.google;


import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UniqueEmailAddressesTest {
    UniqueEmailAddresses uniqueEmailAddresses;

    @BeforeTest
    void setup(){
        uniqueEmailAddresses = new UniqueEmailAddresses();
    }

    @Test
    void test1(){
        String[] emails = {"test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"};
        int ans = uniqueEmailAddresses.numUniqueEmails(emails);
        Assert.assertEquals(2, ans);
    }


    @Test
    void test2(){
        String[] emails = {"a@leetcode.com","b@leetcode.com","c@leetcode.com"};
        int ans = uniqueEmailAddresses.numUniqueEmails(emails);
        Assert.assertEquals(3, ans);
    }
}