package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TrappingRainWaterTest {

    private final TrappingRainWater t = new TrappingRainWater();

    @Test
    public void test1() {
        Assert.assertEquals(6, t.trapBruteForce(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }

    @Test
    public void test2() {
        Assert.assertEquals(9, t.trapBruteForce(new int[]{4,2,0,3,2,5}));
    }

    @Test
    public void test3() {
        Assert.assertEquals(6, t.trapPrefixSuffix(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }

    @Test
    public void test4() {
        Assert.assertEquals(9, t.trapPrefixSuffix(new int[]{4,2,0,3,2,5}));
    }

    @Test
    public void test5() {
        Assert.assertEquals(6, t.trap(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }

    @Test
    public void test6() {
        Assert.assertEquals(9, t.trap(new int[]{4,2,0,3,2,5}));
    }

}