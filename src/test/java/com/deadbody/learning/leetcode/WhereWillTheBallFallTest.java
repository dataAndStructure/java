package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class WhereWillTheBallFallTest {

    private final WhereWillTheBallFall w = new WhereWillTheBallFall();

    @Test
    public void test1() {
        Assert.assertEquals(new int[]{1, -1, -1, -1, -1},
                w.findBall(new int[][]{{1, 1, 1, -1, -1}, {1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1}, {1, 1, 1, 1, -1}, {-1, -1, -1, -1, -1}}));
    }

}