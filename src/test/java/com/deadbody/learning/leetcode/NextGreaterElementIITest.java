package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NextGreaterElementIITest {

    private final NextGreaterElementII nextGreaterElementII = new NextGreaterElementII();

    @Test
    void test1() {
        int[] arr = {1, 2, 1};
        int[] out = {2, -1, 2};

        Assert.assertEquals(nextGreaterElementII.nextGreaterElements(arr), out);
    }

    @Test
    void test2() {
        int[] arr = {1, 2, 3, 4, 3};
        int[] out = {2, 3, 4, -1, 4};

        Assert.assertEquals(nextGreaterElementII.nextGreaterElements(arr), out);
    }

}