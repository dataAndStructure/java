package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PalindromeNumberTest {

    private PalindromeNumber palindromeNumber;

    @BeforeMethod
    public void setUp() {
        palindromeNumber = new PalindromeNumber();
    }

    @Test
    public void testOne() {
        Assert.assertTrue(palindromeNumber.isPalindrome(121));
    }

    @Test
    public void testTwo() {
        Assert.assertFalse(palindromeNumber.isPalindrome(-121));
    }


    @Test
    public void testThree() {
        Assert.assertFalse(palindromeNumber.isPalindrome(10));
    }

    @Test
    public void testFour() {
        Assert.assertFalse(palindromeNumber.isPalindrome(100));
    }
}