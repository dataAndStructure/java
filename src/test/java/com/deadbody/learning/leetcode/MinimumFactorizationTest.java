package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinimumFactorizationTest {

    MinimumFactorization minimumFactorization;

    @BeforeMethod
    public void setup(){
        minimumFactorization = new MinimumFactorization();
    }

    @Test
    public void test1(){
        Assert.assertEquals(68, minimumFactorization.smallestFactorization(48));
    }

    @Test
    public void test2(){
        Assert.assertEquals(35, minimumFactorization.smallestFactorization(15));
    }

}