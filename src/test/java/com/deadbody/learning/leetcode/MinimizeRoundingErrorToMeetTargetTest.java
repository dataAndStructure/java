package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinimizeRoundingErrorToMeetTargetTest {
    MinimizeRoundingErrorToMeetTarget minimizeRoundingErrorToMeetTarget = new MinimizeRoundingErrorToMeetTarget();

    @Test
    void test1() {
        String[] prices = new String[]{"0.700", "2.800", "4.900"};
        Assert.assertEquals("1.000", minimizeRoundingErrorToMeetTarget.minimizeError(prices, 8));
    }

    @Test
    void test2() {
        String[] prices = new String[]{"1.500", "2.500", "3.500"};
        Assert.assertEquals("-1", minimizeRoundingErrorToMeetTarget.minimizeError(prices, 10));
    }

    @Test
    void test3() {
        String[] prices = new String[]{"1.500", "2.500", "3.500"};
        Assert.assertEquals("1.500", minimizeRoundingErrorToMeetTarget.minimizeError(prices, 9));
    }
}