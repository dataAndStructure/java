package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class StringToIntegerTest {
    private StringToInteger stringToInteger;
    @BeforeMethod
    public void setUp() {
        stringToInteger = new StringToInteger();
    }

    @Test
    public void testOne() {
        String s = "42";
        int output = 42;
        Assert.assertEquals(stringToInteger.myAtoi(s), output);
    }

    @Test
    public void testTwo() {
        String s = "   -42";
        int output = -42;
        Assert.assertEquals(stringToInteger.myAtoi(s), output);
    }

    @Test
    public void testThree() {
        String s = "4193 with words";
        int output = 4193;
        Assert.assertEquals(stringToInteger.myAtoi(s), output);
    }

    @Test
    public void testFour() {
        String s = "4193 with words";
        int output = 4193;
        Assert.assertEquals(stringToInteger.myAtoi(s), output);
    }

    @Test
    public void testFive() {
        String s = "-6147483648";
        int output = -2147483648;
        Assert.assertEquals(stringToInteger.myAtoi(s), output);
    }
}