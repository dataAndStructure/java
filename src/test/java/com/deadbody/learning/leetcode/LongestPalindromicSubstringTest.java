package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LongestPalindromicSubstringTest {

    private final LongestPalindromicSubstring longestPalindromicSubstring = new LongestPalindromicSubstring();
    private final com.deadbody.learning.scaler.LongestPalindromicSubstring palindromicSubstring =
            new com.deadbody.learning.scaler.LongestPalindromicSubstring();

    @Test
    public void test1() {
        Assert.assertEquals("bab", longestPalindromicSubstring.longestPalindromeManachersAlgorithm("babad"));
        Assert.assertEquals("bab", palindromicSubstring.longestPalindrome("babad"));
    }

    @Test
    public void test2() {
        Assert.assertEquals("aaabaaa", longestPalindromicSubstring.longestPalindromeManachersAlgorithm("aaaabaaa"));
        Assert.assertEquals("aaabaaa", palindromicSubstring.longestPalindrome("aaaabaaa"));
    }

    @Test
    public void test3() {
        Assert.assertEquals("abba", longestPalindromicSubstring.longestPalindromeManachersAlgorithm("abba"));
        Assert.assertEquals("abba", palindromicSubstring.longestPalindrome("abba"));
    }
}