package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LeftAndRightSumDifferencesTest {

    private final LeftAndRightSumDifferences leftAndRightSumDifferences = new LeftAndRightSumDifferences();

    @Test
    void test1() {
        Assert.assertEquals(new int[]{15, 1, 11, 22},
                leftAndRightSumDifferences.leftRightDifference(new int[]{10, 4, 8, 3}));
    }

    @Test
    void test2() {
        Assert.assertEquals(new int[]{0},
                leftAndRightSumDifferences.leftRightDifference(new int[]{1}));
    }

}