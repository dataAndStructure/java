package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PacificAtlanticWaterFlowTest {

    private final PacificAtlanticWaterFlow pacificAtlanticWaterFlow = new PacificAtlanticWaterFlow();

    @Test
    void test1() {
        int[][] heights = new int[][]{{1, 2, 2, 3, 5}, {3, 2, 3, 4, 4}, {2, 4, 5, 3, 1}, {6, 7, 1, 4, 5}, {5, 1, 1, 2, 4}};
        List<List<Integer>> output = new ArrayList<>();
        output.add(List.of(0, 4));
        output.add(List.of(1, 3));
        output.add(List.of(1, 4));
        output.add(List.of(2, 2));
        output.add(List.of(3, 0));
        output.add(List.of(3, 1));
        output.add(List.of(4, 0));
        List<List<Integer>> actual = pacificAtlanticWaterFlow.pacificAtlantic(heights);
        actual.sort((o1, o2) -> Objects.equals(o1.get(0), o2.get(0)) ? o1.get(1) - o2.get(1) : o1.get(0) - o2.get(0));
        Assert.assertEquals(actual, output);
    }

}