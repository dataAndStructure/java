package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ContainerWithMostWaterTest {

    private final ContainerWithMostWater c = new ContainerWithMostWater();

    @Test
    public void test1() {
        Assert.assertEquals(49, c.maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
    }

    @Test
    public void test2() {
        Assert.assertEquals(1, c.maxArea(new int[]{1, 1}));
    }

}