package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RedundantConnectionGraphTest {
    RedundantConnectionGraph graph = new RedundantConnectionGraph();

    @Test
    public void testOne() {
        int[][] input = {{1, 4}, {3, 4}, {1, 3}, {1, 2}, {4, 5}};
        int[] output = {1, 3};
        Assert.assertEquals(output, graph.findRedundantConnection(input));
    }

    @Test
    public void testTwo() {
        int[][] input = {{1, 2}, {1, 3}, {2, 3}};
        int[] output = {2, 3};
        Assert.assertEquals(output, graph.findRedundantConnection(input));
    }

    @Test
    public void testThree() {
        int[][] input = {{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}};
        int[] output = {1, 4};
        Assert.assertEquals(output, graph.findRedundantConnection(input));
    }
}