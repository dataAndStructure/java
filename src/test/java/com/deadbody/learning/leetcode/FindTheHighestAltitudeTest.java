package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindTheHighestAltitudeTest {

    private final FindTheHighestAltitude findTheHighestAltitude = new FindTheHighestAltitude();

    @Test
    void test1() {
        Assert.assertEquals(findTheHighestAltitude.largestAltitude(new int[]{-5, 1, 5, 0, -7}), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(findTheHighestAltitude.largestAltitude(new int[]{-4, -3, -2, -1, 4, 3, 2}), 0);
    }

}