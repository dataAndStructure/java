package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ZigzagConversionTest {

    private ZigzagConversion zigzagConversion;

    @BeforeMethod
    public void setUp() {
        zigzagConversion = new ZigzagConversion();
    }

    @Test
    public void testOne() {
        String s = "PAYPALISHIRING";
        int numRows = 3;
        String output = "PAHNAPLSIIGYIR";
        Assert.assertEquals(output, zigzagConversion.convert(s, numRows));
    }

    @Test
    public void testTwo() {
        String s = "PAYPALISHIRING";
        int numRows = 4;
        String output = "PINALSIGYAHRPI";
        Assert.assertEquals(output, zigzagConversion.convert(s, numRows));
    }

    @Test
    public void testThree() {
        String s = "A";
        int numRows = 1;
        String output = "A";
        Assert.assertEquals(output, zigzagConversion.convert(s, numRows));
    }

    @Test
    public void testFour() {
        /**
         * "Apalindromeisaword,phrase,number,orothersequenceofunitsthatcanbereadthesamewayineitherdirection,withgeneralallowancesforadjustmentstopunctuationandworddividers."
         * 2
         * "Aaidoeswr,haenme,rtesqecouishtabrateaeaietedrcinwtgnrlloacsoajsmnsoucutoadodiiesplnrmiaodprs,ubroohreunefnttacneedhsmwynihrieto,iheeaalwnefrdutettpntainnwrdvdr."
         */
        String s = "Apalindromeisaword,phrase,number,orothersequenceofunitsthatcanbereadthesamewayineitherdirection,withgeneralallowancesforadjustmentstopunctuationandworddividers.";
        int numRows = 2;
        String output = "Aaidoeswr,haenme,rtesqecouishtabrateaeaietedrcinwtgnrlloacsoajsmnsoucutoadodiiesplnrmiaodprs,ubroohreunefnttacneedhsmwynihrieto,iheeaalwnefrdutettpntainnwrdvdr.";
        Assert.assertEquals(output, zigzagConversion.convert(s, numRows));
    }

    @Test
    public void testFive() {
        /**
         * "Apalindromeisaword,phrase,number,orothersequenceofunitsthatcanbereadthesamewayineitherdirection,withgeneralallowancesforadjustmentstopunctuationandworddividers."
         * 3
         * "Aaidoeswr,haenme,rtesqecouishtabrateaeaietedrcinwtgnrlloacsoajsmnsoucutoadodiiesplnrmiaodprs,ubroohreunefnttacneedhsmwynihrieto,iheeaalwnefrdutettpntainnwrdvdr."
         */
        String s = "Apalindromeisaword,phrase,number,orothersequenceofunitsthatcanbereadthesamewayineitherdirection,withgeneralallowancesforadjustmentstopunctuationandworddividers.";
        int numRows = 3;
        String output = "Aiosrhem,tseoihartaaeeriwgrlasasnoctaoieplnrmiaodprs,ubroohreunefnttacneedhsmwynihrieto,iheeaalwnefrdutettpntainnwrdvdr.adew,anereqcustbaeeitdcntnlocojmsuuoddis";
        Assert.assertEquals(output, zigzagConversion.convert(s, numRows));
    }
}