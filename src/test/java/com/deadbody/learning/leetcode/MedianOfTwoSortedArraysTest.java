package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MedianOfTwoSortedArraysTest {

    MedianOfTwoSortedArrays medianOfTwoSortedArrays = new MedianOfTwoSortedArrays();

    @Test
    public void testFindMedianSortedArrays() {
        Assert.assertEquals(2d, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{1, 3}, new int[]{2}));
        Assert.assertEquals(2.5d, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}));
        Assert.assertEquals(0d, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{0, 0}, new int[]{0, 0}));
        Assert.assertEquals(1d, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{}, new int[]{1}));
        Assert.assertEquals(2d, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{2}, new int[]{}));
    }
}