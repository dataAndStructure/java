package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MaximumNumberOfCoinsYouCanGetTest {

    private MaximumNumberOfCoinsYouCanGet maximumNumberOfCoinsYouCanGet;

    @BeforeMethod
    public void setup() {
        maximumNumberOfCoinsYouCanGet = new MaximumNumberOfCoinsYouCanGet();
    }

    @Test
    public void test1() {
        int []arr = {2, 4, 1, 2, 7, 8};
        Assert.assertEquals(maximumNumberOfCoinsYouCanGet.maxCoins(arr),9);
    }
}