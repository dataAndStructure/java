package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LongestSubstringWithoutRepeatingCharactersTest {

    private LongestSubstringWithoutRepeatingCharacters longestSubstringWithoutRepeatingCharacters = new LongestSubstringWithoutRepeatingCharacters();

    @Test
    public void testOne() {
        Assert.assertEquals(4, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("abccdef"));
    }

    @Test
    public void testTwo() {
        Assert.assertEquals(3, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("abcabcbb"));
    }

    @Test
    public void testThree() {
        Assert.assertEquals(1, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("bbbbb"));
    }

    @Test
    public void testFour() {
        Assert.assertEquals(3, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("pwwkew"));
    }

    @Test
    public void testFive() {
        Assert.assertEquals(0, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring(null));
    }

    @Test
    public void testSix() {
        Assert.assertEquals(0, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring(""));
    }

    @Test
    public void testSeven() {
        Assert.assertEquals(1, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("a"));
    }

    @Test
    public void testEight() {
        Assert.assertEquals(2, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("abba"));
    }

    @Test
    public void testNine() {
        Assert.assertEquals(3, longestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("dvdf"));
    }

}