package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.ListNode;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddTwoNumberLinkedListTest {
    @Test
    public void testOne() {
        AddTwoNumberLinkedList addTwoNumberLinkedList = new AddTwoNumberLinkedList();
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);

        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);

        ListNode result = addTwoNumberLinkedList.addTwoNumbers(l1, l2);
        String res = "";
        while (result != null) {
            res += result.val;
            result = result.next;
        }
        Assert.assertEquals("708", res);
    }
}
