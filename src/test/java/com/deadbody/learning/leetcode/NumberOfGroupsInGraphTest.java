package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NumberOfGroupsInGraphTest {

    NumberOfGroupsInGraph numberOfGroupsInGraph = new NumberOfGroupsInGraph();

    @Test
    public void testOne() {
        int[][] input = {
                {1, 1, 0},
                {1, 1, 0},
                {0, 0, 1}
        };
        Assert.assertEquals(numberOfGroupsInGraph.solution(input), 2);
    }

    @Test
    public void testTwo() {
        int[][] input = {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        };
        Assert.assertEquals(numberOfGroupsInGraph.solution(input), 3);
    }

    @Test
    public void testThree() {
        int[][] input = {
                {1, 0, 0, 1},
                {0, 1, 1, 0},
                {0, 1, 1, 1},
                {1, 0, 1, 1}
        };
        Assert.assertEquals(numberOfGroupsInGraph.solution(input), 1);
    }

}