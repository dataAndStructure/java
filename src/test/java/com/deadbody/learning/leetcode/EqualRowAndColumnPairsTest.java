package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class EqualRowAndColumnPairsTest {

    EqualRowAndColumnPairs equalRowAndColumnPairs = new EqualRowAndColumnPairs();

    @Test
    void test1() {
        int[][] grid = {{3, 2, 1}, {1, 7, 6}, {2, 7, 7}};
        int output = 1;
        Assert.assertEquals(equalRowAndColumnPairs.equalPairs(grid), output);
    }

    @Test
    void test2() {
        int[][] grid = {{3, 1, 2, 2}, {1, 4, 4, 5}, {2, 4, 2, 2}, {2, 4, 2, 2}};
        int output = 3;
        Assert.assertEquals(equalRowAndColumnPairs.equalPairs(grid), output);
    }

}