package com.deadbody.learning.leetcode;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NextGreaterElementIIITest {

    private final NextGreaterElementIII nextGreaterElementIII = new NextGreaterElementIII();

    @Test
    void test1() {
        int n = 12;
        int o = 21;
        Assert.assertEquals(nextGreaterElementIII.nextGreaterElement(n), o);
    }

    @Test
    void test2() {
        int n = 21;
        int o = -1;
        Assert.assertEquals(nextGreaterElementIII.nextGreaterElement(n), o);
    }

}