package com.deadbody.learning.hackerRank;

import com.deadbody.learning.hackerrank.UsPhone;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UsPhoneTest {

    @Test
    public void testOne(){
        UsPhone usPhone = new UsPhone();
        Assert.assertEquals(usPhone.find_phone_number("123-456-7890"),"123-456-7890");
    }

    @Test
    public void testTwo(){
        UsPhone usPhone = new UsPhone();
        Assert.assertEquals(usPhone.find_phone_number("1234567890"),"NONE");
    }

    @Test
    public void Three(){
        UsPhone usPhone = new UsPhone();
        Assert.assertEquals(usPhone.find_phone_number("xxx999-999-9999"),"999-999-9999");
    }

    @Test
    public void testFour(){
        UsPhone usPhone = new UsPhone();
        Assert.assertEquals(usPhone.find_phone_number("(000) 000-0000111"),"(000) 000-0000");
    }
}
