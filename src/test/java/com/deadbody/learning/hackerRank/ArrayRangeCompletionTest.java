package com.deadbody.learning.hackerRank;

import com.deadbody.learning.hackerrank.ArrayRangeCompletion;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayRangeCompletionTest {
    @Test
    public void testOne(){
        ArrayRangeCompletion arrayRangeCompletion = new ArrayRangeCompletion();
        int[] test={12,13,14,15};
        Assert.assertEquals(arrayRangeCompletion.solution(test),"12-15");
    }

    @Test(expectedExceptions = {IndexOutOfBoundsException.class})
    public void testTwo(){
        ArrayRangeCompletion arrayRangeCompletion = new ArrayRangeCompletion();
        int[] test={12,13};
        Assert.assertEquals(arrayRangeCompletion.solution(test),"12,13");
    }

    @Test(expectedExceptions = {OutOfMemoryError.class})
    public void testThree(){
        ArrayRangeCompletion arrayRangeCompletion = new ArrayRangeCompletion();
        int[] test={-3,-2,-1,5,6,7,9,11,12,13,15,16};
        Assert.assertEquals(arrayRangeCompletion.solution(test),"-3--1,5-7,9,11-13,15,16");
    }
}
