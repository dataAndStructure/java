package com.deadbody.learning.hackerRank;

import com.deadbody.learning.hackerrank.WeightedUniformStrings;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WeightedUniformStringsTest {
    @Test
    public void testOne(){
        String s= "abccddde";
        int[] queries = {1,3,12,5,9,10};
        String[] act = {"YES", "YES", "YES", "YES", "NO", "NO"};
        String[] eval = WeightedUniformStrings.weightedUniformStrings(s,queries);
        Assert.assertEquals(act,eval);
    }
}
