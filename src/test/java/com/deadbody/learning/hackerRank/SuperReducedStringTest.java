package com.deadbody.learning.hackerRank;

import com.deadbody.learning.hackerrank.SuperReducedString;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SuperReducedStringTest {

    @Test
    public void testOne(){
        SuperReducedString superReducedString = new SuperReducedString();
        Assert.assertEquals(superReducedString.superReducedString("aaabccddd"),"abd");
    }

    @Test
    public void testTwo(){
        SuperReducedString superReducedString = new SuperReducedString();
        Assert.assertEquals(superReducedString.superReducedString("lrfkqyuqfjjfquyqkfrlkxyqvnrtyssytrnvqyxkfrzrmzlygffgylzmrzrfveulqfpdbhhbdpfqluevlqdqrrcrwddwrcrrqdql"),"Empty String");
    }

    @Test
    public void testThree(){
        SuperReducedString superReducedString = new SuperReducedString();
        Assert.assertEquals(superReducedString.superReducedString("aa"),"Empty String");
    }

    @Test
    public void testFour(){
        SuperReducedString superReducedString = new SuperReducedString();
        Assert.assertEquals(superReducedString.superReducedString("baab"),"Empty String");
    }

    @Test
    public void testfove(){
        String test="a,s,c,d,f,r,";
        String[] t =  test.split(",");
        System.out.print(t);
    }
}
