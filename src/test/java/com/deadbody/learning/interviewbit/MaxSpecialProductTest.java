package com.deadbody.learning.interviewbit;

import com.deadbody.learning.common.CommonCall;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class MaxSpecialProductTest {

    private MaxSpecialProduct maxSpecialProduct;

    @BeforeMethod
    public void setUp() {
        maxSpecialProduct = new MaxSpecialProduct();
    }

    @Test
    public void testOne() {
        int[] arr = {5, 9, 6, 8, 6, 4, 6, 9, 5, 4, 9};
        List<Integer> input = CommonCall.getListFromArray(arr);
        Assert.assertEquals(maxSpecialProduct.maxSpecialProduct(input), 80);
    }

    @AfterMethod
    public void tearDown() {
        maxSpecialProduct = null;
    }
}