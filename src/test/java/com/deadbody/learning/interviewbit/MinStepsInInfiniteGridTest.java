package com.deadbody.learning.interviewbit;

import com.deadbody.learning.common.CommonCall;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class MinStepsInInfiniteGridTest {

    @Test
    public void testOne() {
        int[] x = {0, 1, 1};
        List<Integer> X = CommonCall.getListFromArray(x);

        int[] y = {0, 1, 2};
        List<Integer> Y = CommonCall.getListFromArray(y);

        MinStepsInInfiniteGrid minStepsInInfiniteGrid = new MinStepsInInfiniteGrid();
        Assert.assertEquals(minStepsInInfiniteGrid.getMinSteps(X, Y), 2);

    }

    @Test
    public void testTwo() {
        int[] x = {1, 4};
        List<Integer> X = CommonCall.getListFromArray(x);

        int[] y = {1, 3};
        List<Integer> Y = CommonCall.getListFromArray(y);

        MinStepsInInfiniteGrid minStepsInInfiniteGrid = new MinStepsInInfiniteGrid();
        Assert.assertEquals(minStepsInInfiniteGrid.getMinSteps(X, Y), 3);

    }

    @Test
    public void testThree() {
        int[] x = {1, 5, -4};
        List<Integer> X = CommonCall.getListFromArray(x);

        int[] y = {1, 7, -1};
        List<Integer> Y = CommonCall.getListFromArray(y);

        MinStepsInInfiniteGrid minStepsInInfiniteGrid = new MinStepsInInfiniteGrid();
        Assert.assertEquals(minStepsInInfiniteGrid.getMinSteps(X, Y), 15);

    }
    //A : [ -4, 7, 5, 3, 5, -4, 2, -1, -9, -8, -3, 0, 9, -7, -4, -10, -4, 2, 6, 1, -2, -3, -1, -8, 0, -8, -7, -3, 5, -1, -8, -8, 8, -1, -3, 3, 6, 1, -8, -1, 3, -9, 9, -6, 7, 8, -6, 5, 0, 3, -4, 1, -10, 6, 3, -8, 0, 6, -9, -5, -5, -6, -3, 6, -5, -4, -1, 3, 7, -6, 5, -8, -5, 4, -3, 4, -6, -7, 0, -3, -2, 6, 8, -2, -6, -7, 1, 4, 9, 2, -10, 6, -2, 9, 2, -4, -4, 4, 9, 5, 0, 4, 8, -3, -9, 7, -8, 7, 2, 2, 6, -9, -10, -4, -9, -5, -1, -6, 9, -10, -1, 1, 7, 7, 1, -9, 5, -1, -3, -3, 6, 7, 3, -4, -5, -4, -7, 9, -6, -2, 1, 2, -1, -7, 9, 0, -2, -2, 5, -10, -1, 6, -7, 8, -5, -4, 1, -9, 5, 9, -2, -6, -2, -9, 0, 3, -10, 4, -6, -6, 4, -3, 6, -7, 1, -3, -5, 9, 6, 2, 1, 7, -2, 5 ]

    public static class PlusOneTest {

        @Test
        public void testOne() {
            PlusOne plusOne = new PlusOne();
            int[] arr = {9, 9, 9, 9};
            int[] out = {1, 0, 0, 0, 0};

            List<Integer> input = CommonCall.getListFromArray(arr);

            List<Integer> output = plusOne.plusOne(input);

            Assert.assertEquals(output, CommonCall.getListFromArray(out));
        }

        @Test
        public void testTwo() {
            PlusOne plusOne = new PlusOne();
            int[] arr = {0, 3, 7, 6, 4, 0, 5, 5, 5};
            int[] out = {3, 7, 6, 4, 0, 5, 5, 6};

            List<Integer> input = CommonCall.getListFromArray(arr);

            List<Integer> output = plusOne.plusOne(input);

            Assert.assertEquals(output, CommonCall.getListFromArray(out));
        }

    }
}
