package com.deadbody.learning.interviewbit;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class BestTimeToBuyAndSellStocksITest {

    private final BestTimeToBuyAndSellStocksI bestTimeToBuyAndSellStocksI = new BestTimeToBuyAndSellStocksI();
    private final com.deadbody.learning.scaler.BestTimeToBuyAndSellStocksI scalar = new com.deadbody.learning.scaler.BestTimeToBuyAndSellStocksI();

    @Test
    void test1() {
        Assert.assertEquals(bestTimeToBuyAndSellStocksI.maxProfit(new int[]{1, 2}), 1);
        Assert.assertEquals(scalar.maxProfit(new ArrayList<>(List.of(1, 2))), 1);
    }

    @Test
    void test2() {
        Assert.assertEquals(bestTimeToBuyAndSellStocksI.maxProfit(new int[]{1, 4, 5, 2, 4}), 4);
        Assert.assertEquals(scalar.maxProfit(new ArrayList<>(List.of(1, 4, 5, 2, 4))), 4);
    }

}