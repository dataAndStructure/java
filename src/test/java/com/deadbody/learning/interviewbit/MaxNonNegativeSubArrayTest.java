package com.deadbody.learning.interviewbit;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.deadbody.learning.common.CommonCall.getListFromArray;

public class MaxNonNegativeSubArrayTest {

    @Test
    public void firstTestCase() {

        int[] in = {1, 2, 5, -7, 2, 3};
        List<Integer> input = getListFromArray(in);

        int[] out = {1, 2, 5};
        List<Integer> output = getListFromArray(out);

        List<Integer> maxSubList = getMaxSubList(input);

        Assert.assertEquals(output, maxSubList);
    }

    @Test
    public void secondTestCase() {

        int[] in = {-1, -1, -1, -3};
        List<Integer> input = getListFromArray(in);

        List<Integer> output = new ArrayList<>();

        List<Integer> maxSubList = getMaxSubList(input);

        Assert.assertEquals(output, maxSubList);
    }

    @Test
    public void thirdTestCase() {

        int[] in = {1, 2, 5, 2, 3};
        List<Integer> input = getListFromArray(in);

        int[] out = {1, 2, 5, 2, 3};
        List<Integer> output = getListFromArray(out);

        List<Integer> maxSubList = getMaxSubList(input);

        Assert.assertEquals(output, maxSubList);
    }

    private List<Integer> getMaxSubList(List<Integer> input) {
        MaxNonNegativeSubArray maxNonNegativeSubArray = new MaxNonNegativeSubArray();
        return maxNonNegativeSubArray.getMaxSubList(input);
    }

}
