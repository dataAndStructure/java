package com.deadbody.learning.adityaverma.heap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class KSortedArrayTest {

    private KSortedArray kSortedArray;

    @BeforeMethod
    public void setup(){
        kSortedArray = new KSortedArray();
    }

    @Test
    public void test1(){
        int[] arr = {6, 5, 3, 2, 8, 10, 9};
        Assert.assertEquals(new int[]{2, 3, 5, 6, 8, 9, 10}, kSortedArray.solution(arr, 3));
    }

}