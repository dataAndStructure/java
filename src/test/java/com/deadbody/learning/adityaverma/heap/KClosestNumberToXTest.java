package com.deadbody.learning.adityaverma.heap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

public class KClosestNumberToXTest {

    private KClosestNumberToX kClosestNumberToX;

    @BeforeMethod
    public void setUp() {
        kClosestNumberToX = new KClosestNumberToX();
    }

    @Test
    public void test1() {
        int[] arr = {5, 6, 7, 8, 9};
        int[] solution = kClosestNumberToX.solution(arr, 3, 7);
        Arrays.sort(solution);
        Assert.assertEquals(new int[]{6, 7, 8}, solution);
    }

    @Test
    public void test2() {
        int[] arr = {10, 2, 14, 4, 7, 6};
        int[] solution = kClosestNumberToX.solution(arr, 3, 5);
        Arrays.sort(solution);
        Assert.assertEquals(new int[]{4, 6, 7}, solution);
    }
}