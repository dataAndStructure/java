package com.deadbody.learning.adityaverma.heap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class KLargestElementTest {
    private KLargestElement kLargestElement;

    @BeforeMethod
    public void setup() {
        kLargestElement = new KLargestElement();
    }

    @Test
    public void test1() {
        int[] arr = {7, 10, 4, 3, 20, 15};
        Assert.assertEquals(new int[]{10, 15, 20}, kLargestElement.solution(arr, 3));
    }

}