package com.deadbody.learning.adityaverma.heap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class KthSmallestElementTest {

    private KthSmallestElement kthSmallestElement;
    private com.deadbody.learning.scaler.KthSmallestElement smallestElement;

    @BeforeMethod
    public void setup() {
        kthSmallestElement = new KthSmallestElement();
        smallestElement = new com.deadbody.learning.scaler.KthSmallestElement();
    }

    @Test
    public void test1() {
        int[] arr = {7, 10, 4, 3, 20, 15};
        Assert.assertEquals(7, kthSmallestElement.solution(arr, 3));
        Assert.assertEquals(7, smallestElement.kthSmallest(List.of(7, 10, 4, 3, 20, 15), 3));
    }

    @Test
    public void test2() {
        int[] arr = {8, 16, 80, 55, 32, 8, 38, 40, 65, 18, 15, 45, 50, 38, 54, 52, 23, 74, 81, 42, 28, 16, 66, 35, 91, 36, 44, 9, 85, 58, 59, 49, 75, 20, 87, 60, 17, 11, 39, 62, 20, 17, 46, 26, 81, 92};
        List<Integer> input = List.of(8, 16, 80, 55, 32, 8, 38, 40, 65, 18, 15, 45, 50, 38, 54, 52, 23, 74, 81, 42, 28, 16, 66, 35, 91, 36, 44, 9, 85, 58, 59, 49, 75, 20, 87, 60, 17, 11, 39, 62, 20, 17, 46, 26, 81, 92);
        Assert.assertEquals(17, kthSmallestElement.solution(arr, 9));
        Assert.assertEquals(17, smallestElement.kthSmallest(input, 9));
    }

}