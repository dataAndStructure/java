package com.deadbody.learning.sorting;

import com.deadbody.learning.sorting.InsertionSort;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InsertionSortTest {

    int[] input = {5, 7, 9, 0, 1, 2, 8};
    int[] output = {0, 1, 2, 5, 7, 8, 9};

    @Test
    public void testInsertionSort() {
        InsertionSort insertionSort = new InsertionSort();
        insertionSort.insertionSort(input);
        Assert.assertEquals(input,output);
    }
}
