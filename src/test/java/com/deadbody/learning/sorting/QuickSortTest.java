package com.deadbody.learning.sorting;

import com.deadbody.learning.sorting.QuickSort;
import org.testng.Assert;
import org.testng.annotations.Test;

public class QuickSortTest {

    int[] input = {5, 7, 9, 0, 1, 2, 8};
    int[] output = {0, 1, 2, 5, 7, 8, 9};

    @Test
    public void testLomuto() {
        QuickSort quickSort = new QuickSort();
        quickSort.quickSort(input,0,input.length-1,QuickSort.SortingType.LOMUTO);
        Assert.assertEquals(input,output);
    }


    @Test
    public void testHoare() {
        QuickSort quickSort = new QuickSort();
        quickSort.quickSort(input,0,input.length-1,QuickSort.SortingType.HOARE);
        Assert.assertEquals(input,output);
    }
}
