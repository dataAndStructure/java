package com.deadbody.learning;

import java.util.ArrayList;
import java.util.Arrays;

public class Test {
    ArrayList<ArrayList<Integer>> performOps(ArrayList<ArrayList<Integer>> A) {
        ArrayList<ArrayList<Integer>> B = new ArrayList<>();
        for (int i = 0; i < A.size(); i++) {
            B.add(new ArrayList<>());

            for (int j = 0; j < A.get(i).size(); j++) {
                B.get(i).add(0);
            }

            for (int j = 0; j < A.get(i).size(); j++) {
                B.get(i).set(A.get(i).size() - 1 - j, A.get(i).get(j));
            }
        }
        return B;
    }

    public static void main(String[] args) {
        Test test = new Test();
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        input.add(a);
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(5, 6, 7, 8));
        input.add(b);
        ArrayList<Integer> c = new ArrayList<>(Arrays.asList(9, 10, 11, 12));
        input.add(c);
        ArrayList<ArrayList<Integer>> B = test.performOps(input);
        for (int i = 0; i < B.size(); i++) {
            for (int j = 0; j < B.get(i).size(); j++) {
                System.out.print(B.get(i).get(j) + " ");
            }
        }
    }
}
