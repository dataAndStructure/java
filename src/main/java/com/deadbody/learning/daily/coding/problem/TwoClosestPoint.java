package com.deadbody.learning.daily.coding.problem;


import java.util.LinkedList;
import java.util.List;

public class TwoClosestPoint {
    public List<Coordinates> solution(List<Coordinates> coordinates) {
        if (coordinates == null || coordinates.isEmpty() || coordinates.size() == 1) return coordinates;
        coordinates.sort((o1, o2) -> o1.x - o2.x == 0 ? o1.y - o2.y : o1.x - o2.x);

        double min = Double.MAX_VALUE;
        Coordinates previous = coordinates.get(0);
        for (int i = 1; i < coordinates.size(); i++) {
            Coordinates current = coordinates.get(i);
            double distance = distance(current, previous);
            min = Math.min(distance, min);
            previous = current;
        }
        previous = coordinates.get(0);
        List<Coordinates> result = new LinkedList<>();
        for (int i = 1; i < coordinates.size(); i++) {
            Coordinates current = coordinates.get(i);
            double distance = distance(current, previous);
            if (min == distance) {
                result.add(previous);
                result.add(current);
            }
            previous = current;
        }
        return result;
    }

    private double distance(Coordinates current, Coordinates previous) {
        double x = (double) current.x - (double) previous.x;
        double y = (double) current.y - (double) previous.y;
        return Math.sqrt(x * x + y * y);
    }

    public record Coordinates(int x, int y) {
    }
}
