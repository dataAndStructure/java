package com.deadbody.learning;


import java.util.Locale;

/**
 * Write a Java program to print a Fibonacci sequence using recursion
 */
public class OracleQuestion {


    public static void main(String[] args) {
        new OracleQuestion().printFibonacciSeries(10);
        new OracleQuestion().printFibonacciSeries(20);
        System.out.println("For abc : " + new OracleQuestion().isPalindrome("AB BA"));
        System.out.println("For aba : " + new OracleQuestion().isPalindrome("AB Ba"));
    }

    int fibonacci(int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    void printFibonacciSeries(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(fibonacci(i) + " ");
        }
        System.out.println();
    }

    /**
     * How do you check whether a string is a palindrome in Java?
     */
    boolean isPalindrome(String input) {
        if (input == null) return false;
        if (input.isEmpty() || input.length() == 1) {
            return true;
        }
        input = input.toLowerCase(Locale.ROOT);
        int i = 0;
        int j = input.length() - 1;

        while (i < j) {
            if (input.charAt(i) != input.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
