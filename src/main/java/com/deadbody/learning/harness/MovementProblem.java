package com.deadbody.learning.harness;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MovementProblem {
    public int stepsToSolutions(int startX, int startY, int endX, int endY, int[][] board) {
        if (board.length == 0 || board[0].length == 0)
            throw new InvalidParameterException("Board length is 0");
        if (isValid(startX, endX, board.length))
            throw new InvalidParameterException("X is out of board");
        if (isValid(startY, endY, board.length))
            throw new InvalidParameterException("Y is out of board");

        Queue<Node> queue = new LinkedList<>();
        queue.add(new Node(startX, startY, 0));
        boolean[][] isVisited = new boolean[board.length][board[0].length];

        while (!queue.isEmpty()) {
            Node node = queue.remove();
            isVisited[node.x][node.y] = true;
            if (node.x == endX && node.y == endY)
                return node.distance;
            List<Node> nextNodes = getPossibleNodes(node, board, isVisited);
            queue.addAll(nextNodes);
        }

        return -1;
    }

    private List<Node> getPossibleNodes(Node node, int[][] board, boolean[][] isVisited) {
        List<Node> nextNodes = new ArrayList<>();

        //For x+2 & y+1
        if (isValid(node.x + 2, node.y + 1, board.length) && !isVisited[node.x + 2][node.y + 1]) {
            Node nextNode = new Node(node.x + 2, node.y + 1, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //For x+2 & y-1
        if (isValid(node.x + 2, node.y - 1, board.length) && !isVisited[node.x + 2][node.y - 1]) {
            Node nextNode = new Node(node.x + 2, node.y - 1, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //For x-2 & y-1
        if (isValid(node.x - 2, node.y - 1, board.length) && !isVisited[node.x - 2][node.y - 1]) {
            Node nextNode = new Node(node.x - 2, node.y - 1, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //For x-2 & y+1
        if (isValid(node.x - 2, node.y + 1, board.length) && !isVisited[node.x - 2][node.y + 1]) {
            Node nextNode = new Node(node.x - 2, node.y + 1, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //for x+1 & y+2
        if (isValid(node.x + 1, node.y + 2, board.length) && !isVisited[node.x + 1][node.y + 2]) {
            Node nextNode = new Node(node.x + 1, node.y + 2, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //for x-1 & y+2
        if (isValid(node.x - 1, node.y + 2, board.length) && !isVisited[node.x - 1][node.y + 2]) {
            Node nextNode = new Node(node.x - 1, node.y + 2, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //for x+1 & y-2
        if (isValid(node.x + 1, node.y - 2, board.length) && !isVisited[node.x + 1][node.y - 2]) {
            Node nextNode = new Node(node.x + 1, node.y - 2, node.distance + 1);
            nextNodes.add(nextNode);
        }
        //for x-1 & y-2
        if (isValid(node.x - 1, node.y - 2, board.length) && !isVisited[node.x - 1][node.y - 2]) {
            Node nextNode = new Node(node.x - 1, node.y - 2, node.distance + 1);
            nextNodes.add(nextNode);
        }

        return nextNodes;
    }

    private boolean isValid(int x, int y, int length) {
        return x >= 0 && x < length && y >= 0 && y < length;
    }
}

class Node {
    public int x;
    public int y;
    public int distance;

    public Node(int x, int y, int distance) {
        this.x = x;
        this.y = y;
        this.distance = distance;
    }
}
