package com.deadbody.learning.interviews;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintSubArraySumZero {
    public List<Pair<Integer, Integer>> solution(int[] arr) {

        List<Pair<Integer, Integer>> returnVal = new ArrayList<>();
        Map<Integer, List<Integer>> map = new HashMap<>();
        int sum = 0;

        for (int i = 0; i < arr.length; i++) {
            sum+=arr[i];
            if (map.get(sum) != null){
                map.get(sum).add(i);
            }else{
               List<Integer> integers = new ArrayList<>();
                integers.add(i);
                map.put(0, integers);
            }
        }

        return returnVal;
    }
}
