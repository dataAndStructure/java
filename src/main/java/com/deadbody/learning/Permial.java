package com.deadbody.learning;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Permial {

    public int solution(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        PriorityQueue<Data> maxHeap = new PriorityQueue<>((o1, o2) -> o2.occ - o1.occ);
        int size = arr.length;
        for (int i : arr) {
            if (map.containsKey(i))
                map.put(i, map.get(i) + 1);
            else
                map.put(i, 1);
        }

        if(map.size()==1){
            return 1;
        }

        for (Map.Entry<Integer, Integer> entries : map.entrySet()) {
            maxHeap.add(new Data(entries.getValue(), entries.getKey()));
        }
        int k = size;
        int n = 0;
        while (k > size / 2) {
            Data peek = maxHeap.poll();
            assert peek != null;
            k = k - peek.occ;
            n++;
        }
        return n;
    }

    private record Data(int occ, int data) {
    }

    public static void main(String[] args) {
        Permial permial = new Permial();
        System.out.println(permial.solution(new int[]{1,2,3,4,5,6}));
    }
}
