package com.deadbody.learning.withgoogle;

public class FindTheAccessCodes {
    public static int solution(int[] l) {
        // Your code here
        int c = 0;
        for (int j = 1; j < l.length - 1; j++) {
            int p = 0;
            for (int i = 0; i < j; i++) {
                if (l[j] % l[i] == 0) {
                    p++;
                }
            }
            int q = 0;
            for (int k = j + 1; k < l.length; k++) {
                if (l[k] % l[j] == 0) {
                    q++;
                }
            }
            c += p * q;
        }
        return c;
    }
}
