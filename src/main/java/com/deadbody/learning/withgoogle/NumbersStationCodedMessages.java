package com.deadbody.learning.withgoogle;

public class NumbersStationCodedMessages {
    public static int[] solution(int[] l, int k) {
        int s = 0;
        int e = 0;
        int t = l[0];
        while (true) {
            if (t < k) {
                e++;
                if (e == l.length) break;
                t += l[e];
            }
            if (t > k) {
                t -= l[s];
                s++;
            }
            if (t == k) {
                return new int[]{s, e};
            }
        }
        return new int[]{-1, -1};
    }
}
