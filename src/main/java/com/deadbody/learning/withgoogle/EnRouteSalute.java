package com.deadbody.learning.withgoogle;

public class EnRouteSalute {
    public static int solution(String s) {
        int a = 0;
        int r = 0;
        for (char c : s.toCharArray()) {
            if (c == '>') {
                r++;
            }
            if (c == '<') {
                a += r * 2;
            }
        }
        return a;
    }
}
