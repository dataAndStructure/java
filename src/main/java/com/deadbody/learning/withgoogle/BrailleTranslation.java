package com.deadbody.learning.withgoogle;

import java.util.HashMap;
import java.util.Map;

public class BrailleTranslation {
    public static String solution(String s) {
        Map<Character, String> m = getCodes();
        StringBuilder a = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (c >= 65 && c <= 90) {
                a.append("000001");
                a.append(m.get((char) (c + 32)));
            } else {
                a.append(m.get(c));
            }
        }
        return a.toString();
    }

    private static Map<Character, String> getCodes() {
        Map<Character, String> m = new HashMap<>();
        m.put(' ', "000000");
        m.put('a', "100000");
        m.put('b', "110000");
        m.put('c', "100100");
        m.put('d', "100110");
        m.put('e', "100010");
        m.put('f', "110100");
        m.put('g', "110110");
        m.put('h', "110010");
        m.put('i', "010100");
        m.put('j', "010110");
        m.put('k', "101000");
        m.put('l', "111000");
        m.put('m', "101100");
        m.put('n', "101110");
        m.put('o', "101010");
        m.put('p', "111100");
        m.put('q', "111110");
        m.put('r', "111010");
        m.put('s', "011100");
        m.put('t', "011110");
        m.put('u', "101001");
        m.put('v', "111001");
        m.put('w', "010111");
        m.put('x', "101101");
        m.put('y', "101111");
        m.put('z', "101011");
        return m;
    }
}
