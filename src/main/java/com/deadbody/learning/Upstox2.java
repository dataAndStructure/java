package com.deadbody.learning;

public class Upstox2 {
    /*
    Given an n x n matrix and a number x, find the position of x in the matrix if it is present in it. Otherwise,
    print “Not Found”. In the given matrix, every row and column is sorted in increasing order. The designed algorithm
     should have linear time complexity.

Example:

Input: mat[4][4] = { {10, 20, 30, 40},
                      {15, 25, 35, 45},
                      {27, 29, 37, 48},
                      {32, 33, 39, 50}};
x = 29
Output: Found at (2, 1)
Explanation: Element at (2,1) is 29

Input : mat[4][4] = { {10, 20, 30, 40},
                      {15, 25, 35, 45},
                      {27, 29, 37, 99},
                      {32, 33, 39, 100}};
x = 100
Output : Element not found

     */

    public static void main(String[] args) {
        Upstox2 upstox2 = new Upstox2();
        int[][] mat = {{10, 20, 30, 40},
                {15, 25, 35, 45},
                {27, 29, 37, 99},
                {32, 33, 39, 100}};
        System.out.println(upstox2.solution(mat,37).toString());
    }

    public Index solution(int[][] mat, int x) {
        if (mat == null || mat.length == 0)
            return new Index();
        int i = 0;
        int j = mat.length - 1;
        while (i < mat.length && j >= 0) {
            if (mat[i][j] == x) {
                return new Index(i, j);
            }
            if (mat[i][j] < x)
                i++;
            else
                j--;
        }
        return new Index();
    }
}

class Index {
    public int i;
    public int j;

    public Index(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public Index() {
        i=j=-1;
    }

    @Override
    public String toString() {
        return "Index{" +
                "i=" + i +
                ", j=" + j +
                '}';
    }
}
