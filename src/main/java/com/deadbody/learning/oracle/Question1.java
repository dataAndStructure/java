package com.deadbody.learning.oracle;

public class Question1 {
    public static void main(String[] args) {
        Question1 question1 = new Question1();
        System.out.println(question1.reverse("Oracle"));
    }

    public String reverse(String s) {
        if (s == null) return null;
        if (s.length() < 2) {
            return s;
        }
        int i = 0;
        int j = s.length() - 1;
        char[] charArray = s.toCharArray();
        while (i < j) {
            char c = charArray[i];
            charArray[i] = charArray[j];
            charArray[j] = c;
            i++;
            j--;
        }

        return s + " - " + String.valueOf(charArray).toLowerCase() + "Health";
    }
}
