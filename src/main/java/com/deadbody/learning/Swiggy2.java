package com.deadbody.learning;

import java.util.LinkedList;
import java.util.Queue;

public class Swiggy2 {
    public static void main(String[] args) {
        int[][] mat = new int[][]{
                {1, 10, 3, 8},
                {12, 2, 9, 6},
                {5, 7, 4, 11},
                {3, 7, 16, 5}
        };
        System.out.println(solution(mat));
    }

    private static int solution(int[][] mat) {
        int max = Integer.MIN_VALUE;
        if (mat == null || mat.length == 0 || mat[0].length == 0)
            return 0;

        Queue<Node> queue = new LinkedList<>();

        queue.add(new Node(mat[0][0], 0, 0));

        while (!queue.isEmpty()) {
            Node n = queue.remove();
            max = Math.max(n.value, max);

            if (valid(n.x + 1, n.y, mat)) {
                queue.add(new Node(n.value + mat[n.x + 1][n.y], n.x + 1, n.y));
            }
            if (valid(n.x, n.y + 1, mat)) {
                queue.add(new Node(n.value + mat[n.x][n.y + 1], n.x, n.y + 1));
            }
            System.out.printf("x=%d, y=%d, size=%d \n", n.x, n.y, queue.size());
        }

        return max;
    }

    private static boolean valid(int x, int y, int[][] mat) {
        return x < mat.length && y < mat[0].length;
    }
}

class Node {
    public int value;
    public int x;
    public int y;

    public Node(int value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }
}
