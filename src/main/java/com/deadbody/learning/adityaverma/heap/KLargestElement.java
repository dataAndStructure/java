package com.deadbody.learning.adityaverma.heap;

import java.util.PriorityQueue;

public class KLargestElement {
    public int[] solution(int[] arr, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        for (int i : arr) {
            priorityQueue.add(i);
            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }
        }
        int[] retVal = new int[k];
        int i = 0;
        while (!priorityQueue.isEmpty()) {
            retVal[i++] = priorityQueue.poll();
        }
        return retVal;
    }
}
