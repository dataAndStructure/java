package com.deadbody.learning.adityaverma.heap;

import java.util.Collections;
import java.util.PriorityQueue;

public class KClosestNumberToX {

    public int[] solution(int[] arr, int k, int x) {
        int[] ans = new int[k];
        PriorityQueue<Data> priorityQueue = new PriorityQueue<>(Collections.reverseOrder((o1, o2) ->
                o1.diff - o2.diff == 0 ? o1.data - o2.data : o1.diff - o2.diff));

        for (int i : arr) {
            priorityQueue.add(new Data(Math.abs(i - x), i));
            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }
        }
        int i = 0;
        while (!priorityQueue.isEmpty()) {
            ans[i++] = priorityQueue.poll().data;
        }
        return ans;
    }

    private record Data(int diff, int data) {
    }
}
