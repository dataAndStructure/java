package com.deadbody.learning.adityaverma.heap;

import java.util.PriorityQueue;

public class KSortedArray {
    public int[] solution(int[] arr, int k) {
        int[] retVal = new int[arr.length];
        int c = 0;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i : arr) {
            minHeap.add(i);
            if (minHeap.size() > k) {
                retVal[c++] = minHeap.poll();
            }
        }
        while (!minHeap.isEmpty())
            retVal[c++] = minHeap.poll();
        return retVal;
    }
}
