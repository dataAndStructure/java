package com.deadbody.learning.adityaverma.heap;

import java.util.Collections;
import java.util.PriorityQueue;

public class KthSmallestElement {
    public int solution(int[] arr, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        for (int i : arr) {
            maxHeap.add(i);
            if (maxHeap.size() > k) {
                maxHeap.poll();
            }
        }
        return maxHeap.isEmpty() ? -1 : maxHeap.poll();
    }
}
