package com.deadbody.learning.Vymo;

public class QuestionOne {
    /*
    int[] arr = {2,5,6,1,8,4, -10, -11}
    int out= 240 5*6*8
    min Left 2 2 2 1 1 1 -10 -11
    min Right -11 -11 -11 -11 -11 -11 -11
    max left 2 5 6 6 8 8 8 8
    max right 8 8 8 8 8 4-10 -11
     */

    public static void main(String[] args) {
        int[] arr = {2, 5, 6, 1, 8, 4, -10, -11};
        QuestionOne questionOne = new QuestionOne();
        System.out.println(questionOne.solution(arr));
    }

    public int solution(int[] arr) {

        if (arr.length < 3)
            throw new IllegalArgumentException("Array length is less than 3");

        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;

        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max1) {
                max3 = max2;
                max2 = max1;
                max1 = arr[i];
            } else if (arr[i] > max2) {
                max3 = max2;
                max2 = arr[i];
            } else if (arr[i] > max3) {
                max3 = arr[i];
            }
            if (arr[i] < min1) {
                min2 = min1;
                min1 = arr[i];
            } else if (arr[i] < min2) {
                min1 = arr[i];
            }
        }

        return Math.max(max1 * max2 * max3, max1 * min1 * min2);
    }

}
