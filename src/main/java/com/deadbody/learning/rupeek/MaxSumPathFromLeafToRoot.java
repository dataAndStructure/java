package com.deadbody.learning.rupeek;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MaxSumPathFromLeafToRoot {
    public List<Integer> solution(TreeNode root) {
        if (root == null) {
            return null;
        }
        int maxSum = Integer.MIN_VALUE;
        List<Integer> maxSumPath = null;
        Queue<ReturnValues> queue1 = new LinkedList<>();
        Queue<ReturnValues> queue2 = new LinkedList<>();
        List<Integer> path = new ArrayList<>();
        path.add(root.data);
        queue1.add(new ReturnValues(root.data, root, path));

        while (!queue1.isEmpty()) {
            while (!queue1.isEmpty()) {
                ReturnValues element = queue1.remove();
                if (element.node.left != null) {
                    element.path.add(element.node.left.data);
                    if (maxSum < (element.sum + element.node.left.data)) {
                        maxSum = element.sum + element.node.left.data;
                        maxSumPath = element.path;
                    }
                    queue2.add(new ReturnValues(element.sum + element.node.left.data, element.node.left, new ArrayList<>(element.path)));
                }
                if (element.node.right != null) {
                    element.path.add(element.node.right.data);
                    if (maxSum < (element.sum + element.node.right.data)) {
                        maxSum = element.sum + element.node.right.data;
                        maxSumPath = element.path;
                    }
                    queue2.add(new ReturnValues(element.sum + element.node.right.data, element.node.right, new ArrayList<>(element.path)));
                }
            }
            while (!queue2.isEmpty()) {
                ReturnValues element = queue2.remove();
                if (element.node.left != null) {
                    element.path.add(element.node.left.data);
                    if (maxSum < (element.sum + element.node.left.data)) {
                        maxSum = element.sum + element.node.left.data;
                        maxSumPath = element.path;
                    }
                    queue1.add(new ReturnValues(element.sum + element.node.left.data, element.node.left, new ArrayList<>(element.path)));
                }
                if (element.node.right != null) {
                    element.path.add(element.node.right.data);
                    if (maxSum < (element.sum + element.node.right.data)) {
                        maxSum = element.sum + element.node.right.data;
                        maxSumPath = element.path;
                    }
                    queue1.add(new ReturnValues(element.sum + element.node.right.data, element.node.right, new ArrayList<>(element.path)));
                }
            }
        }

        return maxSumPath;
    }
}

class TreeNode {
    public int data;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int data, TreeNode left, TreeNode right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }
}

class ReturnValues {
    public int sum;
    public TreeNode node;
    public List<Integer> path;

    public ReturnValues(int sum, TreeNode node, List<Integer> path) {
        this.sum = sum;
        this.node = node;
        this.path = path;
    }
}