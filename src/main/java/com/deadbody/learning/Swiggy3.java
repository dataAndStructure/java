package com.deadbody.learning;

public class Swiggy3 {
    /*
    m bigger string
    n smaller string
    m= abcabcabc
    n= abc
    a->1
    b->1
    c->1
    o=7
     */

    public int solution(String m, String n) {
        Integer x = validate(m, n);
        if (x != null) return x;

        int counter = 0;

        int[] nMap = new int[26];
        int[] mMap = new int[26];

        mapForN(n, nMap);

        mapForM(m, n, mMap);

        for (int i = 0; i < (m.length() - n.length() + 1); i++) {
            boolean update = true;
            for (int j = 0; j < 26; j++) {
                /**
                 * m = acbd
                 * n =dcb
                 */
                if (mMap[j] != nMap[j]) {
                    update = false;
                    break;
                }
            }
            if (update) {
                counter++;
            }
            mMap[m.charAt(i) - 'a']--;
            if (i != (m.length() - n.length()))
                mMap[m.charAt(i + n.length()) - 'a']++;
        }

        return counter;
    }

    private void mapForM(String m, String n, int[] mMap) {
        for (int i = 0; i < n.length(); i++) {
            mMap[m.charAt(i) - 'a']++;
        }
    }

    private void mapForN(String n, int[] nMap) {
        for (int i = 0; i < n.length(); i++) {
            nMap[n.charAt(i) - 'a']++;
        }
    }

    private Integer validate(String m, String n) {
        if (m == null || n == null) {
            return 0;
        }
        if (m.length() == 0 && n.length() == 0) {
            return 1;
        }
        if (m.length() == 0 || n.length() == 0) {
            return 0;
        }
        if (m.length() < n.length()) {
            return solution(n, m);
        }
        return null;
    }
}
