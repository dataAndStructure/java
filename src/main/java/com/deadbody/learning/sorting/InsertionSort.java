package com.deadbody.learning.sorting;

/**
 * avg - O(n^2)
 * worst - O(n^2)
 */
public class InsertionSort {

    public void insertionSort(int arr[]) {

        for (int i = 1; i < arr.length; i++) {
            int value = arr[i];
            int hole = i;
            while (hole > 0 && arr[hole - 1] > value) {
                arr[hole] = arr[hole - 1];
                hole--;
            }
            arr[hole] = value;
        }

    }
}
