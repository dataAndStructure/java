package com.deadbody.learning.sorting;

public class MergeSort {

    public void mergeSort(int arr[]) {
        int len = arr.length;
        if (len < 2)
            return;
        int mid = len / 2;

        int[] left = subArray(arr, mid, 0);
        int[] right = subArray(arr, len, mid);

        mergeSort(left);
        mergeSort(right);
        merge(left, right, arr);
    }

    private int[] subArray(int arr[], int len, int mid) {
        int[] returnValue = new int[len - mid];
        for (int i = 0; i < len - mid; i++) {
            returnValue[i] = arr[i + mid];
        }
        return returnValue;
    }

    private void merge(int[] left, int[] right, int[] arr) {
        int l = 0;
        int r = 0;
        int a = 0;

        while (l < left.length && r < right.length) {
            if (left[l] <= right[r]) {
                arr[a] = left[l];
                l++;
            } else {
                arr[a] = right[r];
                r++;
            }
            a++;
        }

        while (l < left.length) {
            arr[a] = left[l];
            a++;
            l++;
        }

        while (r < right.length) {
            arr[a] = right[r];
            a++;
            r++;
        }


    }
}
