package com.deadbody.learning.sorting;


/**
 * avg - O(nlogn)
 * worst - O(n^2)
 */
public class QuickSort {

    public void quickSort(int arr[], int low, int high, SortingType sortingType) {
        if (low < high) {
            int partition = sortingType == SortingType.LOMUTO ? lomutoPartition(arr, low, high) : hoarePartition(arr, low, high);
            quickSort(arr, low, sortingType == SortingType.HOARE ? partition : partition - 1, sortingType);
            quickSort(arr, partition + 1, high, sortingType);
        }
    }

    private int hoarePartition(int[] arr, int low, int high) {
        int pivot = arr[low];
        int i = low - 1;
        int j = high + 1;

        while (true) {
            do {
                i++;
            } while (arr[i] < pivot);
            do {
                j--;
            } while (arr[j] > pivot);
            if (i >= j)
                return j;
            swap(arr, i, j);
        }
    }

    private int lomutoPartition(int[] arr, int low, int high) {
        int pivot = arr[high];
        int i = low - 1;
        for (int j = low; j < high; j++) {
            if (arr[j] < pivot) {
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i + 1, high);
        return i + 1;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


    public enum SortingType {
        LOMUTO,
        HOARE;
    }
}
