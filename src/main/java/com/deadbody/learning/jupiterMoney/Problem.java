package com.deadbody.learning.jupiterMoney;

public class Problem {
    /* Function to reverse arr[] from start to end*/
    static void rverese(char[] arr, int start, int end) {
        if (arr.length <= 1)
            return;
        char c;
        while (start < end) {
            if (arr[start] == ' ') {
                start++;
                continue;
            }
            if (arr[end] == ' ') {
                end--;
                continue;
            }
            c = arr[start];
            arr[start] = arr[end];
            arr[end] = c;
            start++;
            end--;
        }
    }

    public static void main(String[] args) {
        char[] arr = "THIS IS A CAT".toCharArray();
        //TAC A SI SIHT
        //TACA SI S IHT
        System.out.println(arr);
        rverese(arr, 0, arr.length - 1);
        System.out.print("Reversed string is \n");
        System.out.println(arr);
    }
}
