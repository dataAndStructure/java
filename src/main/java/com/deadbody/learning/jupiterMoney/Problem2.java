package com.deadbody.learning.jupiterMoney;

public class Problem2 {
    public static void main(String[] args) {
        int target = 10;
        System.out.println(Problem2.getNumberOfPath(target, 0));
    }

    public static int getNumberOfPath(int target, int sumTillNow) {
        if (target == sumTillNow)
            return 1;
        if (target < sumTillNow)
            return 0;
        return getNumberOfPath(target, sumTillNow + 1) + getNumberOfPath(target, sumTillNow + 2);
    }
}
