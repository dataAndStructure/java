package com.deadbody.learning;

import java.util.*;

public class Swiggy {
    public static Map<Character, List<Character>> numCharSet = new HashMap<>();

    public static void init() {
        numCharSet.put('2', Arrays.asList('a', 'b', 'c'));
        numCharSet.put('3', Arrays.asList('d', 'e', 'f'));
        numCharSet.put('4', Arrays.asList('g', 'h', 'i'));
        numCharSet.put('5', Arrays.asList('j', 'k', 'l'));
        numCharSet.put('6', Arrays.asList('m', 'n', 'o'));
        numCharSet.put('7', Arrays.asList('p', 'q', 'r', 's'));
        numCharSet.put('8', Arrays.asList('t', 'u', 'v'));
        numCharSet.put('9', Arrays.asList('w', 'x', 'y', 'z'));
    }

    public static void solution(String input) {
        init();
        solRecusive(input, "");
    }

    private static void solRecusive(String input, String output) {
        if (input == null || input.length() == 0) {
            System.out.println(output);
            return;
        }
        char c = input.charAt(0);
        List<Character> keys = numCharSet.getOrDefault(c, new ArrayList<>());
        if (keys.isEmpty()) {
            solRecusive(input.substring(1), output);
        } else {
            for (char key : keys) {
                solRecusive(input.substring(1), output + key);
            }
        }
    }

    public static void main(String[] args) {
        solution("2d34f");
    }
}
