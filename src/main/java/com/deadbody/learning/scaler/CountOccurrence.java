package com.deadbody.learning.scaler;

public class CountOccurrence {
    public int solve(String str) {
        String search = "bob";
        if (str.length() < 3) return 0;
        if (str.length() == 3 && str.equals(search)) return 1;
        if (str.length() == 3) return 0;
        int count = 0;
        int n = str.length();
        for (int i = 0, j = 3; j <= n; i++, j++) {
            String substring = str.substring(i, j);
            if (substring.equals(search)) count++;
        }
        return count;
    }
}
