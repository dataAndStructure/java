package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SumOfAllSubArrays {
    public Long subarraySum(ArrayList<Integer> arr) {
        long ans = 0;
        int n = arr.size();
        for (int i = 0; i < n; i++) {
            ans += (long) arr.get(i) * (i + 1) * (n - i);
        }
        return ans;
    }
}
