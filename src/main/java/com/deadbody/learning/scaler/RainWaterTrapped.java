package com.deadbody.learning.scaler;

import java.util.List;

public class RainWaterTrapped {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int trap(final List<Integer> arr) {
        int n = arr.size();
        int[] rightMax = new int[n];
        rightMax[n - 1] = arr.get(n - 1);
        int ans = 0;
        for (int i = n - 2; i >= 0; i--) {
            rightMax[i] = Integer.max(rightMax[i + 1], arr.get(i));
        }
        int leftMax = arr.get(0);
        for (int i = 1; i < n - 1; i++) {
            int water = Integer.min(leftMax, rightMax[i + 1]) - arr.get(i);
            ans += Math.max(water, 0);
            leftMax = Math.max(leftMax, arr.get(i));
        }
        return ans;
    }

    public int twoPointerTrap(final List<Integer> arr) {
        int ans = 0;
        int n = arr.size();
        int leftMax = arr.get(0);
        int rightMax = arr.get(n - 1);
        int l = 1;
        int r = n - 2;
        while (l <= r) {
            if (leftMax < rightMax) {
                int water = leftMax - arr.get(l);
                water = Integer.max(water, 0);
                ans += water;
                leftMax = Integer.max(arr.get(l), leftMax);
                l++;
            } else {
                int water = rightMax - arr.get(r);
                water = Integer.max(water, 0);
                ans += water;
                rightMax = Integer.max(arr.get(r), rightMax);
                r--;
            }
        }
        return ans;
    }
}
