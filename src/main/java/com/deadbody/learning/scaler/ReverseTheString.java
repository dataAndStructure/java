package com.deadbody.learning.scaler;

public class ReverseTheString {
    public String solve(String s) {
        char[] charArray = s.trim().toCharArray();
        int start = 0;
        int end = charArray.length - 1;
        int length = charArray.length;
        reverse(start, end, charArray);
        for (int i = 0; i < length; i++) {
            if (charArray[i] == ' ') {
                end = i - 1;
                reverse(start, end, charArray);
            }
            if (i != 0 && charArray[i - 1] == ' ' && charArray[i] != ' ') {
                start = i;
            }
        }
        reverse(start, length - 1, charArray);
        return new String(charArray).trim();
    }

    private void reverse(int start, int end, char[] charArray) {
        while (start < end) {
            swap(charArray, start, end);
            start++;
            end--;
        }
    }

    private void swap(char[] charArray, int start, int end) {
        char c = charArray[start];
        charArray[start] = charArray[end];
        charArray[end] = c;
    }
}
