package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class RowSum {
    public ArrayList<Integer> solve(ArrayList<ArrayList<Integer>> arr) {
        int r = arr.size();
        int c = arr.get(0).size();
        ArrayList<Integer> answer = new ArrayList<>();
        for (ArrayList<Integer> arrayList : arr) {
            int sum = 0;
            for (int j = 0; j < c; j++) {
                sum += arrayList.get(j);
            }
            answer.add(sum);
        }
        return answer;
    }
}
