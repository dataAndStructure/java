package com.deadbody.learning.scaler;

import java.util.HashSet;
import java.util.Set;

public class FindPerfectNumbers {
    public int solve(int n) {
        if (n == 1)
            return 0;
        int sum = 1;
        Set<Integer> factors = new HashSet<>();
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                int j = n / i;
                if (!factors.contains(i)) {
                    factors.add(i);
                    sum += i;
                }
                if (!factors.contains(j)) {
                    factors.add(j);
                    sum += j;
                }
            }
        }
        return n == sum ? 1 : 0;
    }
}
