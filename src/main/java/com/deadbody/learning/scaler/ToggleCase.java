package com.deadbody.learning.scaler;

public class ToggleCase {
    public String solve(String s) {
        StringBuilder ans = new StringBuilder();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c >= 65 && c <= 90) ans.append((char) (c + 32));
            else ans.append((char) (c - 32));
        }
        return ans.toString();
    }
}
