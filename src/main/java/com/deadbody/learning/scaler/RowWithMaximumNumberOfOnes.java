package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class RowWithMaximumNumberOfOnes {
    public int solve(ArrayList<ArrayList<Integer>> arr) {
        int row = arr.size();
        int col = arr.get(0).size();

        int i = 0;
        int j = col - 1;
        int ans = i;

        while (i < row && j >= 0) {
            if (arr.get(i).get(j) == 1) {
                j--;
                ans = i;
            } else {
                i++;
            }
        }

        return ans;
    }
}
