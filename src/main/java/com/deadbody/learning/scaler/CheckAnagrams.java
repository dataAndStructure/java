package com.deadbody.learning.scaler;

public class CheckAnagrams {
    public int solve(String a, String b) {
        int[] arr = new int[26];
        for (int i = 0; i < a.length(); i++) {
            char c = a.charAt(i);
            int ch = c - 'a';
            arr[ch]++;
        }
        for (int i = 0; i < b.length(); i++) {
            char c = b.charAt(i);
            int ch = c - 'a';
            if (arr[ch] != 0) arr[ch]--;
        }
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return sum == 0 ? 1 : 0;
    }
}
