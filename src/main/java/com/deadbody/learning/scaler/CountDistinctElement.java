package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CountDistinctElement {
    public int solve(ArrayList<Integer> arr) {
        Set<Integer> set = new HashSet<>(arr);
        return set.size();
    }
}
