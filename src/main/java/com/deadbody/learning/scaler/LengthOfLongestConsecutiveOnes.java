package com.deadbody.learning.scaler;

public class LengthOfLongestConsecutiveOnes {
    public int solve(String s) {
        return genericSolution(s, 1);
    }

    private int genericSolution(String s, int k) {
        int length = s.length();
        int once = 0;
        for (int i = 0; i < length; i++) {
            once += s.charAt(i) == '1' ? 1 : 0;
        }
        if (once == length) {
            return once;
        }
        int ans = 0;
        int[] left = new int[length];
        int[] right = new int[length];
        left[0] = s.charAt(0) == '1' ? 1 : 0;
        right[length - 1] = s.charAt(length - 1) == '1' ? 1 : 0;
        for (int i = 1; i < length; i++) {
            left[i] = s.charAt(i) == '1' ? (left[i - 1] + 1) : 0;
        }
        for (int i = length - 2; i >= 0; i--) {
            right[i] = s.charAt(i) == '1' ? right[i + 1] + 1 : 0;
        }
        for (int i = 0; i < length; i++) {
            if (s.charAt(i) == '0') {
                int count;
                if (i == 0) {
                    count = right[i + 1];
                } else if (i == length - 1) {
                    count = left[i - 1];
                } else {
                    count = left[i - 1] + right[i + 1];
                }
                ans = Integer.max(ans, (count == once) ? count : count + 1);
            }
        }
        return ans;
    }
}
