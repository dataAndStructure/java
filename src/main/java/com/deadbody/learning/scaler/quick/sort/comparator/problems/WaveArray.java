package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import java.util.Arrays;

public class WaveArray {
    public int[] wave(int[] arr) {
        Arrays.sort(arr);
        for (int i = 0, j = 1; j < arr.length; i += 2, j += 2) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        return arr;
    }
}
