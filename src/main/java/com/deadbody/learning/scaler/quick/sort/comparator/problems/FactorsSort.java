package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactorsSort {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        int maxFactor = 0;
        for (Integer i : arr) {
            int factor = getFactor(i);
            maxFactor = Integer.max(factor, maxFactor);
            List<Integer> numbers = map.getOrDefault(factor, new ArrayList<>());
            numbers.add(i);
            Collections.sort(numbers);
            map.put(factor, numbers);
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 1; i <= maxFactor; i++) {
            List<Integer> numbers = map.getOrDefault(i, new ArrayList<>());
            ans.addAll(numbers);
        }
        return ans;
    }

    private int getFactor(Integer i) {
        int count = 0;
        for (int j = 1; j * j <= i; j++) {
            if (j * j == i) {
                count++;
                continue;
            }
            if (i % j == 0) {
                count += 2;
            }
        }
        return count;
    }
}
