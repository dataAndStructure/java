package com.deadbody.learning.scaler.quick.sort.comparator.problems;

public class SortByColor {
    public int[] sortColors(int[] arr) {
        int low = 0;
        int mid = 0;
        int high = arr.length - 1;
        while (mid <= high) {
            switch (arr[mid]) {
                case 0:
                    int temp = arr[low];
                    arr[low] = arr[mid];
                    arr[mid] = temp;
                    low++;
                    mid++;
                    break;
                case 1:
                    mid++;
                    break;
                case 2:
                    int t = arr[high];
                    arr[high] = arr[mid];
                    arr[mid] = t;
                    high--;
                    break;
            }
        }
        return arr;
    }
}
