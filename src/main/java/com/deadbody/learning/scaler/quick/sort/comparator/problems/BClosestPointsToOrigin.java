package com.deadbody.learning.scaler.quick.sort.comparator.problems;


import java.util.ArrayList;

public class BClosestPointsToOrigin {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> arr, int b) {
        arr.sort((o1, o2) -> {
            int d1 = o1.get(0) * o1.get(0) + o1.get(1) * o1.get(1);
            int d2 = o2.get(0) * o2.get(0) + o2.get(1) * o2.get(1);
            return d1 - d2;
        });
        return new ArrayList<>(arr.subList(0, b));
    }
}
