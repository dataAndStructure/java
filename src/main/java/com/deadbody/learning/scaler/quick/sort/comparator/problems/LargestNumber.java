package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import java.util.ArrayList;

public class LargestNumber {
    public String largestNumber(ArrayList<Integer> arr) {
        arr.sort((o1, o2) -> {
            String s1 = o1.toString();
            String s2 = o2.toString();

            String i1 = s1 + s2;
            String i2 = s2 + s1;

            return Long.compare(Long.parseLong(i2), Long.parseLong(i1));
        });

        StringBuilder ans = new StringBuilder();
        boolean skipZero = true;
        for (Integer i : arr) {
            if (i == 0 && skipZero) {
                continue;
            }
            skipZero = false;
            ans.append(i);
        }
        if (ans.isEmpty()) {
            ans.append(0);
        }
        return ans.toString();
    }
}
