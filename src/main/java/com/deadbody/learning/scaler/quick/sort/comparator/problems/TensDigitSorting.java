package com.deadbody.learning.scaler.quick.sort.comparator.problems;

import java.util.Arrays;
import java.util.Comparator;

public class TensDigitSorting {
    public int[] solve(int[] arr) {
        Comparator<Integer> comparator = new Comparator<>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                Integer t1 = getTenthIndexValue(o1);
                Integer t2 = getTenthIndexValue(o2);
                int o = t1 - t2;
                return o == 0 ? o2 - o1 : o;
            }

            private Integer getTenthIndexValue(Integer i) {
                i = i / 10;
                return i == 0 ? 0 : i % 10;
            }
        };
        Integer[] array = Arrays.stream(arr).boxed().toArray(Integer[]::new);
        Arrays.sort(array, comparator);
        return Arrays.stream(array).mapToInt(Integer::intValue).toArray();
    }
}
