package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class MinimumSwaps {
    public int solve(ArrayList<Integer> arr, int pointer) {
        int count = 0;
        for (Integer i : arr) {
            if (i <= pointer) count++;
        }
        int bad = 0;
        for (int i = 0; i < count; i++) {
            if (arr.get(i) > pointer) bad++;
        }
        int ans = bad;
        for (int i = 0, j = count; j < arr.size(); i++, j++) {
            if (arr.get(i) > pointer) bad--;
            if (arr.get(j) > pointer) bad++;
            ans = Math.min(ans, bad);
        }
        return ans;
    }
}
