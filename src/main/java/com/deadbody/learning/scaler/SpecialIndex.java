package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SpecialIndex {
    public int solve(ArrayList<Integer> arr) {
        int n = arr.size();
        if (n < 2) return 0;
        long[] evenPrefix = new long[n];
        long[] oddPrefix = new long[n];
        evenPrefix[0] = evenPrefix[1] = (long) arr.get(0);
        oddPrefix[0] = 0L;
        oddPrefix[1] = (long) arr.get(1);
        for (int i = 2; i < n; i++) {
            if (i % 2 == 0) {
                oddPrefix[i] = oddPrefix[i - 1];
                evenPrefix[i] = evenPrefix[i - 1] + (long) arr.get(i);
            } else {
                evenPrefix[i] = evenPrefix[i - 1];
                oddPrefix[i] = oddPrefix[i - 1] + (long) arr.get(i);
            }
        }
        int count = 0;
        for (int i = 0; i < n; i++) {
            long sumEven;
            long sumOdd;
            if (i == 0) {
                sumOdd = evenPrefix[n - 1] - evenPrefix[i];
                sumEven = oddPrefix[n - 1] - oddPrefix[i];
            } else {
                sumOdd = oddPrefix[i - 1] + evenPrefix[n - 1] - evenPrefix[i];
                sumEven = evenPrefix[i - 1] + oddPrefix[n - 1] - oddPrefix[i];
            }
            if (sumOdd == sumEven) count++;
        }
        return count;
    }
}
