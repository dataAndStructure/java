package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class ClosestMinMax {
    public int solve(ArrayList<Integer> arr) {
        int min = Integer.MAX_VALUE;
        int minIndex = arr.size();
        int max = Integer.MIN_VALUE;
        int maxIndex = arr.size();
        for (Integer integer : arr) {
            max = Math.max(integer, max);
            min = Math.min(integer, min);
        }
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i) == max) {
                maxIndex = i;
                if (minIndex != arr.size()) {
                    ans = Math.min(Math.abs(maxIndex - minIndex) + 1, ans);
                }
            }
            if (arr.get(i) == min) {
                minIndex = i;
                if (maxIndex != arr.size()) {
                    ans = Math.min(Math.abs(maxIndex - minIndex) + 1, ans);
                }
            }
        }
        return ans;
    }
}
