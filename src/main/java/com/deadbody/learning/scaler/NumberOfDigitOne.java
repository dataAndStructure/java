package com.deadbody.learning.scaler;

public class NumberOfDigitOne {
    public int solve(int A) {
        int i = 1;
        int ans = 0;
        do {
            ans += (A / (i * 10)) * i + Integer.min(Integer.max(A % (i * 10) - (i - 1), 0), i);
            i *= 10;
        } while (A / i > 0);
        return ans;
    }
}
