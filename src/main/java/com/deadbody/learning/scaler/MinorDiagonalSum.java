package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.List;

public class MinorDiagonalSum {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int solve(final List<ArrayList<Integer>> arr) {
        int n = arr.size();
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr.get(i).get(n - 1 - i);
        }
        return sum;
    }
}
