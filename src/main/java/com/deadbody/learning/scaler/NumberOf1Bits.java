package com.deadbody.learning.scaler;

public class NumberOf1Bits {
    public int numSetBits(int A) {
        int count = 0;
        while (A > 0) {
            if (A % 2 == 1) {
                count++;
            }
            A /= 2;
        }
        return count;
    }
}
