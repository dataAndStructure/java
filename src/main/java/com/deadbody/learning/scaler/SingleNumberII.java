package com.deadbody.learning.scaler;

import java.util.List;

public class SingleNumberII {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int singleNumber(final List<Integer> arr) {
        int ans = 0;
        /*
         * 1. Find the count of set the bit for all number
         * 2. Check if count is in form of 3x+1 which mean reminder should be 1 if divided by 3
         * 3. Set that bit for answer by doing ans=ans|1<<i
         */
        for (int i = 0; i < 32; i++) {
            int count = 0;
            for (Integer integer : arr) {
                if ((integer & (1 << i)) > 0) {
                    count++;
                }
            }
            if (count % 3 == 1) {
                ans = ans | 1 << i;
            }
        }
        return ans;
    }
}
