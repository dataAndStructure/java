package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class IsAllAlphaNumeric {
    public int solve(ArrayList<Character> arr) {
        for (Character c : arr) {
            if (isCapital(c) || isSmall(c) || isNumber(c)) {
                continue;
            }
            return 0;
        }
        return 1;
    }

    private boolean isNumber(Character c) {
        return c >= '0' && c <= '9';
    }

    private boolean isSmall(Character c) {
        return c >= 'a' && c <= 'z';
    }

    private boolean isCapital(Character c) {
        return c >= 'A' && c <= 'Z';
    }
}
