package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GoodPair {
    public int solve(ArrayList<Integer> arr, int sum) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < arr.size(); i++) {
            if (map.containsKey(sum - arr.get(i))) {
                if(!map.get(sum - arr.get(i)).contains(i))
                    return 1;
            }
            List<Integer> values = map.getOrDefault(arr.get(i), new LinkedList<>());
            values.add(i);
            map.put(arr.get(i), values);
        }
        return 0;
    }
}
