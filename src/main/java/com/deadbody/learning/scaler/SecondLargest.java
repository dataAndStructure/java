package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SecondLargest {
    public int solve(ArrayList<Integer> arr) {
        int max = arr.get(0);
        int secMax = -1;

        for (int i = 1; i < arr.size(); i++) {
            if (max < arr.get(i)) {
                secMax = max;
                max = arr.get(i);
            }
            if (max > arr.get(i) && secMax < arr.get(i)) {
                secMax = arr.get(i);
            }
        }

        return secMax;
    }
}
