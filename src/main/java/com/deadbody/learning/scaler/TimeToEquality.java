package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class TimeToEquality {
    public int solve(ArrayList<Integer> arr) {
        int max = Integer.MIN_VALUE;
        for (int a : arr) {
            max = Math.max(max, a);
        }
        int sol = 0;
        for (int a : arr) {
            sol += max - a;
        }
        return sol;
    }

}
