package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class LinearSearchMultipleOccurrences {
    public int solve(ArrayList<Integer> arr, int k) {
        return (int) arr.stream().filter(integer -> integer == k).count();
    }
}
