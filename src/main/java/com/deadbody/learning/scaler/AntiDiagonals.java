package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class AntiDiagonals {
    public ArrayList<ArrayList<Integer>> diagonal(ArrayList<ArrayList<Integer>> arr) {
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        int r = arr.size();
        int n = arr.size();
        int c = arr.get(0).size();
        for (int j = 0; j < c; j++) {
            int i = 0;
            ArrayList<Integer> diagonal = new ArrayList<>();
            int nc = n;
            int jc = j;
            while (i < r && jc >= 0) {
                diagonal.add(arr.get(i).get(jc));
                i++;
                jc--;
                nc--;
            }
            while (nc-- > 0) {
                diagonal.add(0);
            }
            ans.add(diagonal);
        }
        for (int i = 1; i < r; i++) {
            int j = n - 1;
            ArrayList<Integer> diagonal = new ArrayList<>();
            int nc = n;
            int ic = i;
            while (ic < r && j >= 0) {
                diagonal.add(arr.get(ic).get(j));
                ic++;
                j--;
                nc--;
            }
            while (nc-- > 0) {
                diagonal.add(0);
            }
            ans.add(diagonal);
        }
        return ans;
    }
}
