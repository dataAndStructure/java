package com.deadbody.learning.scaler;

public class SpecialSubsequencesAG {
    public int solve(String s) {
        int ans = 0;
        int aCount = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == 'A') aCount++;
            if (c == 'G') ans += aCount;
            ans %= 1000000007;
        }
        return ans;
    }
}
