package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PairSumDivisibleByM {
    public int solve(ArrayList<Integer> A, int B) {
        int zeros = 0;
        Map<Integer, Integer> map = new HashMap<>();
        long ans = 0;
        long mod = (long) (1e9 + 7);
        for (Integer val : A) {
            val = val % B;
            if (val == 0) {
                zeros++;
            } else {
                if (map.containsKey(B - val)) {
                    ans = ((ans % mod) + (map.get(B - val) % mod)) % mod;
                }
                map.put(val, map.getOrDefault(val, 0) + 1);
            }
        }
        ans = (addZeros(zeros) % mod + ans % mod) % mod;
        return (int) (ans % mod);
    }

    private long addZeros(int zeros) {
        if (zeros < 2) return 0;
        return (long) zeros * (zeros - 1) / 2;
    }
}
