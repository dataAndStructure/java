package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class LargestCoprimeDivisor {
    public int cpFact(int A, int B) {
        int gcd = gcd(A, B);
        if (gcd == 1) return A;
        return cpFact(A / gcd, B);
    }

    private int gcd(int a, int b) {
        if (b == 0) return a;
        return gcd(b, a % b);
    }
}
