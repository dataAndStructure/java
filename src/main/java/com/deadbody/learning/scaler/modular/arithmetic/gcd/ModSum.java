package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import java.util.ArrayList;

public class ModSum {
    public int solve(ArrayList<Integer> A) {
        int mod = (int) (1e9 + 7);
        int[] arr = new int[1001];
        for (Integer i : A) {
            arr[i]++;
        }
        long sum = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == 0) continue;
            for (Integer integer : A) {
                long val = i % integer;
                if (val == 0) continue;
                val = ((val % mod) * (arr[i] % mod)) % mod;
                sum = ((sum % mod) + (val % mod)) % mod;
            }
        }
        return (int) (sum % mod);
    }
}
