package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class PrimeModuloInverse {
    public int solve(int A, int B) {
        if (gcd(A, B) != 1) {
            return 0;
        }
        return gcdModuloInverse(A, B - 2, B);
    }

    // Extended Euclidean Algorithm
    private int gcdModuloInverse(int x, int y, int m) {
        if (y == 0) return 1;
        long p = gcdModuloInverse(x, y / 2, m) % m;
        p = (p % m * p % m) % m;
        return (int) ((y % 2 == 0) ? p : (x % m * p % m) % m);
    }

    // Euclidean Algorithm
    private int gcd(int a, int b) {
        while (b > 0) {
            int r = a % b;
            a = b;
            b = r;
        }
        return a;
    }
}
