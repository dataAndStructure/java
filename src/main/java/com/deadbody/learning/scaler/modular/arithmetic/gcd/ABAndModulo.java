package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class ABAndModulo {
    public int solve(int A, int B) {
        return (A > B) ? A - B : B - A;
    }
}
