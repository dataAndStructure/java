package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class GreatestCommonDivisor {
    public int gcd(int A, int B) {
        if (B > A) return gcd(B, A);
        while (B > 0) {
            int r = A % B;
            A = B;
            B = r;
        }
        return A;
    }
}
