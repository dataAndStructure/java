package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class TrailingZerosInFactorial {
    public int trailingZeroes(int A) {
        int ans = 0;
        long a = A;
        for (long i = 5; a / i > 0; i *= 5) {
            ans += (int) (a / i);
        }
        return ans;
    }
}