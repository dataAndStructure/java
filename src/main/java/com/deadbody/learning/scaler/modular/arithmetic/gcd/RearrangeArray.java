package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import java.util.ArrayList;

public class RearrangeArray {
    public void arrange(ArrayList<Integer> arr) {
        int n = arr.size();
        for (int i = 0; i < n; i++) {
            arr.set(i, (arr.get(i) % n) + ((arr.get(arr.get(i)) % n) * n));
        }
        for (int i = 0; i < n; i++) {
            arr.set(i, arr.get(i) / n);
        }
    }
}
