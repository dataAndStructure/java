package com.deadbody.learning.scaler.modular.arithmetic.gcd;

public class DivisorGame {
    public int solve(int A, int B, int C) {
        return A / lcm(B, C);
    }

    private int lcm(int b, int c) {
        return (b * c) / gcd(b, c);
    }

    private int gcd(int b, int c) {
        if (c == 0) return b;
        return gcd(c, b % c);
    }
}
