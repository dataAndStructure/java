package com.deadbody.learning.scaler.modular.arithmetic.gcd;

import java.util.ArrayList;

public class DeleteOne {
    public int solve(ArrayList<Integer> A) {
        int n = A.size();
        int[] prefix = new int[n];
        int[] suffix = new int[n];
        prefix[0] = A.get(0);
        suffix[n - 1] = A.get(n - 1);
        for (int i = 1; i < n; i++) {
            prefix[i] = gcd(prefix[i - 1], A.get(i));
        }
        for (int i = n - 2; i >= 0; i--) {
            suffix[i] = gcd(suffix[i + 1], A.get(i));
        }
        int ans = 0;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                ans = suffix[i + 1];
                continue;
            }
            if (i == n - 1) {
                ans = Integer.max(ans, prefix[i - 1]);
                continue;
            }
            ans = Integer.max(ans, gcd(suffix[i + 1], prefix[i - 1]));
        }
        return ans;
    }

    private int gcd(int a, int b) {
        if (b == 0) return a;
        return gcd(b, a % b);
    }
}
