package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class CountingSubArraysEasy {
    public int solve(ArrayList<Integer> arr, int maxSum) {
        int count = 0;
        for (int i = 0; i < arr.size(); i++) {
            int sum = 0;
            for (int j = i; j < arr.size(); j++) {
                sum += arr.get(j);
                if (sum < maxSum) count++;
            }
        }
        return count;
    }
}
