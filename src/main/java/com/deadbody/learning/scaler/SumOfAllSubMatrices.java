package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SumOfAllSubMatrices {
    public int solve(ArrayList<ArrayList<Integer>> arr) {
        int row = arr.size();
        int col = arr.get(0).size();
        int ans = 0;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                ans += arr.get(i).get(j) * (i + 1) * (row - i) * (j + 1) * (col - j);
            }
        }
        return ans;
    }
}
