package com.deadbody.learning.scaler;

public class ToggleIthBit {
    public int solve(int a, int b) {
        int c = 1 << b;
        return a ^ c;
    }
}
