package com.deadbody.learning.scaler;

import java.util.List;

public class MajorityElement {
    public int majorityElement(final List<Integer> arr) {
        int votes = 1;
        int candidate = arr.get(0);
        for (int j = 1; j < arr.size(); j++) {
            Integer i = arr.get(j);
            if (candidate == i) {
                votes++;
            } else {
                votes--;
            }
            if (votes == 0) {
                candidate = i;
                votes++;
            }
        }
        return candidate;
    }
}
