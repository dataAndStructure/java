package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SearchInRowWiseAndColumnWiseSortedMatrix {
    public int solve(ArrayList<ArrayList<Integer>> arr, int k) {
        int row = arr.size();
        int col = arr.get(0).size();
        int i = 0;
        int j = col - 1;
        while (i < row && j >= 0) {
            if (arr.get(i).get(j) == k) {
                if (j - 1 >= 0 && arr.get(i).get(j - 1) == k) {
                    j--;
                    continue;
                }
                return (i + 1) * 1009 + j + 1;
            }
            if (arr.get(i).get(j) < k) {
                i++;
            } else {
                j--;
            }
        }
        return -1;
    }
}
