package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SubarrayWithGivenSumAndLength {
    public int solve(ArrayList<Integer> arr, int k, int target) {
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += arr.get(i);
        }
        if (sum == target) return 1;
        int n = arr.size();
        for (int j = k; j < n; j++) {
            sum += arr.get(j);
            sum -= arr.get(j - k);
            if (sum == target) return 1;
        }
        return 0;
    }
}
