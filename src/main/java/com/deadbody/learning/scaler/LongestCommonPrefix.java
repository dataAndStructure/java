package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class LongestCommonPrefix {
    public String longestCommonPrefix(ArrayList<String> arr) {
        int i = 0;
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            boolean breakLoop = false;
            char ch = 0;
            for (int j = 0; j < arr.size(); j++) {
                String s = arr.get(j);
                if (s.length() == i) {
                    breakLoop = true;
                    break;
                }
                if (j == 0) ch = s.charAt(i);
                if (j != 0 && ch != s.charAt(i)) {
                    breakLoop = true;
                    break;
                }
            }
            i++;
            if (breakLoop) break;
            stringBuilder.append(ch);
        }
        return stringBuilder.toString();
    }
}
