package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SubarrayInGivenRange {
    public ArrayList<Integer> solve(ArrayList<Integer> arr, int start, int end) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            result.add(arr.get(i));
        }
        return result;
    }
}
