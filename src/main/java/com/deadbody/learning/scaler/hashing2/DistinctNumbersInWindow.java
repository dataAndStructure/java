package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DistinctNumbersInWindow {
    public ArrayList<Integer> dNums(ArrayList<Integer> arr, int k) {
        if (k > arr.size()) return new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < k; i++) {
            map.put(arr.get(i), map.getOrDefault(arr.get(i), 0) + 1);
        }
        ArrayList<Integer> ans = new ArrayList<>();
        ans.add(map.size());
        for (int i = k; i < arr.size(); i++) {
            Integer firstKey = arr.get(i - k);
            if (map.containsKey(firstKey)) {
                map.put(firstKey, map.get(firstKey) - 1);
                if (map.get(firstKey) == 0) map.remove(firstKey);
            }
            Integer key = arr.get(i);
            map.put(key, map.getOrDefault(key, 0) + 1);
            ans.add(map.size());
        }
        return ans;
    }
}
