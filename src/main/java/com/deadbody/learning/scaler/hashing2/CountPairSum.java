package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountPairSum {
    public int solve(ArrayList<Integer> arr, int k) {
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        int mod = 1000000007;
        for (Integer i : arr) {
            count = (count % mod + map.getOrDefault(k - i, 0) % mod) % mod;
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        return count;
    }
}
