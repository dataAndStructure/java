package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LongestSubarrayZeroSum {
    public int solve(ArrayList<Integer> arr) {
        int ans = 0;
        Map<Long, Integer> map = new HashMap<>();
        map.put(0L, -1);
        long sum = 0;
        for (int i = 0; i < arr.size(); i++) {
            Integer key = arr.get(i);
            sum += key;
            ans = Integer.max(ans, i - map.getOrDefault(sum, i));
            map.put(sum, map.getOrDefault(sum, i));
        }
        return ans;
    }
}
