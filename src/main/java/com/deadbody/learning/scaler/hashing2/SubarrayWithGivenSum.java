package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubarrayWithGivenSum {
    public ArrayList<Integer> solve(ArrayList<Integer> arr, int k) {
        Map<Long, Integer> map = new HashMap<>();
        map.put(0L, -1);
        long sum = 0;
        for (int i = 0; i < arr.size(); i++) {
            Integer value = arr.get(i);
            sum += value;
            long target = sum - k;
            if (map.containsKey(target)) {
                ArrayList<Integer> ans = new ArrayList<>();
                for (int j = map.get(target) + 1; j <= i; j++) {
                    ans.add(arr.get(j));
                }
                return ans;
            }
            map.put(sum, map.getOrDefault(sum, i));
        }
        ArrayList<Integer> ans = new ArrayList<>();
        ans.add(-1);
        return ans;
    }
}
