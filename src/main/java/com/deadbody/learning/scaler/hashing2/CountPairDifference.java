package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountPairDifference {
    public int solve(ArrayList<Integer> arr, int k) {
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        int mod = 1000000007;
        for (Integer c : arr) {
            count = (count % mod + map.getOrDefault(c - k, 0) % mod) % mod;
            count = (count % mod + map.getOrDefault(k + c, 0) % mod) % mod;
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        return count;
    }
}
