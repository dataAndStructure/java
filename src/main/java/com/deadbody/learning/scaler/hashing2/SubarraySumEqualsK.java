package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubarraySumEqualsK {
    public int solve(ArrayList<Integer> arr, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        int count = 0;
        map.put(0, 1);
        for (Integer i : arr) {
            sum += i;
            int target = sum - k;
            count += map.getOrDefault(target, 0);
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }
}
