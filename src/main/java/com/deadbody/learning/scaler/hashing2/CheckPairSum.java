package com.deadbody.learning.scaler.hashing2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CheckPairSum {
    public int solve(int k, ArrayList<Integer> arr) {
        Set<Integer> map = new HashSet<>();
        for (Integer i : arr) {
            if (map.contains(k - i)) return 1;
            map.add(i);
        }
        return 0;
    }
}
