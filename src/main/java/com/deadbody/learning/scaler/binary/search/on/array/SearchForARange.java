package com.deadbody.learning.scaler.binary.search.on.array;

public class SearchForARange {
    public int[] searchRange(final int[] arr, int target) {
        int s = 0;
        int e = arr.length - 1;
        int[] ans = new int[2];
        ans[0] = searchLeft(arr, target, s, e);
        ans[1] = searchRight(arr, target, s, e);
        return ans;
    }

    private int searchRight(int[] arr, int target, int s, int e) {
        int mid = 0;
        boolean found = false;
        while (s <= e) {
            mid = s + (e - s) / 2;
            if ((mid == arr.length - 1 && arr[mid] == target) || (arr[mid] == target && arr[mid] != arr[mid + 1])) {
                found = true;
                break;
            }
            if ((arr[mid] == target && arr[mid] == arr[mid + 1]) || (arr[mid] < target)) {
                s = mid + 1;
            } else {
                e = mid - 1;
            }
        }
        return found ? mid : -1;
    }

    private int searchLeft(int[] arr, int target, int s, int e) {
        int mid = 0;
        boolean found = false;
        while (s <= e) {
            mid = s + (e - s) / 2;
            if ((mid == 0 && arr[mid] == target) || (arr[mid] == target && arr[mid] != arr[mid - 1])) {
                found = true;
                break;
            }
            if ((arr[mid] == target && arr[mid] == arr[mid - 1]) || (arr[mid] > target)) {
                e = mid - 1;
            } else {
                s = mid + 1;
            }
        }
        return found ? mid : -1;
    }
}
