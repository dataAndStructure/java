package com.deadbody.learning.scaler.binary.search.on.array;

public class SingleElementInSortedArray {
    public int solve(int[] arr) {
        int s = 0;
        int e = arr.length - 1;
        int mid = 0;
        while (s <= e) {
            mid = s + (e - s) / 2;
            if ((mid == 0 || arr[mid - 1] < arr[mid]) && (mid == arr.length - 1 || arr[mid] < arr[mid + 1])) {
                break;
            }
            if (mid == 0 || arr[mid - 1] == arr[mid]) {
                if (mid - 1 != s) {
                    mid = mid - 1;
                } else {
                    mid = mid + 1;
                }
            }
            if ((e - mid + 1) % 2 == 0) {
                e = mid - 1;
            } else {
                s = mid;
            }
        }
        return arr[mid];
    }
}
