package com.deadbody.learning.scaler.binary.search.on.array;

public class FindAPeakElement {
    public int solve(int[] arr) {
        int s = 0;
        int e = arr.length - 1;
        int mid = 0;
        boolean leftPeak = false;
        boolean rightPeak = false;
        while (s <= e) {
            mid = s + (e - s) / 2;
            if (mid == 0 || arr[mid - 1] <= arr[mid]) {
                leftPeak = true;
            }
            if (mid == arr.length - 1 || arr[mid] >= arr[mid + 1]) {
                rightPeak = true;
            }
            if (leftPeak && rightPeak) {
                break;
            }
            if ((mid == 0 || arr[mid - 1] < arr[mid]) && (mid == arr.length - 1 || arr[mid] < arr[mid + 1])) {
                s = mid + 1;
                leftPeak = false;
                rightPeak = false;
            }
            if ((mid == 0 || arr[mid - 1] > arr[mid]) && (mid == arr.length - 1 || arr[mid] > arr[mid + 1])) {
                e = mid - 1;
                leftPeak = false;
                rightPeak = false;
            }
        }
        return arr[mid];
    }
}
