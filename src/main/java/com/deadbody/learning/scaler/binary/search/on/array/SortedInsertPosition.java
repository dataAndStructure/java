package com.deadbody.learning.scaler.binary.search.on.array;

public class SortedInsertPosition {
    public int searchInsert(int[] arr, int target) {
        int s = 0;
        int e = arr.length - 1;
        int mid = s + (e - s) / 2;

        if (arr[s] > target) return s;
        if (arr[e] < target) return e + 1;

        while (s <= e) {
            mid = s + (e - s) / 2;
            if (arr[mid] == target) return mid;
            if (arr[mid] < target) s = mid + 1;
            if (arr[mid] > target) e = mid - 1;
        }
        if (arr[mid] < target && arr[mid + 1] > target) return mid + 1;
        if (arr[mid - 1] < target && arr[mid] > target) return mid;
        return -1;
    }
}
