package com.deadbody.learning.scaler.binary.search.on.array;

import java.util.Arrays;

public class MinimumDifference {
    public int solve(int rows, int cols, int[][] mat) {
        for (int i = 0; i < rows; i++) {
            Arrays.sort(mat[i]);
        }
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < cols; j++) {
                int min = minClosest(mat[i][j], mat[i + 1]);
                int max = minClosest(mat[i][j], mat[i + 1]);
                ans = Math.min(ans, Math.min(Math.abs(min - mat[i][j]), Math.abs(max - mat[i][j])));
            }
        }
        return ans;
    }

    private int minClosest(int k, int[] arr) {
        int s = 0;
        int e = arr.length - 1;
        int mid = 0;
        while (s <= e) {
            mid = s + (e - s) / 2;
            if (arr[mid] == k) break;
            if (s == e) {
                break;
            }
            if (arr[mid] > k) {
                e = mid - 1;
            }
            if (arr[mid] < k) {
                s = mid + 1;
            }
        }
        return arr[mid];
    }
}
