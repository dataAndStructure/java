package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class ContinuousSumQuery {
    public ArrayList<Integer> solve(int n, ArrayList<ArrayList<Integer>> q) {
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ans.add(0);
        }
        for (ArrayList<Integer> query : q) {
            int l = query.get(0) - 1;
            int r = query.get(1) - 1;
            int p = query.get(2);
            ans.set(l, ans.get(l) + p);
            if (r != n - 1) {
                ans.set(r + 1, ans.get(r + 1) - p);
            }
        }
        for (int i = 1; i < n; i++) {
            ans.set(i, ans.get(i) + ans.get(i - 1));
        }
        return ans;
    }
}
