package com.deadbody.learning.scaler;

import java.util.List;

public class BestTimeToBuyAndSellStocksI {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int maxProfit(final List<Integer> arr) {
        if (arr.size() < 2) return 0;
        int min = Integer.MAX_VALUE;
        int ans = 0;
        for (int number : arr) {
            min = Math.min(min, number);
            ans = Math.max(ans, number - min);
        }
        return ans;
    }
}
