package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.List;

public class MainDiagonalSum {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int solve(final List<ArrayList<Integer>> arr) {
        int n = arr.size();
        int ans = 0;
        for (int i = 0; i < n; i++) {
            ans += arr.get(i).get(i);
        }
        return ans;
    }
}
