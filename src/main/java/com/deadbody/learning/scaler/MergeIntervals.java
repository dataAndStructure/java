package com.deadbody.learning.scaler;

import com.deadbody.learning.util.Interval;

import java.util.ArrayList;

public class MergeIntervals {
    public ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        ArrayList<Interval> ans = new ArrayList<>();
        int i = 0;
        int n = intervals.size();
        while (i < n && intervals.get(i).end < newInterval.start) {
            ans.add(intervals.get(i));
            i++;
        }
        while (i < n && !(intervals.get(i).end < newInterval.start || newInterval.end < intervals.get(i).start)) {
            newInterval.start = Integer.min(intervals.get(i).start, newInterval.start);
            newInterval.end = Integer.max(intervals.get(i).end, newInterval.end);
            i++;
        }
        ans.add(newInterval);
        while (i < n) {
            ans.add(intervals.get(i));
            i++;
        }
        return ans;
    }
}