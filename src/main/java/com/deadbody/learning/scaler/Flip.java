package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class Flip {
    public ArrayList<Integer> flip(String str) {
        int n = str.length();
        int currentSum = 0;
        int maxSum = 0;
        int left = 0;
        int right = 0;
        int maxLeft = 0;
        int maxRight = 0;
        for (int i = 0; i < n; i++) {
            char c = str.charAt(i);
            currentSum += c == '1' ? -1 : 1;
            if (maxSum < currentSum) {
                maxSum = currentSum;
                maxLeft = left;
                maxRight = right;
            }
            if (currentSum < 0) {
                currentSum = 0;
                left = i + 1;
                right = i + 1;
            } else {
                right++;
            }
        }
        ArrayList<Integer> result = new ArrayList<>();
        result.add(maxLeft + 1);
        result.add(maxRight + 1);
        return maxSum == 0 ? new ArrayList<>() : result;
    }
}
