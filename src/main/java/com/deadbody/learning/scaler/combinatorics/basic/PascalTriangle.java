package com.deadbody.learning.scaler.combinatorics.basic;

import java.util.ArrayList;

public class PascalTriangle {
    public ArrayList<ArrayList<Integer>> solve(int A) {
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < A; i++) {
            ArrayList<Integer> a = new ArrayList<>();
            for (int j = 0; j < A; j++) {
                a.add(0);
            }
            ans.add(a);
        }

        for (int i = 0; i < A; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0) ans.get(i).set(j, 1);
                else if (i == j) ans.get(i).set(j, 1);
                else ans.get(i).set(j, ans.get(i - 1).get(j - 1) + ans.get(i - 1).get(j));
            }
        }
        return ans;
    }
}
