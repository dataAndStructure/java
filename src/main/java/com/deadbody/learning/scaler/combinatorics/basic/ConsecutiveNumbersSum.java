package com.deadbody.learning.scaler.combinatorics.basic;

public class ConsecutiveNumbersSum {
    public int solve(int A) {
        int count = 0;
        for (int k = 1; k * k < 2 * A; k++) {
            int t = (A - (k * (k - 1)) / 2);
            if (t % k == 0) count++;
        }
        return count;
    }
}
