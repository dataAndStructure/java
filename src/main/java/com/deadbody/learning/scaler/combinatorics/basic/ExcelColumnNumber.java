package com.deadbody.learning.scaler.combinatorics.basic;

public class ExcelColumnNumber {
    public int titleToNumber(String A) {
        int ans = 0;
        int power = 1;
        for (int i = A.length() - 1; i >= 0; i--) {
            ans += (A.charAt(i) - 64) * power;
            power = power * 26;
        }
        return ans;
    }
}
