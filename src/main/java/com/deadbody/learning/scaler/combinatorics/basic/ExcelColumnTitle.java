package com.deadbody.learning.scaler.combinatorics.basic;

public class ExcelColumnTitle {
    public String convertToTitle(int A) {
        StringBuilder ans = new StringBuilder();
        while (A > 0) {
            A--;
            int i = A % 26;
            ans.insert(0, (char) (i + 65));
            A = A / 26;
        }
        return ans.toString();
    }
}
