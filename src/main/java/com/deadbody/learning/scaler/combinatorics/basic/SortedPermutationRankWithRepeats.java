package com.deadbody.learning.scaler.combinatorics.basic;

import java.util.ArrayList;

public class SortedPermutationRankWithRepeats {
    private final int MOD = 1000003;

    public int findRank(String A) {

        // Initializations
        ArrayList<Integer> charCount = new ArrayList<>(256);

        for (int i = 0; i < 256; i++)
            charCount.add(0);
        for (int i = 0; i < A.length(); i++) {
            int ch = A.charAt(i);
            charCount.set(ch, charCount.get(ch) + 1);
        }

        ArrayList<Integer> fact = new ArrayList<>(); // fact[i] will contain i! % MOD
        initializeFactorials(A.length() + 1, fact);

        long rank = 1;

        for (int i = 0; i < A.length(); i++) {
            // find number of permutations placing character smaller than A[i] at ith position
            // among characters from i to A.length
            long less = 0;
            int remaining = A.length() - i - 1;
            for (int ch = 0; ch < A.charAt(i); ch++) {
                if (charCount.get(ch) == 0) continue;
                // Lets try placing ch as the first character in remaining characters
                // and check the number of permutation possible.
                charCount.set(ch, charCount.get(ch) - 1);
                long numPermutation = fact.get(remaining);

                for (int c = 0; c < 128; c++) {
                    if (charCount.get(c) <= 1) continue;
                    numPermutation = (numPermutation * inverseNumber(fact.get(charCount.get(c)))) % MOD;
                }

                charCount.set(ch, charCount.get(ch) + 1);
                less = (less + numPermutation) % MOD;
            }

            rank = (rank + less) % MOD;
            // remove the current character from the set.
            charCount.set(A.charAt(i), charCount.get(A.charAt(i)) - 1);
        }
        return (int) rank;

    }

    private void initializeFactorials(int totalLen, ArrayList<Integer> fact) {
        // calculates factorial
        long factorial = 1;
        fact.add(1); // 0!= 1
        for (int curIndex = 1; curIndex < totalLen; curIndex++) {
            factorial = (factorial * curIndex) % MOD;
            fact.add((int) factorial);
        }
    }

    private long pow(long x, int y, int k) {
        long result = 1;
        while (y > 0) {
            if (y % 2 == 1) {
                result = (result * x) % k;
                y--;
            }
            y >>= 1;
            x = (x * x) % k;
        }
        return result;
    }

    private long inverseNumber(int num) {
        // Find the modular multiplicative inverse
        // Calculates (num ^ (MOD - 2)) % MOD
        return pow(num, MOD - 2, MOD);
    }
}
