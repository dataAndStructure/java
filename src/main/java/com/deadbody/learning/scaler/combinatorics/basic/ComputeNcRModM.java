package com.deadbody.learning.scaler.combinatorics.basic;

public class ComputeNcRModM {
    public int solve(int A, int B, int C) {
        return combination(A, Integer.min(B, A - B), C);
    }

    private int combination(int n, int r, int m) {
        int[][] dp = new int[(n + 1)][(r + 1)];
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= Integer.min(i, r); j++) {
                if (j == 0) dp[i][j] = 1;
                else if (i == j) dp[i][j] = 1;
                else dp[i][j] = (dp[i - 1][j - 1] % m + dp[i - 1][j] % m) % m;
            }
        }
        return dp[n][r];
    }
}
