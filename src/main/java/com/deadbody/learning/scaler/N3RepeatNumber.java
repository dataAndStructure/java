package com.deadbody.learning.scaler;

import java.util.List;

public class N3RepeatNumber {
    // DO NOT MODIFY THE LIST
    public int repeatedNumber(final List<Integer> arr) {
        int votes1 = 0;
        int candidate1 = -1;
        int votes2 = 0;
        int candidate2 = -1;
        for (int j = 0; j < arr.size(); j++) {
            Integer i = arr.get(j);
            if (votes1 == 0 && i != candidate2) {
                candidate1 = i;
                votes1++;
                continue;
            }
            if (i == candidate1) {
                votes1++;
                continue;
            }
            if (votes2 == 0) {
                candidate2 = i;
                votes2++;
                continue;
            }
            if (i == candidate2) {
                votes2++;
                continue;
            }
            votes1--;
            votes2--;
        }
        int count = 0;
        for (Integer i : arr) {
            if (i == candidate1) count++;
        }
        if (count * 3 > arr.size()) return candidate1;
        count = 0;
        for (Integer i : arr) {
            if (i == candidate2) count++;
        }
        if (count * 3 > arr.size()) return candidate2;
        return -1;
    }
}
