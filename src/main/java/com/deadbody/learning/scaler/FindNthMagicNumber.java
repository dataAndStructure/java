package com.deadbody.learning.scaler;

public class FindNthMagicNumber {

    public int solve(int a) {
        int x = 5;
        int ans = 0;
        while (a > 0) {
            if (a % 2 == 1) {
                ans += x;
            }
            x *= 5;
            a /= 2;
        }
        return ans;
    }
}
