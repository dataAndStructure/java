package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class CountOfElements {
    public int solve(ArrayList<Integer> arr) {
        int max = Integer.MIN_VALUE;
        for (int i : arr) {
            max = Math.max(max, i);
        }
        int count = 0;
        for (int i : arr) {
            count += ((i == max) ? 1 : 0);
        }
        return arr.size() - count;
    }
}
