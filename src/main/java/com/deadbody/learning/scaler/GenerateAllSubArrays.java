package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class GenerateAllSubArrays {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<Integer> arr) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            for (int j = i; j < arr.size(); j++) {
                ArrayList<Integer> subarray = getSubarray(arr, i, j);
                result.add(subarray);
            }
        }
        return result;
    }

    private ArrayList<Integer> getSubarray(ArrayList<Integer> arr, int start, int end) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            result.add(arr.get(i));
        }
        return result;
    }
}
