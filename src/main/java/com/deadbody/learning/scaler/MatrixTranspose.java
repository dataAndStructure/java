package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class MatrixTranspose {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> arr) {
        int r = arr.size();
        int c = arr.get(0).size();

        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < c; i++) {
            ArrayList<Integer> list = new ArrayList<>();
            for (int j = 0; j < r; j++) {
                list.add(0);
            }
            ans.add(list);
        }

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                ans.get(j).set(i, arr.get(i).get(j));
            }
        }
        return ans;
    }
}
