package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommonElements {
    public ArrayList<Integer> solve(ArrayList<Integer> a, ArrayList<Integer> b) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer i : a) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (Integer i : b) {
            if (map.containsKey(i)) {
                Integer val = map.get(i);
                if (val == 1) {
                    map.remove(i);
                } else {
                    map.put(i, map.get(i) - 1);
                }
                ans.add(i);
            }
        }
        return ans;
    }
}
