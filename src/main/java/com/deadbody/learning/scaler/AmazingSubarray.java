package com.deadbody.learning.scaler;

public class AmazingSubarray {
    public int solve(String arr) {
        int count = 0;
        int mod = 10003;
        int n = arr.length();
        for (int i = 0; i < n; i++) {
            char c = arr.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
                count = (count + (n - i) % mod) % mod;
            }
        }
        return count;
    }
}
