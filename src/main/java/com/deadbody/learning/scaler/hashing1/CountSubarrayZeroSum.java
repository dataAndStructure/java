package com.deadbody.learning.scaler.hashing1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountSubarrayZeroSum {
    public int solve(ArrayList<Integer> arr) {

        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        int sum = 0;
        int mod = 1000000007;
        map.put(0, 1);
        for (Integer i : arr) {
            sum = (sum % mod + i % mod) % mod;
            if (map.containsKey(sum)) {
                count += map.get(sum);
            }
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }
}
