package com.deadbody.learning.scaler.hashing1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountUniqueElements {
    public int solve(ArrayList<Integer> arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer i : arr) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                count++;
            }
        }
        return count;
    }
}
