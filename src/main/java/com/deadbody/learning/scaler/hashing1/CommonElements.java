package com.deadbody.learning.scaler.hashing1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommonElements {
    public ArrayList<Integer> solve(ArrayList<Integer> a, ArrayList<Integer> b) {
        // Just write your code below to complete the function. Required input is available to you as the function
        // arguments.
        // Do not print the result or any output. Just return the result via this function.
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer i : a) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        ArrayList<Integer> retVal = new ArrayList<>();
        for (Integer i : b) {
            if (map.containsKey(i)) {
                retVal.add(i);
                if (map.get(i) == 1) {
                    map.remove(i);
                } else {
                    map.put(i, map.getOrDefault(i, 0) - 1);
                }
            }
        }
        return retVal;
    }
}
