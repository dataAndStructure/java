package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class GoodSubArraysEasy {
    public int solve(ArrayList<Integer> arr, int pointer) {
        int count = 0;
        for (int i = 0; i < arr.size(); i++) {
            int sum = 0;
            for (int j = i; j < arr.size(); j++) {
                sum += arr.get(j);
                if (sum < pointer && (j - i + 1) % 2 == 0) count++;
                if (sum > pointer && (j - i + 1) % 2 != 0) count++;
            }
        }
        return count;
    }
}
