package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class FirstMissingInteger {
    public int firstMissingPositive(ArrayList<Integer> arr) {
        int n = arr.size();
        for (int i = 0; i < n; i++) {
            if (arr.get(i) < 0) arr.set(i, 0);
        }
        for (int i = 0; i < n; i++) {
            int val = Math.abs(arr.get(i));
            if (1 <= val && val <= n) {
                if (arr.get(val - 1) > 0) {
                    arr.set(val - 1, arr.get(val - 1) * -1);
                }
                if (arr.get(val - 1) == 0) {
                    arr.set(val - 1, (n + 1) * -1);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (arr.get(i) >= 0) {
                return i + 1;
            }
        }
        return n + 1;
    }
}
