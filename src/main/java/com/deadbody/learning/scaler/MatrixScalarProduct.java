package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class MatrixScalarProduct {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> arr, int multiplier) {
        int r = arr.size();
        int c = arr.get(0).size();
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                arr.get(i).set(j, arr.get(i).get(j) * multiplier);
            }
        }
        return arr;
    }
}
