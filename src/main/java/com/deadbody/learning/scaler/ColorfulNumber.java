package com.deadbody.learning.scaler;

import java.util.HashSet;
import java.util.Set;

public class ColorfulNumber {
    public int colorful(int a) {
        Set<Integer> set = new HashSet<>();
        for (int i = 1; a / i != 0; i *= 10) {
            int c = a / i;
            int p = getProduct(c);
            if (set.contains(p)) {
                return 0;
            }
            set.add(p);
            for (int j = 10; c != c % j; j *= 10) {
                int d = c % j;
                int q = getProduct(d);
                if (set.contains(q)) {
                    return 0;
                }
                set.add(q);
            }
        }
        return 1;
    }

    private int getProduct(int x) {
        int ans = 1;
        int d = 10;
        do {
            ans *= x % d;
            x /= d;
        } while (x > 0);
        return ans;
    }
}
