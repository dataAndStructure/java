package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class NobleInteger {
    public int solve(ArrayList<Integer> arr) {
        arr.sort(Collections.reverseOrder());
        int n = arr.size();
        int count = 0;
        int ans = 0;
        if (arr.get(0) == 0) ans++;
        for (int i = 1; i < n; i++) {
            if (!Objects.equals(arr.get(i), arr.get(i - 1))) {
                count = i;
            }
            if (arr.get(i) == count) {
                ans++;
            }
        }
        return ans == 0 ? -1 : ans;
    }
}
