package com.deadbody.learning.scaler.recursion1;

public class CheckPalindrome {
    public int solve(String A) {
        return isPalindrome(A, 0, A.length() - 1) ? 1 : 0;
    }

    private boolean isPalindrome(String A, int s, int e) {
        if (s >= e) return true;
        if (A.charAt(s) == A.charAt(e)) return isPalindrome(A, s + 1, e - 1);
        return false;
    }
}
