package com.deadbody.learning.scaler;

public class LongestPalindromicSubstring {
    public String longestPalindrome(String s) {
        int length = s.length();
        int start = 0;
        int end = 0;
        int ans = 0;
        for (int i = 0; i < length; i++) {
            //Odd case
            int len = 1;
            int st = i - 1;
            int e = i + 1;
            while (st >= 0 && e < length) {
                if (s.charAt(st) != s.charAt(e)) break;
                len += 2;
                st--;
                e++;
            }
            if (len > ans) {
                ans = len;
                start = st + 1;
                end = e - 1;
            }
        }
        for (int i = 0, j = 1; j < length; i++, j++) {
            //Odd case
            int len = 0;
            int st = i;
            int e = j;
            while (st >= 0 && e < length) {
                if (s.charAt(st) != s.charAt(e)) break;
                len += 2;
                st--;
                e++;
            }
            if (len > ans) {
                ans = len;
                start = st + 1;
                end = e - 1;
            }
        }
        return s.substring(start, end + 1);
    }
}
