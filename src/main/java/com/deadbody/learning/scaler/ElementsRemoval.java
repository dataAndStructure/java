package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.Collections;

public class ElementsRemoval {
    public int solve(ArrayList<Integer> arr) {
        arr.sort(Collections.reverseOrder());
        int cost = 0;
        int n = arr.size();
        for (int i = 0; i < n; i++) {
            cost += arr.get(i) * (i + 1);
        }
        return cost;
    }
}
