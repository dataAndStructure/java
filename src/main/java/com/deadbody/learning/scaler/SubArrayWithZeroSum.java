package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SubArrayWithZeroSum {

    public int solve(ArrayList<Integer> arr) {
        if (arr.size() == 1 && arr.get(0) == 0) return 1;
        if (arr.size() == 1) return 0;
        Set<Long> set = new HashSet<>();
        long sum = arr.get(0);
        set.add(sum);
        for (int i = 1; i < arr.size(); i++) {
            sum += arr.get(i);
            if (set.contains(sum) || sum == 0)
                return 1;
            set.add(sum);
        }
        return 0;
    }
}
