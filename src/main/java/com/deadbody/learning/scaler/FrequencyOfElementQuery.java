package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FrequencyOfElementQuery {
    public ArrayList<Integer> solve(ArrayList<Integer> a, ArrayList<Integer> b) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer i : a) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (Integer i : b) {
            ans.add(map.getOrDefault(i, 0));
        }
        return ans;
    }
}
