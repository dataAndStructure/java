package com.deadbody.learning.scaler;

public class SetBit {
    public int solve(int A, int B) {
        if (A == B) {
            return 1 << A;
        }
        return (1 << A) + (1 << B);
    }
}
