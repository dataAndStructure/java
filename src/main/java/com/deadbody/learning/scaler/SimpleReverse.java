package com.deadbody.learning.scaler;

public class SimpleReverse {
    public String solve(String s) {
        int i = 0;
        int j = s.length() - 1;
        char[] charArray = s.toCharArray();
        while (i < j) {
            char c = charArray[i];
            charArray[i] = charArray[j];
            charArray[j] = c;
            i++;
            j--;
        }
        return new String(charArray);
    }
}
