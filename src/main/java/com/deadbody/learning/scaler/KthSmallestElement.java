package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.List;

public class KthSmallestElement {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int kthSmallest(final List<Integer> arr, int k) {
        List<Integer> result = selectionSort(arr);
        return result.get(k - 1);
    }

    private List<Integer> selectionSort(List<Integer> arr) {
        int n = arr.size();
        List<Integer> result = new ArrayList<>(n);
        result.addAll(arr);
        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (result.get(j) < result.get(minIndex)) minIndex = j;
            }
            if (minIndex != i) swap(result, minIndex, i);
        }
        return result;
    }

    private void swap(List<Integer> result, int maxIndex, int end) {
        if (maxIndex == -1) return;
        Integer temp = result.get(maxIndex);
        result.set(maxIndex, result.get(end));
        result.set(end, temp);
    }

}
