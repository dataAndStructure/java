package com.deadbody.learning.scaler;

public class UnsetIthBit {
    public int solve(int a, int b) {
        int y = 1 << b;
        y = ~y;
        return (a & y);
    }
}
