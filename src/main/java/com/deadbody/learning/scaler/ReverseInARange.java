package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class ReverseInARange {
    public ArrayList<Integer> solve(ArrayList<Integer> arr, int B, int C) {
        int i = B;
        int j = C;

        while (i < j) {
            int temp = arr.get(i);
            arr.set(i, arr.get(j));
            arr.set(j, temp);
            i++;
            j--;
        }
        return arr;
    }
}
