package com.deadbody.learning.scaler;

public class HelpFromSam {
    public int solve(int A) {
        int count = 0;
        while (A > 0) {
            if (A % 2 == 1) {
                count++;
            }
            A /= 2;
        }
        return count;
    }
}
