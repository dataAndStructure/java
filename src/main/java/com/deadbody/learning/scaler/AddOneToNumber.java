package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class AddOneToNumber {
    public ArrayList<Integer> plusOne(ArrayList<Integer> arr) {
        int carry = 1;
        for (int i = arr.size() - 1; i >= 0; i--) {
            if (carry == 0) break;
            int data = arr.get(i);
            data += carry;
            carry = data / 10;
            data %= 10;
            arr.set(i, data);
        }
        if (carry != 0) {
            arr.add(0, carry);
        }
        while (arr.get(0) == 0) {
            arr.remove(0);
        }
        return arr;
    }
}
