package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class RangeSumQuery {
    public ArrayList<Long> rangeSum(ArrayList<Integer> arr, ArrayList<ArrayList<Integer>> query) {
        long[] prefix = new long[arr.size()];
        prefix[0] = (long) arr.get(0);
        for (int i = 1; i < arr.size(); i++) {
            prefix[i] = prefix[i - 1] + arr.get(i);
        }
        ArrayList<Long> result = new ArrayList<>();
        for (ArrayList<Integer> integers : query) {
            int s = integers.get(0);
            int e = integers.get(1);
            if (s == 0) result.add(prefix[e]);
            else result.add(prefix[e] - prefix[s - 1]);
        }
        return result;
    }
}
