package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class PickFromBothSides {
    public int solve(ArrayList<Integer> arr, int k) {
        for (int i = 1; i < arr.size(); i++) {
            arr.set(i, arr.get(i) + arr.get(i - 1));
        }
        if (k == arr.size()) return arr.get(arr.size() - 1);
        int ans = Integer.MIN_VALUE;
        for (int right = 0; right <= k; right++) {
            int left = k - right;
            if (right == 0) {
                ans = Math.max(ans, arr.get(left - 1));
            } else if (left == 0) {
                ans = Math.max(ans, arr.get(arr.size() - 1) - arr.get(arr.size() - right - 1));
            } else {
                ans = Math.max(ans, arr.get(left - 1) + arr.get(arr.size() - 1) - arr.get(arr.size() - right - 1));
            }
        }
        return ans;
    }
}
