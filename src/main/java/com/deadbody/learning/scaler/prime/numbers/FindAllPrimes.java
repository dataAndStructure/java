package com.deadbody.learning.scaler.prime.numbers;

import java.util.ArrayList;
import java.util.Arrays;

public class FindAllPrimes {
    public ArrayList<Integer> solve(int A) {
        boolean[] primes = new boolean[A + 1];
        Arrays.fill(primes, true);
        for (int i = 2; i * i <= A; i++) {
            if (!primes[i]) continue;
            for (int j = i * i; j <= A; j = j + i) {
                primes[j] = false;
            }
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 2; i <= A; i++) {
            if (primes[i]) ans.add(i);
        }
        return ans;
    }
}
