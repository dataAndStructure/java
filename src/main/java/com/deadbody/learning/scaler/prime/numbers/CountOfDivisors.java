package com.deadbody.learning.scaler.prime.numbers;

import java.util.ArrayList;

public class CountOfDivisors {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        int max = Integer.MIN_VALUE;
        for (Integer i : arr) {
            max = Integer.max(max, i);
        }
        int[] spf = new int[max + 1];
        for (int i = 0; i <= max; i++) {
            spf[i] = i;
        }
        for (int i = 2; i * i <= max; i++) {
            if (spf[i] == i) {
                for (int j = i * i; j <= max; j = j + i) {
                    spf[j] = Integer.min(spf[j], i);
                }
            }
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (Integer i : arr) {
            int val = 1;
            int s = spf[i];
            int count = 0;
            while (i > 1) {
                if (spf[i] == s) {
                    count++;
                    i = i / s;
                } else {
                    val = val * (count + 1);
                    s = spf[i];
                    count = 0;
                }
            }
            val = val * (count + 1);
            ans.add(val);
        }
        return ans;
    }
}
