package com.deadbody.learning.scaler.prime.numbers;

import java.util.ArrayList;
import java.util.Arrays;

public class PrimeSum {
    public ArrayList<Integer> primesum(int A) {
        boolean[] primes = new boolean[A + 1];
        Arrays.fill(primes, true);
        for (int i = 2; i * i <= A; i++) {
            if (primes[i]) {
                for (int j = i * i; j <= A; j = j + i) {
                    primes[j] = false;
                }
            }
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 2; i * 2 <= A; i++) {
            if (primes[i] && primes[A - i]) {
                ans.add(i);
                ans.add(A - i);
                break;
            }
        }
        return ans;
    }
}
