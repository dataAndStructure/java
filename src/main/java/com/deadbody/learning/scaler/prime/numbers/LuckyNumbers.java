package com.deadbody.learning.scaler.prime.numbers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LuckyNumbers {
    public int solve(int A) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i <= A; i++) {
            map.put(i, new ArrayList<>());
        }
        for (int i = 2; i * 2 <= A; i++) {
            if (map.get(i).isEmpty()) {
                for (int j = i + i; j <= A; j = j + i) {
                    map.get(j).add(i);
                }
            }
        }
        int count = 0;
        for (int i = 0; i <= A; i++) {
            if (map.get(i).size() == 2) count++;
        }
        return count;
    }
}
