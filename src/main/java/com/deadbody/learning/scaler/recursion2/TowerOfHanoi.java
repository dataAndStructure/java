package com.deadbody.learning.scaler.recursion2;

import java.util.ArrayList;

public class TowerOfHanoi {
    public ArrayList<ArrayList<Integer>> towerOfHanoi(int A) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        towerOfHanoi(A, 1, 2, 3, result);
        return result;
    }

    private void towerOfHanoi(int A, int s, int i, int d, ArrayList<ArrayList<Integer>> result) {
        if (A == 0) return;
        towerOfHanoi(A - 1, s, d, i, result);
        ArrayList<Integer> move = new ArrayList<>();
        move.add(A);
        move.add(s);
        move.add(d);
        result.add(move);
        towerOfHanoi(A - 1, i, s, d, result);
    }
}
