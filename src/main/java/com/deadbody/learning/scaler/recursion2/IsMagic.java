package com.deadbody.learning.scaler.recursion2;

public class IsMagic {
    public int solve(int A) {
        return calculate(A) == 1 ? 1 : 0;
    }

    private int calculate(int a) {
        if (a > 9) {
            int val = a % 10 + calculate(a / 10);
            return calculate(val);
        }
        return a;
    }
}
