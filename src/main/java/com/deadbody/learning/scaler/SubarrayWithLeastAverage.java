package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SubarrayWithLeastAverage {
    public int solve(ArrayList<Integer> arr, int k) {
        int n = arr.size();
        int sum = 0;
        int index = 0;
        for (int i = 0; i < k; i++) {
            sum += arr.get(i);
        }
        int ans = sum;
        for (int i = k; i < n; i++) {
            sum += arr.get(i);
            sum -= arr.get(i - k);
            if (sum < ans) {
                ans = sum;
                index = i - k + 1;
            }
        }
        return index;
    }
}
