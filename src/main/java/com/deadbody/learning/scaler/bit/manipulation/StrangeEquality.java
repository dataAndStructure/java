package com.deadbody.learning.scaler.bit.manipulation;

public class StrangeEquality {
    public int solve(int a) {
        int index = getLastIndex(a);
        int x = getSmallest(a);
        int y = 1 << (index);
        return (x ^ y);
    }

    private int getSmallest(int a) {
        int z = 0;
        int i = 0;
        while (a > 0) {
            int x = a % 2;
            if (x == 0) {
                z = z | (1 << i);
            }
            i++;
            a /= 2;
        }
        return z;
    }

    private int getLastIndex(int a) {
        int index = 0;
        while (a > 0) {
            index++;
            a /= 2;
        }
        return index;
    }
}
