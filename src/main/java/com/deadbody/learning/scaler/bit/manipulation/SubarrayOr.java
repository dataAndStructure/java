package com.deadbody.learning.scaler.bit.manipulation;

import java.util.ArrayList;

public class SubarrayOr {
    public int solve(ArrayList<Integer> arr) {
        long ans = 0L;
        int index = 0;
        while (index < 27) {
            int zeros = 0;
            int subArrays = getSubArrays(arr.size());
            for (Integer i : arr) {
                if ((i & (1 << index)) == 0) {
                    zeros++;
                } else {
                    subArrays -= getSubArrays(zeros);
                    zeros = 0;
                }
            }
            if (zeros > 0) {
                subArrays -= getSubArrays(zeros);
            }
            ans = ans + (1L << index) * subArrays;
            index++;
        }
        return (int) (ans % 1000000007);
    }

    private int getSubArrays(int n) {
        return (n * (n + 1)) / 2;
    }

    public int solveActual(ArrayList<Integer> A) {
        int n = A.size();
        int[] idx = new int[32];
        long ans = 0;
        for (int i = 1; i <= n; ++i) {
            long tmp = A.get(i - 1);
            for (int j = 0; j <= 31; ++j) {
                long pw = 1 << j;
                if ((tmp & pw) != 0) { //if jth bit is set
                    ans += pw * i; // add its contribution in ans for all subarrays ending at index i
                    idx[j] = i; // store the index for next elements
                } else if (idx[j] != 0) // if jth bit is not set
                {
                    ans += pw * idx[j]; // add its contribution in ans for all subarrays ending at index i using
                } // the information of last element having jth bit set
            }
        }
        return (int) (ans % 1000000007);
    }
}
