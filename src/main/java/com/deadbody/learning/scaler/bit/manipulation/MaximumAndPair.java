package com.deadbody.learning.scaler.bit.manipulation;

import java.util.ArrayList;

public class MaximumAndPair {
    public int solve(ArrayList<Integer> arr) {
        for (int i = 31; i >= 0; i--) {
            int count = 0;
            for (Integer integer : arr) {
                if ((integer & (1 << i)) > 0) {
                    count++;
                }
            }
            if (count < 2) {
                continue;
            }
            for (int j = 0; j < arr.size(); j++) {
                if ((arr.get(j) & (1 << i)) == 0) {
                    arr.set(j, 0);
                }
            }
        }
        int ans = 0;
        for (Integer i : arr) {
            if (i == 0) continue;
            if (ans == 0) ans |= i;
            else {
                ans &= i;
                break;
            }
        }
        return ans;

    }
}
