package com.deadbody.learning.scaler.bit.manipulation;

import java.util.ArrayList;

public class SumOfXorOfAllPairs {
    public int solve(ArrayList<Integer> arr) {
        long ans = 0L;
        for (int i = 0; i < 32; i++) {
            int zeros = 0;
            int once = 0;
            for (int val : arr) {
                if ((val & (1 << i)) == 0) {
                    zeros++;
                } else {
                    once++;
                }
            }
            ans += (long) zeros * once * (1 << i);
        }
        return (int) (ans % 1000000007);
    }
}
