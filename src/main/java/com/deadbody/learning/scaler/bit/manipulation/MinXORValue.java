package com.deadbody.learning.scaler.bit.manipulation;

import java.util.ArrayList;
import java.util.Collections;

public class MinXORValue {
    public int findMinXor(ArrayList<Integer> arr) {
        Collections.sort(arr);
        int ans = Integer.MAX_VALUE;
        for (int i = 0, j = 1; j < arr.size(); i++, j++) {
            ans = Integer.min(ans, arr.get(i) ^ arr.get(j));
        }
        return ans;
    }
}
