package com.deadbody.learning.scaler.bit.manipulation;

import java.util.ArrayList;

public class FindTwoMissingNumbers {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        int xorNumber = 0;
        for (Integer a : arr) {
            xorNumber ^= a;
        }
        for (int i = 1; i <= arr.size() + 2; i++) {
            xorNumber ^= i;
        }
        int index = 0;
        while (index < 32) {
            if ((xorNumber & (1 << index)) > 0) {
                break;
            }
            index++;
        }
        int xor1 = 0;//set bit
        int xor2 = 0;//unset bit
        for (Integer a : arr) {
            if ((a & (1 << index)) > 0) {
                xor1 ^= a;
            } else {
                xor2 ^= a;
            }
        }
        for (int i = 1; i <= arr.size() + 2; i++) {
            if ((i & (1 << index)) > 0) {
                xor1 ^= i;
            } else {
                xor2 ^= i;
            }
        }
        ArrayList<Integer> ans = new ArrayList<>();
        if (xor1 > xor2) {
            ans.add(xor2);
            ans.add(xor1);
        } else {
            ans.add(xor1);
            ans.add(xor2);
        }
        return ans;
    }
}
