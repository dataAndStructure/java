package com.deadbody.learning.scaler;

public class FindingGoodDays {
    public int solve(int a) {
        int ans = 0;
        while (a > 0) {
            if (a % 2 == 1) {
                ans++;
            }
            a /= 2;
        }
        return ans;
    }
}
