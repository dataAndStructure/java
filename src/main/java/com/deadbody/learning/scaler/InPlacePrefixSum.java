package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class InPlacePrefixSum {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        int n = arr.size();
        if (n < 2) return arr;
        for (int i = 1; i < n; i++) {
            arr.set(i, arr.get(i - 1) + arr.get(i));
        }
        return arr;
    }
}
