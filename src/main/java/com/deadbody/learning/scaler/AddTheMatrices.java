package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class AddTheMatrices {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> A, ArrayList<ArrayList<Integer>> B) {
        int r = A.size();
        int c = B.get(0).size();
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < r; i++) {
            ArrayList<Integer> row = new ArrayList<>();
            for (int j = 0; j < c; j++) {
                row.add(0);
            }
            ans.add(row);
        }
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                ans.get(i).set(j, A.get(i).get(j) + B.get(i).get(j));
            }
        }
        return ans;
    }
}
