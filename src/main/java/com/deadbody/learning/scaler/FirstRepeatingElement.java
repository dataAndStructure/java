package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirstRepeatingElement {
    public int solve(ArrayList<Integer> arr) {
        int n = arr.size();
        Map<Integer, Integer> set = new HashMap<>();
        int minIndex = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            Integer num = arr.get(i);
            if (set.containsKey(num)) {
                minIndex = Integer.min(minIndex, set.get(num));
            }
            set.put(num, i);
        }
        return minIndex == Integer.MAX_VALUE ? -1 : arr.get(minIndex);
    }
}
