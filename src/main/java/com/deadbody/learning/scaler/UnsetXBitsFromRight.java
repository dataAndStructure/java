package com.deadbody.learning.scaler;

public class UnsetXBitsFromRight {
    public Long solve(Long a, int b) {
        long c = 0;
        b--;
        while (b >= 0) {
            long x = 1L << b;
            c += x;
            b--;
        }
        c = ~c;
        return a & c;
    }
}
