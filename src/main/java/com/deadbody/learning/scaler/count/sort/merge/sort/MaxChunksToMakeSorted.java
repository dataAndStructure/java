package com.deadbody.learning.scaler.count.sort.merge.sort;

import java.util.ArrayList;

public class MaxChunksToMakeSorted {
    public int solve(ArrayList<Integer> arr) {
        int max = 0;
        int counter = 0;
        for (int i = 0; i < arr.size(); i++) {
            int x = arr.get(i);
            max = Integer.max(max, x);
            if (max == i) {
                counter++;
            }
        }
        return counter;
    }
}
