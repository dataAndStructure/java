package com.deadbody.learning.scaler.count.sort.merge.sort;

import java.util.ArrayList;

public class CountSort {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        int[] count = new int[100001];
        for (Integer i : arr) {
            count[i]++;
        }
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 0; i < 100001; i++) {
            for (int j = 0; j < count[i]; j++) {
                ans.add(i);
            }
        }
        return ans;
    }
}
