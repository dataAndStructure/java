package com.deadbody.learning.scaler.count.sort.merge.sort;

import java.util.ArrayList;
import java.util.List;

public class MergeTwoSortedArrays {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public ArrayList<Integer> solve(final List<Integer> arr1, final List<Integer> arr2) {
        ArrayList<Integer> ans = new ArrayList<>();
        int m = arr1.size();
        int n = arr2.size();
        int i = 0;
        int j = 0;
        while (i < m && j < n) {
            if (arr1.get(i) < arr2.get(j)) {
                ans.add(arr1.get(i));
                i++;
            } else {
                ans.add(arr2.get(j));
                j++;
            }
        }
        while (i < m) {
            ans.add(arr1.get(i));
            i++;
        }
        while (j < n) {
            ans.add(arr2.get(j));
            j++;
        }
        return ans;
    }
}
