package com.deadbody.learning.scaler.count.sort.merge.sort;

import java.util.ArrayList;

public class InversionCountInAnArray {

    public int solve(ArrayList<Integer> arr) {
        IntegerBoxer i = new IntegerBoxer();
        mergeSort(arr, 0, arr.size() - 1, i);
        return i.i;
    }

    private void mergeSort(ArrayList<Integer> arr, int s, int e, IntegerBoxer i) {
        if (s >= e) return;
        int m = (s + e) / 2;
        mergeSort(arr, s, m, i);
        mergeSort(arr, m + 1, e, i);
        merge(arr, s, m, e, i);
    }

    private void merge(ArrayList<Integer> arr, int s, int m, int e, IntegerBoxer x) {
        ArrayList<Integer> first = getList(arr, s, m);
        ArrayList<Integer> second = getList(arr, m + 1, e);

        int i = 0;
        int j = 0;
        while (i < first.size() && j < second.size()) {
            if (first.get(i) <= second.get(j)) {
                arr.set(s, first.get(i));
                i++;
            } else {
                arr.set(s, second.get(j));
                j++;
                int MOD = 1000000007;
                x.i = ((x.i % MOD) + (first.size() % MOD) - (i % MOD)) % MOD;
            }
            s++;
        }
        while (i < first.size()) {
            arr.set(s, first.get(i));
            i++;
            s++;
        }
        while (j < second.size()) {
            arr.set(s, second.get(j));
            j++;
            s++;
        }
    }

    private ArrayList<Integer> getList(ArrayList<Integer> arr, int s, int e) {
        ArrayList<Integer> subArray = new ArrayList<>();
        for (int i = s; i <= e; i++) {
            subArray.add(arr.get(i));
        }
        return subArray;
    }
}

class IntegerBoxer {
    public Integer i = 0;
}