package com.deadbody.learning.scaler;

public class AddBinaryString {
    public String addBinary(String a, String b) {
        int aLength = a.length() - 1;
        int bLength = b.length() - 1;
        int carry = 0;
        StringBuilder stringBuilder = new StringBuilder();
        while (aLength > -1 && bLength > -1) {
            int aInt = a.charAt(aLength--) == '1' ? 1 : 0;
            int bInt = b.charAt(bLength--) == '1' ? 1 : 0;
            int sum = (aInt + bInt + carry) % 2;
            carry = (aInt + bInt + carry) / 2;
            stringBuilder.insert(0, sum);
        }
        while (aLength > -1) {
            int aInt = a.charAt(aLength--) == '1' ? 1 : 0;
            int sum = (aInt + carry) % 2;
            carry = (aInt + carry) / 2;
            stringBuilder.insert(0, sum);
        }
        while (bLength > -1) {
            int bInt = b.charAt(bLength--) == '1' ? 1 : 0;
            int sum = (bInt + carry) % 2;
            carry = (bInt + carry) / 2;
            stringBuilder.insert(0, sum);
        }
        if (carry == 1) stringBuilder.insert(0, carry);
        return stringBuilder.toString();
    }
}
