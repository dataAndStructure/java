package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class NobleIntegerSorting {
    public int solve(ArrayList<Integer> arr) {
        arr.sort(Collections.reverseOrder());
        int ans = 0;
        if (arr.get(0) == 0) ans++;
        int size = arr.size();
        for (int i = 1; i < size; i++) {
            if (arr.get(i) == i && !Objects.equals(arr.get(i - 1), arr.get(i))) ans++;
        }
        return ans == 0 ? -1 : ans;
    }
}
