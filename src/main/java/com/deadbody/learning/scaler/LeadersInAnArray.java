package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class LeadersInAnArray {
    public ArrayList<Integer> solve(ArrayList<Integer> arr) {
        int n = arr.size();
        ArrayList<Integer> integers = new ArrayList<>();
        int ans = arr.get(n - 1);
        integers.add(ans);
        for (int i = n - 2; i >= 0; i--) {
            if (ans < arr.get(i)) {
                ans = arr.get(i);
                integers.add(ans);
            }
        }
        return integers;
    }
}
