package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class NextPermutation {
    public ArrayList<Integer> nextPermutation(ArrayList<Integer> arr) {
        int ind = -1;
        for (int i = arr.size() - 2; i >= 0; i--) {
            if (arr.get(i) < arr.get(i + 1)) {
                ind = i;
                break;
            }
        }
        if (ind == -1) {
            reverse(arr, 0, arr.size() - 1);
            return arr;
        }
        for (int i = arr.size() - 1; i > ind; i--) {
            if (arr.get(i) > arr.get(ind)) {
                int temp = arr.get(i);
                arr.set(i, arr.get(ind));
                arr.set(ind, temp);
                break;
            }
        }
        reverse(arr, ind + 1, arr.size() - 1);
        return arr;
    }

    private void reverse(ArrayList<Integer> arr, int start, int end) {
        while (start < end) {
            int temp = arr.get(start);
            arr.set(start, arr.get(end));
            arr.set(end, temp);
            start++;
            end--;
        }
    }
}
