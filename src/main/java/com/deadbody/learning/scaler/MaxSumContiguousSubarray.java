package com.deadbody.learning.scaler;

import java.util.List;

public class MaxSumContiguousSubarray {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int maxSubArray(final List<Integer> arr) {
        int maxSum = Integer.MIN_VALUE;
        int currentSum = 0;
        for (Integer i : arr) {
            currentSum += i;
            maxSum = Integer.max(currentSum, maxSum);
            if (currentSum < 0) currentSum = 0;
        }
        return maxSum;
    }
}
