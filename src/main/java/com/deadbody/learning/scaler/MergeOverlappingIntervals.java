package com.deadbody.learning.scaler;

import com.deadbody.learning.util.Interval;

import java.util.ArrayList;

public class MergeOverlappingIntervals {
    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
        int n = intervals.size();
        if (n == 1) return intervals;
        intervals.sort((o1, o2) -> (o1.start - o2.start == 0) ? (o1.end - o2.end) : (o1.start - o2.start));
        ArrayList<Interval> ans = new ArrayList<>();
        Interval current = intervals.get(0);
        for (int i = 1; i < n; i++) {
            Interval interval = intervals.get(i);
            if ((current.start <= interval.start && current.end >= interval.start) || (current.start <= interval.end && current.end >= interval.end)) {
                current.start = Integer.min(current.start, interval.start);
                current.end = Integer.max(current.end, interval.end);
            } else {
                ans.add(current);
                current = interval;
            }
        }
        ans.add(current);
        return ans;
    }
}
