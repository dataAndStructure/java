package com.deadbody.learning.scaler;

public class StringOperations {
    public String solve(String s) {
        //Concat String
        String in = s + s;
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < in.length(); i++) {
            char c = in.charAt(i);
            //Remove capital letters
            if (c >= 'A' && c <= 'Z') continue;
            //Replace vowel with #
            switch (c) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    ans.append('#');
                    break;
                default:
                    ans.append(c);
                    break;
            }
        }
        return ans.toString();
    }
}
