package com.deadbody.learning.scaler;

import java.util.ArrayList;
import java.util.Collections;

public class ArithmeticProgression {
    public int solve(ArrayList<Integer> arr) {
        Collections.sort(arr);
        int d = arr.get(1) - arr.get(0);
        int size = arr.size();
        for (int i = 2; i < size; i++) {
            if ((arr.get(i) - arr.get(i - 1)) != d) return 0;
        }
        return 1;
    }
}
