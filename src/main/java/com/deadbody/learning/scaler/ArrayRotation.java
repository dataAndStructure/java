package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class ArrayRotation {
    public ArrayList<Integer> solve(ArrayList<Integer> arr, int k) {
        int n = arr.size();
        k = k % n;
        if (k == 0) return arr;

        rotate(arr, 0, arr.size() - 1);
        rotate(arr, 0, k - 1);
        rotate(arr, k, arr.size() - 1);

        return arr;
    }

    private void rotate(ArrayList<Integer> arr, int s, int e) {
        while (s < e) {
            int temp = arr.get(s);
            arr.set(s, arr.get(e));
            arr.set(e, temp);
            s++;
            e--;
        }
    }
}
