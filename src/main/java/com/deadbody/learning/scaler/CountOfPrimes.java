package com.deadbody.learning.scaler;

public class CountOfPrimes {
    public int solve(int n) {
        int count = 0;
        for (int i = 2; i <= n; i++) {
            count++;
            for (int j = 2; j * j <= i; j++) {
                if (i % j == 0) {
                    count--;
                    break;
                }
            }
        }
        return count;
    }
}
