package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class RowToColumnZero {
    public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> arr) {
        int row = arr.size();
        int col = arr.get(0).size();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (arr.get(i).get(j) == 0) {
                    for (ArrayList<Integer> arrayList : arr) {
                        if (arrayList.get(j) > 0) arrayList.set(j, -arrayList.get(j));
                    }
                    for (int j1 = 0; j1 < col; j1++) {
                        if (arr.get(i).get(j1) > 0) arr.get(i).set(j1, -arr.get(i).get(j1));
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (arr.get(i).get(j) < 0) {
                    arr.get(i).set(j, 0);
                }
            }
        }

        return arr;
    }
}
