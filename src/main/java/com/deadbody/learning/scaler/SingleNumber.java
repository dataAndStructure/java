package com.deadbody.learning.scaler;

import java.util.List;

public class SingleNumber {
    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public int singleNumber(final List<Integer> arr) {
        int ans = 0;
        for (Integer integer : arr) {
            ans ^= integer;
        }
        return ans;
    }
}
