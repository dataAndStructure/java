package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class ColumnSum {
    public ArrayList<Integer> solve(ArrayList<ArrayList<Integer>> arr) {
        int r = arr.size();
        int c = arr.get(0).size();
        ArrayList<Integer> result = new ArrayList<>();
        for (int j = 0; j < c; j++) {
            int sum = 0;
            for (ArrayList<Integer> arrayList : arr) {
                sum += arrayList.get(j);
            }
            result.add(sum);
        }
        return result;
    }
}
