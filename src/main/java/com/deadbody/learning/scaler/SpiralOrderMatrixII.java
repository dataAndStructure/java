package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class SpiralOrderMatrixII {
    public ArrayList<ArrayList<Integer>> generateMatrix(int a) {
        ArrayList<ArrayList<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            ArrayList<Integer> arr = new ArrayList<>();
            for (int j = 0; j < a; j++) {
                arr.add(0);
            }
            ans.add(arr);
        }

        int rowStart = 0;
        int colStart = 0;
        int rowEnd = a - 1;
        int colEnd = a - 1;
        int value = 1;

        while (rowStart < rowEnd && colStart < colEnd) {
            for (int i = colStart; i < colEnd; i++) {
                ans.get(rowStart).set(i, value++);
            }
            for (int i = rowStart; i < rowEnd; i++) {
                ans.get(i).set(colEnd, value++);
            }
            for (int i = colEnd; i > colStart; i--) {
                ans.get(rowEnd).set(i, value++);
            }
            for (int i = rowEnd; i > rowStart; i--) {
                ans.get(i).set(colStart, value++);
            }
            rowStart++;
            rowEnd--;
            colStart++;
            colEnd--;
        }

        if ((a & 1) == 1) ans.get(a / 2).set(a / 2, value);

        return ans;
    }
}
