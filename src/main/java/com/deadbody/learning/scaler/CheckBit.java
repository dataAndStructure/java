package com.deadbody.learning.scaler;

public class CheckBit {
    public int solve(int a, int b) {
        int x = 1 << b;
        return ((a & x) == 0) ? 0 : 1;
    }
}
