package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class EquilibriumIndexOfAnArray {
    public int solve(ArrayList<Integer> arr) {
        int n = arr.size();
        if (n == 0) return -1;
        if (n == 1 && arr.get(0) == 0) return 0;
        if (n == 1 && arr.get(0) != 0) return -1;
        long[] prefix = new long[n];
        prefix[0] = arr.get(0);
        for (int i = 1; i < n; i++) {
            prefix[i] = prefix[i - 1] + arr.get(i);
        }
        for (int i = 0; i < n; i++) {
            long sumLeft = i == 0 ? 0 : prefix[i - 1];
            long sumRight = i == (n - 1) ? 0 : prefix[n - 1] - prefix[i];
            if (sumRight == sumLeft) return i;
        }
        return -1;
    }
}
