package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class MaxMinSumOfAnArray {
    public int solve(ArrayList<Integer> arr) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i : arr) {
            min = Integer.min(min, i);
            max = Integer.max(max, i);
        }

        return max + min;
    }
}
