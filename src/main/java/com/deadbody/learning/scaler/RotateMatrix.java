package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class RotateMatrix {
    public void solve(ArrayList<ArrayList<Integer>> arr) {
        int r = arr.size();
        int c = arr.get(0).size();

        for (int i = 0; i < r; i++) {
            for (int j = i + 1; j < c; j++) {
                int temp = arr.get(i).get(j);
                arr.get(i).set(j, arr.get(j).get(i));
                arr.get(j).set(i, temp);
            }
        }
        for (int i = 0; i < r; i++) {
            for (int s = 0, e = c - 1; s < e; s++, e--) {
                int temp = arr.get(i).get(s);
                arr.get(i).set(s, arr.get(i).get(e));
                arr.get(i).set(e, temp);
            }
        }
    }
}
