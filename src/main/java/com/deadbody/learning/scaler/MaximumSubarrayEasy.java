package com.deadbody.learning.scaler;

import java.util.ArrayList;

public class MaximumSubarrayEasy {
    public int maxSubarray(int n, int maxSum, ArrayList<Integer> arr) {
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = i; j < n; j++) {
                sum += arr.get(j);
                ans = sum > maxSum ? ans : Math.max(ans, sum);
            }
        }
        return ans == Integer.MIN_VALUE ? 0 : ans;
    }
}
