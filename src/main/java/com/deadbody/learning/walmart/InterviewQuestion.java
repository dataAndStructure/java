//package com.deadbody.learning.walmart;
//
///*
//matrix[][] = { {1, 7, 9},
//                    {14, 26, 28},
//                    {36, 39, 43} }
//
//matrix[][] = {{1,7,9,10,12},
//              {14,26,28,30,31}} 28
//
//N = 39
//Ans  (2,1)
//n*n nlogn
//n*m nlogm
// */
//
//public class InterviewQuestion {
//    void solution(int[][] input, int num) {
//        int n = input.length;
//        int m = input[0].length;
//
//        int breakN = -1;
//        int breakM = -1;
//
//        for (int i = 0; i < n; i++) {
//            if (i >= m) {
//                break;
//            }
//            if (input[i][i] >= num) {
//                breakM = i;
//                breakN = i;
//                break;
//            }
//        }
//
//        if (input[0][breakM] <= num) {
//            binarySearch(input[0][breakM], input[breakN][breakM]);
//            return;
//        }
//        if (num <= input[n - 1][m - 1]) {
//            binarySearch(input[breakN - 1][breakM - 1], input[n - 1][breakM - 1]);
//            return;
//        }
//        if (breakM == -1 && breakN == -1) {
//            if (m > n) {
//                binarySearch(input[n][n], input[n][m]);
//            }
//        }
//    }
//    /*
//    input[][] = {{1,7,9,10,12},
//              {14,26,28,30,31}} num=12
//              n =2
//              m=5
//              breakM = -1
//              breakN = -1
//     */
//}
