package com.deadbody.learning.walmart;

public class InterviewQues2 {
    /*
    1->2->3->4->5
    delete 3
    1->2->3->4->5->6
    delete 4

     */

    void solution(Node head) {
        Node fast = head;
        Node slow = head;
        Node preSlow = slow;
        while (fast.next != null && fast.next.next != null) {
            preSlow = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        //odd case
        if (fast.next == null) {
            preSlow.next = slow.next;
        }
        //even case
        if (fast.next.next == null) {
            slow.next = slow.next.next;
        }
    }
}

class Node {
    public int data;
    public Node next;

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }
}
