package com.deadbody.learning.kredx;

public class Problem1 {
    /*
    Full Binary Tree
    Every Node will have left right and parent
    Any Node as an input
    Return the node immediatly right to it at the same level.
     */
    /*
    1
    2 3
    4 5 6 7

    Input 2
    Output 3

    Input 5
    Output 6

    Input 3
    Output null
     */

    public static void main(String[] args) {
        TreeNode node7 = new TreeNode(7, null, null);
        TreeNode node6 = new TreeNode(6, null, null);
        TreeNode node5 = new TreeNode(5, null, null);
        TreeNode node4 = new TreeNode(4, null, null);

        TreeNode node3 = new TreeNode(3, node6, node7);
        node6.parent = node3;
        node7.parent = node3;
        TreeNode node2 = new TreeNode(2, node4, node5);
        node4.parent = node2;
        node5.parent = node2;

        TreeNode node1 = new TreeNode(1, node2, node3);
        node2.parent = node1;
        node3.parent = node1;
        node1.parent = null;

        Problem1 problem1 = new Problem1();
        System.out.println("Input 2 " + getData(problem1.getImmediateRight(node2))); // 3
        System.out.println("Input 5 " + getData(problem1.getImmediateRight(node5))); // 6
        System.out.println("Input 3 " + getData(problem1.getImmediateRight(node3))); // null
    }

    private static String getData(TreeNode node) {
        if (node == null)
            return "NULL";
        return Integer.toString(node.data);
    }

    public TreeNode getImmediateRight(TreeNode node) {
        if (node == null)
            return null;
        if (node.parent.left == node) {
            return node.parent.right;
        }
        return getImmediateRightForRight(node);
    }

    private TreeNode getImmediateRightForRight(TreeNode node) {
        TreeNode current = node;
        int level = 0;
        while (true) {
            if (current.parent == null) {
                current = null;
                break;
            }
            if (current.parent.left == current) {
                break;
            }
            level++;
            current = current.parent;
        }
        if (current == null)
            return null;
        current = current.parent.right;
        while (level > 0) {
            current = current.left;
            level--;
        }
        return current;
    }
}

class TreeNode {
    public int data;
    public TreeNode left;
    public TreeNode right;
    public TreeNode parent;

    public TreeNode(int data, TreeNode left, TreeNode right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }
}