package com.deadbody.learning.ethos;
/*
 * Click `Run` to execute the snippet below!
 */

import java.util.LinkedList;
import java.util.Queue;

/*

We are in a room and the room contains virus, we need to escape as soon as possible. Given a coordinate indicate where our start location in the room coordinate: [x, y] We can move up, down, left, right in each step, each step will use exactly one second.
And this room has wall that we cannot go through please find out if we could escape from the room, and the min seconds we need to escape from the room (if we could escape).


Input:

Room :

   [ ["+","+",".","+"],

     [".",".",".","+"],

     ["+","+","+","."] ]
start coordinate [1,2]

'+' means wall. '.' mains empty space we can pass; You escape the room when you reach a "." on the edge of the grid. If there is no solution, return -1. If the starting poisition is a "+", return -1.


**/

public class FirstRound {
    public static void main(String[] args) {
        char[][] room = {
                {'+', '+', '.', '+'},
                {'.', '.', '.', '+'},
                {'+', '+', '+', '.'}
        };

        boolean[][] visited = new boolean[room.length][room[0].length];

        int startI = 1;
        int startJ = 2;


        if (startI == 0 || startJ == 0) {
            System.out.print(0);
            return;
        }

        if (room[startI][startJ] == '+') {
            System.out.print(-1);
            return;
        }


        Queue<Pos> queue = new LinkedList<>();
        queue.add(new Pos(startJ, startI, 0));

        int solution = Integer.MAX_VALUE;

        while (!queue.isEmpty()) {
            Pos data = queue.poll();
            visited[data.j()][data.i()] = true;
            if (endOfMaze(data)) {
                solution = Math.min(solution, data.d());
            } else {
                right(data, room, visited, queue);
                up(data, room, visited, queue);
                left(data, room, visited, queue);
                down(data, room, visited, queue);
            }
        }
        System.out.print(solution == Integer.MAX_VALUE ? -1 : solution);
    }

    private static void down(Pos data, char[][] room, boolean[][] visited, Queue<Pos> queue) {
        if (moveDown(data, room, visited)) {
            queue.add(new Pos(data.i(), data.j() + 1, data.d() + 1));
        }
    }

    private static void left(Pos data, char[][] room, boolean[][] visited, Queue<Pos> queue) {
        if (moveLeft(data, room, visited)) {
            queue.add(new Pos(data.i() - 1, data.j(), data.d() + 1));
        }
    }

    private static void up(Pos data, char[][] room, boolean[][] visited, Queue<Pos> queue) {
        if (moveUp(data, room, visited)) {
            queue.add(new Pos(data.i(), data.j() - 1, data.d() + 1));
        }
    }

    private static void right(Pos data, char[][] room, boolean[][] visited, Queue<Pos> queue) {
        if (moveRight(data, room, visited)) {
            queue.add(new Pos(data.i() + 1, data.j(), data.d() + 1));
        }
    }

    private static boolean moveDown(Pos data, char[][] room, boolean[][] visited) {
        return data.j() != room.length - 1 && room[data.j() + 1][data.i()] != '+' && !(visited[data.j() + 1][data.i()]);
    }

    private static boolean moveUp(Pos data, char[][] room, boolean[][] visited) {
        return data.j() != 0 && room[data.j() - 1][data.i()] != '+' && !(visited[data.j() - 1][data.i()]);
    }

    private static boolean moveRight(Pos data, char[][] room, boolean[][] visited) {
        return data.i() != room[0].length - 1 && room[data.j()][data.i() + 1] != '+' && !(visited[data.j()][data.i() + 1]);
    }

    private static boolean moveLeft(Pos data, char[][] room, boolean[][] visited) {
        return data.i() != 0 && room[data.j()][data.i() - 1] != '+' && !(visited[data.j()][data.i() - 1]);
    }

    private static boolean endOfMaze(Pos data) {
        return data.i() == 0 || data.j() == 0;
    }
}

record Pos(int i, int j, int d) {
}

