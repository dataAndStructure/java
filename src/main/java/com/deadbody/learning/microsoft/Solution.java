package com.deadbody.learning.microsoft;
//
//To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel
// of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the
// same color), and so on. Replace the color of all of the aforementioned pixels with color.
//
//
//        Example 1:
//Input:
//        [
//        [1,0,1],
//        [0,1,0],
//        [1,0,1]
//        ]
//sr = 1, sc = 1, color = 2
//
//Output: [
//        [1,0,1]
//        [0,2,0],
//        [1,0,1]
//        ]
//
//Example 2:
//
//Input: image =
//        [
//        [0,0,0],
//        [0,0,0]
//        ], sr = 0, sc = 0, color = 0
//
//Output:
//        [
//        [0,0,0],
//        [0,0,0]
//        ]


import java.util.LinkedList;
import java.util.Queue;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[][] image = {{1, 1, 1}, {1, 1, 0}, {1, 0, 1}};
        int[][] result = solution.floodFill(image, 0, 0, 2);
        for (int[] ints : result) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int init = image[sr][sc];
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{sr, sc});
        int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        while (!queue.isEmpty()) {
            int[] current = queue.poll();
            image[current[0]][current[1]] = color;
            for (int[] direction : directions) {
                int nr = current[0] + direction[0];
                int nc = current[1] + direction[1];

                if (nr >= image.length || nr < 0) {
                    continue;
                }

                if (nc >= image[0].length || nc < 0) {
                    continue;
                }

                if (image[nr][nc] == init) {
                    queue.add(new int[]{nr, nc});
                }
            }
        }
        return image;
    }
}
