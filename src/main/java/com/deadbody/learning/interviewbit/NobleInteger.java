package com.deadbody.learning.interviewbit;

import java.util.Collections;
import java.util.List;

/**
 * Given an array arr[], find a Noble integer in it.
 * An integer x is said to be Noble in arr[] if the
 * number of integers greater than x are equal to x.
 */

public class NobleInteger {

    public int solveIfExists(List<Integer> arr) {
        Collections.sort(arr);
        int length = arr.size();
        for (int i = 0; i < arr.size() - 1; i++) {
            if (arr.get(i) == arr.get(i + 1)) {
                length--;
                continue;
            }
            if (arr.get(i) == length - 1) {
                return 1;
            }
            length--;
        }
        if (arr.get(arr.size() - 1) == 0)
            return 1;
        return -1;
    }

    public int solveReturnNumber(List<Integer> arr) {
        Collections.sort(arr);
        int length = arr.size();
        for (int i = 0; i < arr.size() - 1; i++) {
            if (arr.get(i) == arr.get(i + 1)) {
                length--;
                continue;
            }
            if (arr.get(i) == length - 1) {
                return arr.get(i);
            }
            length--;
        }
        if (arr.get(arr.size() - 1) == 0)
            return 0;
        return -1;
    }

}
