package com.deadbody.learning.interviewbit;

import java.util.List;

public class PlusOne {

    public List<Integer> plusOne(List<Integer> integers) {
        for (int i = integers.size() - 1; i >= 0; i--) {
            if (integers.get(i) + 1 > 9) {
                if (i == 0 && integers.get(i) + 1 > 9) {
                    integers.set(i, 0);
                    integers.add(0, 1);
                } else {
                    integers.set(i, 0);
                }
            } else {
                integers.set(i, integers.get(i) + 1);
                break;
            }
        }
        int j = 0;
        while (j == 0) {
            if (integers.get(j) == 0) {
                integers.remove(j);
            } else
                j++;
        }
        return integers;
    }

}
