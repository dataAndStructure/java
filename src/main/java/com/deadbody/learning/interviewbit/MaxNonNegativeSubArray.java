package com.deadbody.learning.interviewbit;

import java.util.ArrayList;
import java.util.List;

public class MaxNonNegativeSubArray {
    public List<Integer> getMaxSubList(List<Integer> integers) {

        List<Integer> currentSubList = new ArrayList<>();
        int currentSum = 0;

        List<Integer> maxSubList = new ArrayList<>();
        int maxSum = 0;

        for (int i : integers) {
            if (i < 0) {
                currentSubList = new ArrayList<>();
                currentSum = 0;
            } else {
                currentSubList.add(i);
                currentSum += i;
                if (currentSum > maxSum || (currentSum == maxSum && currentSubList.size() > maxSubList.size())) {
                    maxSubList = currentSubList;
                    maxSum = currentSum;
                }
            }
        }

        return maxSubList;
    }
}
