package com.deadbody.learning.interviewbit;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MaxSpecialProduct {

    public int maxSpecialProduct(List<Integer> arr) {
        List<Integer> left = firstGen(arr, true);
        List<Integer> right = firstGen(arr, false);
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < left.size(); i++) {
            max = Math.max(max, left.get(i) * right.get(i));
        }
        return max % 1000000007;
    }

    private List<Integer> firstGen(List<Integer> arr, boolean flag) {
        Stack<Integer> stack = new Stack<>();
        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            ans.add(0);
        }

        List<Integer> it = init(arr.size(), flag);

        for (Integer i : it) {
            while (!stack.isEmpty() && arr.get(i) > arr.get(stack.peek())) {
                stack.pop();
            }
            ans.set(i, stack.isEmpty() ? 0 : stack.peek());
            stack.push(i);
        }
        return ans;
    }

    private List<Integer> init(int size, boolean flag) {
        List<Integer> it = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            if (flag) {
                it.add(i);
            } else {
                it.add(size - i - 1);
            }
        }

        return it;
    }

}
