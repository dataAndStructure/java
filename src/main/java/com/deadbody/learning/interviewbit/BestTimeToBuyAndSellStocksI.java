package com.deadbody.learning.interviewbit;

public class BestTimeToBuyAndSellStocksI {
    public int maxProfit(final int[] a) {
        if (a.length == 0)
            return 0;
        int buyprice = a[0];
        int maxProfit = 0;

        for (int i = 1; i < a.length; i++) {
            if (buyprice > a[i])
                buyprice = a[i];
            if (maxProfit < (a[i] - buyprice)) {
                maxProfit = a[i] - buyprice;
            }
        }
        return maxProfit;
    }
}
