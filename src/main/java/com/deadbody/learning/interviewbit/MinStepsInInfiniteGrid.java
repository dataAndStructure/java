package com.deadbody.learning.interviewbit;

import java.util.List;

public class MinStepsInInfiniteGrid {

    public int getMinSteps(List<Integer> X, List<Integer> Y) {
        int sum = 0;

        for (int i = 1; i < X.size(); i++) {
            sum += Math.max(Math.abs(Y.get(i) - Y.get(i - 1)), Math.abs(X.get(i) - X.get(i - 1)));
        }

        return sum;
    }
}
