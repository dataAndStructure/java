package com.deadbody.learning.ShadowFox;

import com.deadbody.learning.sorting.QuickSort;

public class SumOfTripletsIsK {
    public static void main(String[] args) {
        SumOfTripletsIsK sumOfTripletsIsK = new SumOfTripletsIsK();
        sumOfTripletsIsK.solution();
    }

    public void solution() {
        int[] arr = {3, 7, 1, 9, 11, 3};
        int k = 15;
        QuickSort quickSort = new QuickSort();
        quickSort.quickSort(arr, 0, arr.length - 1, QuickSort.SortingType.HOARE);

        for (int i = 0; i < arr.length - 2; i++) {
            int l = i + 1;
            int r = arr.length - 1;
            while (l < r) {
                if ((arr[i] + arr[l] + arr[r]) == k) {
                    System.out.println(arr[i] + "," + arr[l] + "," + arr[r]);
                    l++;
                    r--;
                }
                if ((arr[i] + arr[l] + arr[r]) < k) {
                    l++;
                }
                if ((arr[i] + arr[l] + arr[r]) > k) {
                    r--;
                }
            }
        }
    }
}
