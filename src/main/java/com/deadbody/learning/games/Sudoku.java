package com.deadbody.learning.games;

public class Sudoku {

    public void solveWithBackTrack() {

    }

    private boolean checkValidRow(int num, int[][] board, Pos p) {
        for (int it = 0; it < 10; it++) {
            if (it == p.i)
                continue;
            if (board[p.j][it] == num)
                return false;
        }
        return true;
    }

    private boolean checkValidCol(int num, int[][] board, Pos p) {
        for (int it = 0; it < 10; it++) {
            if (it == p.i)
                continue;
            if (board[it][p.i] == num)
                return false;
        }
        return true;
    }

    private boolean checkValidCube(int num, int[][] board, Pos p) {
        int cube_i = p.i / 3;
        int cube_j = p.j / 3;

        for (int j = cube_j; j < (cube_j + 3); j++) {
            for (int i = cube_i; i < (cube_i + 3); i++) {
                if (i == p.i && j == p.j)
                    continue;
                if (board[j][i] == num)
                    return false;
            }
        }
        return true;
    }

    private void printSudoku(int[][] board) {
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                System.out.print(board[i][j] + " ");
                if (i == 2 || i == 5 || i == 8)
                    System.out.print("|");
                if (i == 8)
                    System.out.println();
            }
            if (j == 2 || j == 5 || j == 8)
                System.out.println("------------------");
        }
    }

    private record Pos(int i, int j) {
    }
}
