package com.deadbody.learning;

public class UpStox {
    /*
    Kth largest integer
    1,2,3,4,5,6 = n
    k=2
    ans 5

    K = [6,5] K*N

    MinHeap(K) ->

     */

    /*
    1. Web servers
    2. Database
     */

    /*
    hostname:8080/health - 5 ping fail alert / 1 ping alert
     */

    /*
    T1 -> R1
    T2 -> R2

     */


}

class ResourceOne {
    private static ResourceOne resourceOne;

    private ResourceOne() {
    }

    public static synchronized ResourceOne getInstance() {
        if (resourceOne == null) {
            resourceOne = new ResourceOne();
        }
        return resourceOne;
    }
}

class ResourceTwo {
    private static ResourceTwo resourceTwo;

    private ResourceTwo() {
    }

    public static synchronized ResourceTwo getInstance() {
        if (resourceTwo == null) {
            resourceTwo = new ResourceTwo();
        }
        return resourceTwo;
    }
}


class ThreadOne implements Runnable {

    @Override
    public void run() {
        ResourceOne resourceOne = ResourceOne.getInstance();
        ResourceTwo resourceTwo = ResourceTwo.getInstance();
    }
}


class ThreadTwo implements Runnable {

    @Override
    public void run() {
        ResourceTwo resourceTwo = ResourceTwo.getInstance();
        ResourceOne resourceOne = ResourceOne.getInstance();
    }
}
