package com.deadbody.learning.cleartax;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

enum Language {
    JAVA
}

enum Type {
    MULTI_LINE_START, MULTI_LINE_END, SINGLE_LINE_COMMENT, BLANK_LINE
}

public class Problem1 {
    /*
    Total number of lines in the file
    Total Number of lines of code
    Total number of blank lines
    Total number of single line comments
    Total number of multiline comments
     */

    private final Language language;
    private int totalNumberOfLines;
    private int lineOfCode;
    private int blankLine;
    private int singleLineComment;
    private int multiLineComment;
    //SINGLE COMMENT
    private boolean insideMultiLineBlock;

    public Problem1(Language language) {
        this.language = language;
    }

    public static void main(String[] args) throws IOException {
        Problem1 problem1 = new Problem1(Language.JAVA);
        problem1.getSats("/Users/vishal/Documents/dataAndStructure/java/src/main/java/com/deadbody/learning/cleartax/Problem1.java");
    }

    public void getSats(String fileName) throws IOException {
        File file = new File(fileName);
        FileReader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            totalNumberOfLines++;
            checkIfInsideMultiLineBlock(line);
            if (isLineOfCode(line)) lineOfCode++;
            if (isBlankLine(line)) blankLine++;
            if (isSingleLineComment(line)) singleLineComment++;
            if (insideMultiLineBlock) multiLineComment++;
        }

        System.out.println("totalNumberOfLines " + totalNumberOfLines);
        System.out.println("lineOfCode " + lineOfCode);
        System.out.println("blankLine " + blankLine);
        System.out.println("singleLineComment " + singleLineComment);
        System.out.println("multiLineComment " + multiLineComment);
    }

    private boolean isLineOfCode(String line) {
        String newLine = line.trim();
        String conf = ConfigInfo.getConf(language, Type.MULTI_LINE_START);
        if (conf == null)
            return false;
        return newLine.length() > 0 && !isSingleLineComment(newLine) && !newLine.startsWith(conf);
    }

    private boolean isSingleLineComment(String line) {
        String conf = ConfigInfo.getConf(language, Type.SINGLE_LINE_COMMENT);
        if (conf == null)
            return false;
        return line.trim().startsWith(conf);
    }

    private boolean isBlankLine(String line) {
        return line.equals(ConfigInfo.getConf(language, Type.BLANK_LINE));
    }

    private void checkIfInsideMultiLineBlock(String line) {
        String conf = ConfigInfo.getConf(language, Type.MULTI_LINE_START);
        if (conf == null)
            return;
        if (!insideMultiLineBlock && line.trim().startsWith(conf)) {
            insideMultiLineBlock = true;
        }
        conf = ConfigInfo.getConf(language, Type.MULTI_LINE_END);
        if (conf == null)
            return;
        if (insideMultiLineBlock && line.contains(conf)) {
            insideMultiLineBlock = false;
            multiLineComment++;
        }

    }
}

class JavaConfig {
    public static final String MULTI_LINE_START = "/*";
    public static final String MULTI_LINE_END = "*/";
    public static final String SINGLE_LINE_COMMENT = "//";
}

class Config {
    public static final String BLANK_LINE = "";
}

class ConfigInfo {
    public static String getConf(Language language, Type type) {
        if (language == Language.JAVA) {
            switch (type) {
                case BLANK_LINE:
                    return Config.BLANK_LINE;
                case MULTI_LINE_END:
                    return JavaConfig.MULTI_LINE_END;
                case MULTI_LINE_START:
                    return JavaConfig.MULTI_LINE_START;
                case SINGLE_LINE_COMMENT:
                    return JavaConfig.SINGLE_LINE_COMMENT;
                default:
                    return null;
            }
        }
        return null;
    }
}
