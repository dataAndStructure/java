package com.deadbody.learning.leetcode;

import java.util.Arrays;

public class MaximumNumberOfCoinsYouCanGet {
    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        reverseArray(piles);
        int i = 1;
        int j = piles.length - 1;
        int res = 0;
        while (i < j) {
            res += piles[i];
            i = i + 2;
            j--;
        }
        return res;
    }

    private void reverseArray(int[] piles) {
        int i = 0;
        int j = piles.length - 1;

        while (i < j) {
            int temp = piles[i];
            piles[i] = piles[j];
            piles[j] = temp;
            i++;
            j--;
        }
    }
}
