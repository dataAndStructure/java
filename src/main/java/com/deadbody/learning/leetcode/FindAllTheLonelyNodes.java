package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class FindAllTheLonelyNodes {
    public List<Integer> getLonelyNodes(TreeNode root) {
        ArrayList<Integer> integers = new ArrayList<>();
        if (root == null) {
            return integers;
        }

        if ((root.left != null && root.right == null)) {
            integers.add(root.left.val);
        } else if ((root.left == null && root.right != null)) {
            integers.add(root.right.val);
        }

        List<Integer> right = getLonelyNodes(root.right);
        integers.addAll(right);
        List<Integer> left = getLonelyNodes(root.left);
        integers.addAll(left);

        return integers;
    }
}
