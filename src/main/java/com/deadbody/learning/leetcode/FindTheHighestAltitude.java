package com.deadbody.learning.leetcode;

/**
 * Example 1:
 * Input: gain = [-5,1,5,0,-7]
 * Output: 1
 * Explanation: The altitudes are [0,-5,-4,1,1,-6]. The highest is 1.
 * ===========================================================================
 * Example 2:
 * Input: gain = [-4,-3,-2,-1,4,3,2]
 * Output: 0
 * Explanation: The altitudes are [0,-4,-7,-9,-10,-6,-3,-1]. The highest is 0.
 */
public class FindTheHighestAltitude {
    public int largestAltitude(int[] gain) {
        int n = gain.length;
        int altitude = Math.max(gain[0], 0);
        for (int i = 1; i < n; i++) {
            gain[i] += gain[i - 1];
            altitude = Math.max(altitude, gain[i]);
        }
        return altitude;
    }
}
