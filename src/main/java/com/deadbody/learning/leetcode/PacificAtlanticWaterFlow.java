package com.deadbody.learning.leetcode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PacificAtlanticWaterFlow {
    public List<List<Integer>> pacificAtlantic(int[][] heights) {
        int r = heights.length;
        int c = heights[0].length;

        boolean[][] visitedPacific = new boolean[r][c];
        Queue<Point> queue = new LinkedList<>();
        for (int i = 0; i < r; i++) {
            queue.add(new Point(i, 0));
        }
        for (int i = 0; i < c; i++) {
            queue.add(new Point(0, i));
        }
        visitIsland(visitedPacific, queue, heights);
        boolean[][] visitedAtlantic = new boolean[r][c];
        queue.clear();
        for (int i = 0; i < r; i++) {
            queue.add(new Point(i, c - 1));
        }
        for (int i = 0; i < c; i++) {
            queue.add(new Point(r - 1, i));
        }
        visitIsland(visitedAtlantic, queue, heights);
        List<List<Integer>> ans = new LinkedList<>();
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (visitedAtlantic[i][j] && visitedPacific[i][j]) {
                    ans.add(List.of(i, j));
                }
            }
        }
        return ans;
    }

    private void visitIsland(boolean[][] visited, Queue<Point> queue, int[][] heights) {
        int r = heights.length;
        int c = heights[0].length;
        int[][] iterators = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        while (!queue.isEmpty()) {
            Point point = queue.poll();
            visited[point.r()][point.c()] = true;
            for (int[] iterator : iterators) {
                Point newPoint = new Point(point.r() + iterator[0], point.c() + iterator[1]);
                if (newPoint.r() < 0 || newPoint.r() >= r || newPoint.c() < 0 || newPoint.c() >= c) continue;
                if (visited[newPoint.r()][newPoint.c()]) continue;
                if (heights[newPoint.r()][newPoint.c()] >= heights[point.r()][point.c()]) queue.add(newPoint);
            }
        }
    }
}

record Point(int r, int c) {
}