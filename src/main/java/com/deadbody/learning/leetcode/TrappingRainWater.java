package com.deadbody.learning.leetcode;

public class TrappingRainWater {

    public int trap(int[] height) {
        int leftMax = 0;
        int left = 0;
        int rightMax = 0;
        int right = height.length - 1;
        int ans = 0;

        while (left < right) {
            if (height[left] < height[right]) {
                if (leftMax < height[left]) {
                    leftMax = height[left];
                } else {
                    ans += leftMax - height[left];
                }
                left++;
            } else {
                if (rightMax < height[right]) {
                    rightMax = height[right];
                } else {
                    ans += rightMax - height[right];
                }
                right--;
            }
        }
        return ans;
    }

    public int trapPrefixSuffix(int[] height) {
        if (height.length < 3)
            return 0;

        int[] prefix = new int[height.length];
        int[] suffix = new int[height.length];

        prefix[0] = height[0];
        for (int i = 1; i < height.length; i++)
            prefix[i] = Math.max(prefix[i - 1], height[i]);

        suffix[height.length - 1] = height[height.length - 1];
        for (int i = height.length - 2; i >= 0; i--)
            suffix[i] = Math.max(suffix[i + 1], height[i]);

        int ans = 0;
        for (int i = 0; i < height.length; i++) {
            ans += Math.min(prefix[i], suffix[i]) - height[i];
        }
        return ans;
    }

    public int trapBruteForce(int[] height) {

        if (height.length < 3)
            return 0;

        int ans = 0;
        for (int i = 1; i < height.length - 1; i++) {
            int leftMax = findMax(height, 0, i);
            int rightMax = findMax(height, i, height.length - 1);
            ans += Math.min(leftMax, rightMax) - height[i];
        }

        return ans;
    }

    private int findMax(int[] height, int start, int end) {
        int max = 0;
        for (int i = start; i <= end; i++) {
            max = Math.max(max, height[i]);
        }
        return max;
    }
}
