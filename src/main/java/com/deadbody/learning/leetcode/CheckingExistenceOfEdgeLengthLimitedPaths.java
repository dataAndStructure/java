package com.deadbody.learning.leetcode;

import java.util.*;

public class CheckingExistenceOfEdgeLengthLimitedPaths {

    // place till we have find the parents
    int current = 0;

    public boolean[] distanceLimitedPathsExist(int n, int[][] edgeList, int[][] queries) {
        // sorting edges on the basis of weight
        Arrays.sort(edgeList, Comparator.comparingInt(a -> a[2]));

        // defining new queries on the basis of weight
        int[][] qs = new int[queries.length][4];
        for (int i = 0; i < queries.length; i++) {
            qs[i] = new int[]{queries[i][0], queries[i][1], queries[i][2], i};
        }
        Arrays.sort(qs, Comparator.comparingInt(a -> a[2]));

        //answer array
        boolean[] answer = new boolean[queries.length];

        //parent init for union find
        int[] parent = new int[n];
        for (int i = 0; i < n; i++)
            parent[i] = i;

        //find solution to all the queries
        for (int[] q : qs) {
            answer[q[3]] = helper(edgeList, q[0], q[1], q[2], parent);
        }
        return answer;
    }

    private boolean helper(int[][] edgeList, int start, int end, int limit, int[] parent) {
        for (int i = current; i < edgeList.length; i++) {
            // Memoize the result for all smaller weights
            if (edgeList[i][2] >= limit) {
                current = i;
                break;
            }
            int root1 = findAndUnion(parent, edgeList[i][0]);
            int root2 = findAndUnion(parent, edgeList[i][1]);
            if (root1 != root2) {
                parent[root1] = root2;
            }
        }
        return findAndUnion(parent, start) == findAndUnion(parent, end);
    }

    private int findAndUnion(int[] parent, int i) {
        int j = i;
        // Find
        while (parent[i] != i) {
            i = parent[i];
        }
        //union
        parent[j] = i;
        return i;
    }

    public boolean[] distanceLimitedPathsExistOld(int n, int[][] edgeList, int[][] queries) {
        boolean[] answer = new boolean[queries.length];
        Set<Integer> isVisited = new HashSet<>();

        //Graph Init
        Map<Integer, Integer>[] graph = new HashMap[n];
        for (int i = 0; i < n; i++) {
            graph[i] = new HashMap<>();
        }

        //Add Edge
        for (int[] edge : edgeList) {
            if (graph[edge[0]].containsKey(edge[1])) {
                if (edge[2] < graph[edge[0]].get(edge[1])) {
                    graph[edge[0]].put(edge[1], edge[2]);
                }
            } else
                graph[edge[0]].put(edge[1], edge[2]);
            if (graph[edge[1]].containsKey(edge[0])) {
                if (edge[2] < graph[edge[1]].get(edge[0])) {
                    graph[edge[1]].put(edge[0], edge[2]);
                }
            } else
                graph[edge[1]].put(edge[0], edge[2]);
        }

        Queue<Integer> queue = new LinkedList<>();

        //resolve queries
        for (int i = 0; i < queries.length; i++) {
            int[] query = queries[i];
            if (i == 3)
                System.out.println(i);
            answer[i] = dfs(query[0], query[1], query[2], graph, isVisited, queue);
            //answer[i] = dfs(query[0], query[1], query[2], graph, isVisited);
            isVisited.clear();
            queue.clear();
        }

        return answer;
    }

    private boolean dfs(int start, int end, int weight, Map<Integer, Integer>[] graph, Set<Integer> isVisited, Queue<Integer> queue) {
        queue.add(start);
        while (!queue.isEmpty()) {
            int value = queue.remove();
            if (value == end)
                return true;
            if (!isVisited.contains(value)) {
                isVisited.add(value);
                for (Map.Entry<Integer, Integer> w : graph[value].entrySet()) {
                    if (w.getValue() < weight)
                        queue.add(w.getKey());
                }
            }
        }
        return false;
    }
}