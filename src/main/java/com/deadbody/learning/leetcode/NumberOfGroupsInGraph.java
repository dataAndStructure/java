package com.deadbody.learning.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class NumberOfGroupsInGraph {
    public int solution(int[][] isConnected) {
        int group = 0;
        boolean[] isVisited = new boolean[isConnected.length];
        Queue<Integer> queue = new LinkedList<>();
        for (int currentNode = 0; currentNode < isConnected.length; currentNode++) {
            if(isVisited[currentNode])
                continue;
            isVisited[currentNode]=true;
            group++;
            for (int i = 0; i < isConnected.length; i++) {
                if (currentNode == i || isVisited[i])
                    continue;
                if (isConnected[currentNode][i] == 1) {
                    queue.add(i);
                    isVisited[i] = true;
                }
                while (!queue.isEmpty()) {
                    int num = queue.remove();
                    for (int j = 0; j < isConnected.length; j++) {
                        if (num == j || isVisited[j])
                            continue;
                        if (isConnected[num][j] == 1) {
                            queue.add(j);
                            isVisited[j] = true;
                        }
                    }
                }
            }
        }
        return group;
    }
}
