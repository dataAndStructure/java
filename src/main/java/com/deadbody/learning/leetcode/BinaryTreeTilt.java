package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.TreeNode;

public class BinaryTreeTilt {
    public int findTilt(TreeNode root) {
        return findTiltRecursive(root).sum;
    }

    private NodeMetadata findTiltRecursive(TreeNode root) {
        if (root == null)
            return new NodeMetadata(0, 0);

        NodeMetadata left = findTiltRecursive(root.left);
        NodeMetadata right = findTiltRecursive(root.right);

        return new NodeMetadata(Math.abs(left.childrenSum - right.childrenSum) + left.sum + right.sum,
                root.val + left.childrenSum + right.childrenSum);
    }

    private record NodeMetadata(int sum, int childrenSum) {
    }
}
