package com.deadbody.learning.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EqualRowAndColumnPairs {
    public int equalPairs(int[][] grid) {
        int r = grid.length;
        int c = grid[0].length;

        Map<List<Integer>, Integer> map = new HashMap<>();

        List<List<Integer>> lists = new ArrayList<>();
        for (int i = 0; i < r; i++) {
            List<Integer> list = new ArrayList<>();
            lists.add(list);
        }
        for (int[] ints : grid) {
            List<Integer> list = new ArrayList<>();
            for (int j = 0; j < c; j++) {
                list.add(ints[j]);
                lists.get(j).add(ints[j]);
            }
            map.put(list, map.getOrDefault(list, 0) + 1);
        }
        int result = 0;
        for (List<Integer> list : lists) {
            result += map.getOrDefault(list, 0);
        }
        return result;
    }
}
