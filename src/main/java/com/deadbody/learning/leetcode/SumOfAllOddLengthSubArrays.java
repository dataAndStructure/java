package com.deadbody.learning.leetcode;

public class SumOfAllOddLengthSubArrays {
    public int sumOddLengthSubarrays(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            arr[i] += arr[i - 1];
        }
        int result = 0;
        for (int i = 0; i < n; i += 2) {
            for (int j = 0; j + i < n; j++) {
                int e = j + i;
                result += (j == 0) ? arr[e] : arr[e] - arr[j - 1];
            }
        }
        return result;
    }
}
