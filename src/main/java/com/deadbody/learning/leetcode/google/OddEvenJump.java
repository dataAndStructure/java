package com.deadbody.learning.leetcode.google;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class OddEvenJump {
    public int oddEvenJumps(int[] arr) {
        int counter = 0;
        boolean[] lower = new boolean[arr.length];
        boolean[] higher = new boolean[arr.length];
        lower[arr.length - 1] = true;
        higher[arr.length - 1] = true;

        //Map to store int followed by its index
        NavigableMap<Integer, Integer> map = new TreeMap<>();
        map.put(arr[arr.length - 1], arr.length - 1);
        for (int i = arr.length - 2; i >= 0; i--) {
            Map.Entry<Integer, Integer> high = map.ceilingEntry(arr[i]);
            Map.Entry<Integer, Integer> low = map.floorEntry(arr[i]);
        }
        return counter;
    }
}
