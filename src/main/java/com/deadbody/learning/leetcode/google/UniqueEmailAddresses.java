package com.deadbody.learning.leetcode.google;

import java.util.HashSet;
import java.util.Set;

public class UniqueEmailAddresses {
    public int numUniqueEmails(String[] emails) {
        Set<String> uniqueEmail = new HashSet<>();
        for (String email : emails) {
            String firstHalf = parseFirstHalf(email.split("@")[0]);
            String secondHalf = parseSecondHalf(email.split("@")[1]);
            uniqueEmail.add(firstHalf + "@" + secondHalf);
        }
        return uniqueEmail.size();
    }

    private String parseSecondHalf(String s) {
        return s;
    }

    private String parseFirstHalf(String firstHalf) {
        String beforePlus = firstHalf.split("\\+")[0];
        return beforePlus.replaceAll("\\.", "");
    }
}
