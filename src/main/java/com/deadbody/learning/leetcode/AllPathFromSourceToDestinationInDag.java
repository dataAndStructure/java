package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.Graph;

import java.util.List;

public class AllPathFromSourceToDestinationInDag {
    public static void main(String[] args) {
        int[][] graph = {{1, 2}, {3}, {3}, {}};
        Graph g = new Graph(graph.length);
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                g.addEdge(i, graph[i][j]);
            }
        }
        List<List<Integer>> allPaths = g.getAllPaths(0, graph.length - 1);
        for (List<Integer> path : allPaths) {
            for (Integer element : path) {
                System.out.println(element);
            }
            System.out.println("=========================");
        }


        int[][] graph2 = {{0, 1}, {2, 1}, {3, 1}, {1, 4}, {2, 4}};
        g = new Graph(5);
        for (int i = 0; i < graph.length; i++) {
            g.addEdge(graph2[i][0], graph2[i][1]);
        }

        List<Integer> vertices = g.verticesFromWhereAllNodesAreReachable();
        for (Integer element : vertices) {
            System.out.println(element);
        }
        System.out.println("=========================");
    }
}
