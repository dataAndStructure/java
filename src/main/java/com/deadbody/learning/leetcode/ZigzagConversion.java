package com.deadbody.learning.leetcode;

public class ZigzagConversion {
    public String convert(String s, int numRows) {
        if (s == null || s.equals("") || numRows <= 0) {
            return "";
        }
        if (numRows == 1)
            return s;

        int maxCharPerBox = maxCount(numRows);
        int maxBox = (s.length() / maxCharPerBox + 1) * numRows;
        char[][] mat = new char[numRows][maxBox];

        loops(s, numRows, mat);

        return computeReturnValue(mat).toString();
    }

    private StringBuilder computeReturnValue(char[][] mat) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char[] chars : mat) {
            for (int y = 0; y < mat[0].length; y++) {
                if (chars[y] == Character.MIN_VALUE)
                    continue;
                stringBuilder.append(chars[y]);
            }
        }
        return stringBuilder;
    }

    private void loops(String s, int numRows, char[][] mat) {
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < s.length()) {
            for (; k < numRows && i < s.length(); k++) {
                mat[k][j] = s.charAt(i);
                i++;
            }

            k -= 2;
            j++;
            while (k >= 0 && i < s.length()) {
                mat[k][j] = s.charAt(i);
                i++;
                k--;
                j++;
            }
            k += 2;
            j--;
        }
    }

    private int maxCount(int numRows) {
        if(numRows==2)
            return 4;
        return 2 * numRows - 1;
    }
}
