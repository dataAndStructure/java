package com.deadbody.learning.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWithoutRepeatingCharacters {
    public int lengthOfLongestSubstring(String s) {
        if(s==null || s.length()==0)
            return 0;
        if(s.length()==1)
            return 1;
        Map<Character, Integer> chars = new HashMap<>();
        int max = 1;
        int start = 0;
        for(int i=0;i<s.length();i++){
            if(chars.containsKey(s.charAt(i))){
                start = Math.max(start, chars.get(s.charAt(i)) + 1);
            }
            chars.put(s.charAt(i), i);
            max = Math.max(max, i-start+1);
        }
        return max;
    }
}
