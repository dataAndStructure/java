package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.ListNode;

public class AddTwoNumberLinkedList {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode retVal = null;
        while (l1 != null && l2 != null) {
            int val = l1.val + l2.val + carry;
            if (10 == val) {
                val = 0;
                carry = 1;
            } else {
                carry = val / 10;
                val = val % 10;
            }
            if (retVal == null) {
                retVal = new ListNode(val);
            } else {
                ListNode n = retVal;
                while (n.next != null) {
                    n = n.next;
                }
                n.next = new ListNode(val);
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        while (l1 != null) {
            int val = l1.val + carry;
            if (val == 10) {
                val = 0;
                carry = 1;
            } else {
                carry = val / 10;
                val = val % 10;
            }
            if (retVal == null) {
                retVal = new ListNode(val);
            } else {
                ListNode n = retVal;
                while (n.next != null) {
                    n = n.next;
                }
                n.next = new ListNode(val);
            }
            l1 = l1.next;
        }
        while (l2 != null) {
            int val = l2.val + carry;
            if (val == 10) {
                val = 0;
                carry = 1;
            } else {
                carry = val / 10;
                val = val % 10;
            }
            if (retVal == null) {
                retVal = new ListNode(val);
            } else {
                ListNode n = retVal;
                while (n.next != null) {
                    n = n.next;
                }
                n.next = new ListNode(val);
            }
            l2 = l2.next;
        }
        if (carry > 0) {
            ListNode n = retVal;
            while (n.next != null) {
                n = n.next;
            }
            n.next = new ListNode(carry);
        }
        return retVal;
    }
}
