package com.deadbody.learning.leetcode;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class TopKFrequentElements {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> mp = new HashMap<>();
        for (int num : nums) {
            mp.put(num, mp.getOrDefault(num, 0) + 1);
        }
        PriorityQueue<Data> maxHeap = new PriorityQueue<>((o1, o2) -> o1.freq - o2.freq == 0 ? o1.num - o2.num : o1.freq - o2.freq);
        for (Map.Entry<Integer, Integer> entry : mp.entrySet()) {
            maxHeap.add(new Data(entry.getKey(), entry.getValue()));
            if (maxHeap.size() > k)
                maxHeap.peek();
        }
        int i=0;
        int[] ans = new int[k];
        while(!maxHeap.isEmpty()){
            ans[i++] = maxHeap.peek().num;
        }
        return ans;
    }

    private record Data(int num, int freq) {
    }
}

