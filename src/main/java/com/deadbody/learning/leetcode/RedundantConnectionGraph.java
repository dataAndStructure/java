package com.deadbody.learning.leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RedundantConnectionGraph {
    public int[] findRedundantConnection(int[][] edges) {
        Set<Integer> isVisited = new HashSet<>();
        List<Integer>[] graph = new ArrayList[1001];
        for (int i = 0; i < 1001; i++) {
            graph[i] = new ArrayList<>();
        }
        for (int[] edge : edges) {
            if (!graph[edge[0]].isEmpty() && !graph[edge[1]].isEmpty() && dfs(isVisited, graph, edge[0], edge[1]))
                return edge;
            graph[edge[0]].add(edge[1]);
            graph[edge[1]].add(edge[0]);
            isVisited.clear();
        }
        return new int[2];
    }

    private boolean dfs(Set<Integer> isVisited, List<Integer>[] graph, int start, int end) {
        if (!isVisited.contains(start)) {
            isVisited.add(start);
            if(start==end)
                return true;
            for(int node: graph[start])
                if(dfs(isVisited,graph,node,end))
                    return true;
        }
        return false;
    }
}
