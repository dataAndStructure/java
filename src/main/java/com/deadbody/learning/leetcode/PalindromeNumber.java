package com.deadbody.learning.leetcode;

public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if (x < 0)
            return false;
        if (x < 10)
            return true;

        int revert = 0;
        int num = x;

        while (num > 0) {
            int d = num % 10;
            num = num / 10;
            revert = revert * 10 + d;
        }

        return x == revert;
    }
}
