package com.deadbody.learning.leetcode;

import java.util.PriorityQueue;

public class MinimizeRoundingErrorToMeetTarget {
    public String minimizeError(String[] prices, int target) {
        PriorityQueue<Double> minHeap = new PriorityQueue<>();
        double sum = 0;
        for (String price : prices) {
            double p = Double.parseDouble(price);
            double floor = Math.floor(p);
            double ceil = Math.ceil(p);
            if (floor != ceil) {
                // (ceil - p) - (p - floor)
                minHeap.add(ceil - 2 * p + floor);
            }
            target -= (int) floor;
            sum += p - floor;
            if (target < 0) {
                return "-1";
            }
        }
        if (target > minHeap.size()) return "-1";
        while (target-- > 0 && !minHeap.isEmpty()) {
            sum += minHeap.poll();
        }
        return String.format("%.3f", sum);
    }
}
