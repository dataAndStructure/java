package com.deadbody.learning.leetcode;

public class NextGreaterElementIII {
    public int nextGreaterElement(int n) {
        char[] in = Integer.toString(n).toCharArray();
        int i = in.length - 2;

        while (i >= 0 && in[i] >= in[i + 1]) {
            i--;
        }
        if (i == -1) {
            return -1;
        }

        int k = in.length - 1;
        while (in[k] <= in[i]) {
            k--;
        }

        swap(in, i, k);

        StringBuilder ans = new StringBuilder();
        for (int j = 0; j <= i; j++) {
            ans.append(in[j]);
        }
        for (int j = in.length - 1; j > i; j--) {
            ans.append(in[j]);
        }

        return Long.parseLong(ans.toString()) > Integer.MAX_VALUE ? -1 : Integer.parseInt(ans.toString());

    }

    private void swap(char[] in, int i, int k) {
        char c = in[i];
        in[i] = in[k];
        in[k] = c;
    }
}
