package com.deadbody.learning.leetcode;

public class MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] num1, int[] num2) {
        if (num1 == null) {
            num1 = new int[0];
        }
        if (num2 == null) {
            num2 = new int[0];
        }
        int[] merged = new int[num1.length + num2.length];

        int i = 0;
        int j = 0;
        int k = 0;

        while (i < num1.length && j < num2.length) {
            if (num1[i] < num2[j])
                merged[k++] = num1[i++];
            else
                merged[k++] = num2[j++];
        }
        while (i < num1.length) {
            merged[k++] = num1[i++];
        }
        while (j < num2.length) {
            merged[k++] = num2[j++];
        }

        return k == 0 ? 0 : k % 2 == 0 ?  ((double) (merged[k / 2] + merged[k / 2 - 1]) / 2d) : (double) merged[k / 2];
    }
}
