package com.deadbody.learning.leetcode.utils;

import java.util.Objects;

public class WeightedNode<E> {
    public E data;
    public int weight;

    public WeightedNode(E data, int weight) {
        this.data = data;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeightedNode)) return false;
        WeightedNode<?> that = (WeightedNode<?>) o;
        return weight == that.weight && data.equals(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, weight);
    }
}
