package com.deadbody.learning.leetcode.utils;

import java.util.*;

public class Graph {
    // no of vertices
    int v;
    //adjList
    List<Integer>[] adjList;

    public Graph(int vertices) {
        this.v = vertices;
        initAdjList();
    }

    private void initAdjList() {
        adjList = new ArrayList[v];
        for (int i = 0; i < v; i++) {
            adjList[i] = new ArrayList<>();
        }
    }

    //Store all path from u to v
    public void addEdge(int u, int v) {
        adjList[u].add(v);
    }

    //print all path from s to d
    public List<List<Integer>> getAllPaths(int s, int d) {
        boolean[] isVisited = new boolean[v];
        List<Integer> pathList = new ArrayList<>();
        List<List<Integer>> returnVal = new ArrayList<>();

        // add source to path[]
        pathList.add(s);

        // Call recursive utility
        printAllPathsUtil(s, d, isVisited, pathList, returnVal);

        return returnVal;
    }

    // recursive Call function for all paths
    // A recursive function to print
    // all paths from 'u' to 'd'.
    // isVisited[] keeps track of
    // vertices in current path.
    // localPathList<> stores actual
    // vertices in the current path
    private void printAllPathsUtil(int u, int d,
                                   boolean[] isVisited,
                                   List<Integer> localPathList, List<List<Integer>> returnVal) {

        if (u == d) {
            final List<Integer> l = new ArrayList<>(localPathList);
            returnVal.add(l);
        }

        // Mark the current node
        isVisited[u] = true;

        // Recur for all the vertices
        // adjacent to current vertex
        for (int i : adjList[u]) {
            if (!isVisited[i]) {
                // store current node
                // in path[]
                localPathList.add(i);
                printAllPathsUtil(i, d, isVisited,
                        localPathList, returnVal);

                // remove current node
                // in path[]
                localPathList.remove((Integer) i);
            }
        }

        // Mark the current node
        isVisited[u] = false;
    }

    public List<Integer> verticesFromWhereAllNodesAreReachable() {
        Set<Integer> reachableVertices = new HashSet<>();
        for (int i = 0; i < v; i++) {
            reachableVertices.add(i);
        }
        for (int i = 0; i < v; i++) {
            boolean[] isVisited = new boolean[v];
            Queue<Integer> queue = new LinkedList<>(adjList[i]);
            while (!queue.isEmpty()) {
                int element = queue.remove();
                if(!isVisited[element]){
                    if(reachableVertices.contains(element)){
                        reachableVertices.remove(element);
                    }
                    isVisited[element] = true;
                }
            }
        }
        return new ArrayList<>(reachableVertices);
    }
}
