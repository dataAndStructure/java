package com.deadbody.learning.leetcode;

public class LongestPalindromicSubstring {

    private static final String BOGUS_CHAR = "|";

    public String longestPalindromeDp(String s) {
        return "";
    }

    public String longestPalindromeManachersAlgorithm(String s) {
        // O(n)
        String input = addBogusChar(s);
        int[] palindromeRadii = new int[input.length()];

        int center = 0;
        int radius = 0;

        // O(n) - bound by length of string so can't run more than n
        while (center < input.length()) {

            while (center - (radius + 1) >= 0 && center + (radius + 1) < input.length() && input.charAt(center - (radius + 1)) == input.charAt(center + (radius + 1))) {
                radius++;
            }

            palindromeRadii[center] = radius;

            int oldCenter = center;
            int oldRadius = radius;
            center++;
            radius = 0;

            while (center <= oldCenter + oldRadius) {
                int mirroredCenter = oldCenter - (center - oldCenter);
                int maxMirroredRadius = oldCenter + oldRadius - center;
                if (palindromeRadii[mirroredCenter] < maxMirroredRadius) {
                    palindromeRadii[center] = palindromeRadii[mirroredCenter];
                    center++;
                } else if (palindromeRadii[mirroredCenter] > maxMirroredRadius) {
                    palindromeRadii[center] = maxMirroredRadius;
                    center++;
                } else {
                    radius = maxMirroredRadius;
                    break;
                }
            }
        }

        return getLongestPalindrome(palindromeRadii, input);
    }

    private String getLongestPalindrome(int[] palindromeRadii, String input) {
        int radius = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < palindromeRadii.length; i++) {
            if (radius < palindromeRadii[i]) {
                radius = palindromeRadii[i];
                index = i;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input.charAt(index));
        int indexR = index;
        int indexL = index;
        while (radius > 0) {
            stringBuilder.insert(0, input.charAt(--indexL));
            stringBuilder.append(input.charAt(++indexR));
            radius--;
        }
        return stringBuilder.toString().replace("|","");
    }

    private String addBogusChar(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(BOGUS_CHAR);
        for (char c : s.toCharArray()) {
            stringBuilder.append(c).append(BOGUS_CHAR);
        }
        return stringBuilder.toString();
    }
}
