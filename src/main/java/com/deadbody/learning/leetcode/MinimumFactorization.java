package com.deadbody.learning.leetcode;

public class MinimumFactorization {
    public int smallestFactorization(int a) {
        if (a < 2)
            return a;

        long res = 0;
        long mul = 1;
        for (long i = 9; i >= 2; i--) {
            while (a % i == 0) {
                a /= i;
                res = i * mul + res;
                mul *= 10;
            }
        }
        return a < 2 && res <= Integer.MAX_VALUE ? (int) res : 0;
    }
}
