package com.deadbody.learning.leetcode;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElementII {
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        Stack<Integer> stack = new Stack<>();
        int[] ans = new int[n];
        Arrays.fill(ans, -1);
        for (int i = n * 2 - 1; i >= 0; i--) {
            while (!stack.empty() && stack.peek() <= nums[i % n]) {
                stack.pop();
            }
            if (i < n) {
                ans[i] = stack.empty() ? -1 : stack.peek();
            }
            stack.push(nums[i % n]);
        }
        return ans;
    }
}
