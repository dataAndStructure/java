package com.deadbody.learning.leetcode;

public class StringToInteger {
    public int myAtoi(String s) {
        s = s.trim();

        int length = s.length();

        if (length == 0)
            return 0;

        int start = 0;
        int multiplier = 1;

        if (s.charAt(0) == '-') {
            multiplier = -1;
            start = 1;
        } else if (s.charAt(0) == '+') {
            start = 1;
        }

        int index = start;
        int res = 0;
        int maxLimit = Integer.MAX_VALUE / 10;
        while (index < length) {
            if (!(s.charAt(index) >= '0' && s.charAt(index) <= '9')) {
                return res * multiplier;
            }

            int digit = s.charAt(index) - '0';
            if (res > maxLimit || (res == maxLimit && digit > 7)) {
                return multiplier == -1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }

            res = res * 10 + digit;
            index++;
        }
        return res * multiplier;
    }
}
