package com.deadbody.learning.leetcode;

import com.deadbody.learning.leetcode.utils.TreeNode;

public class AddOneRowToTree {
    public TreeNode addOneRow(TreeNode root, int val, int depth) {
        if(depth==1){
            TreeNode node = new TreeNode(val);
            node.left=root;
            return node;
        }
        if (depth > 1) {
            addNode(root, val, depth, 1);
        }
        return root;
    }

    private void addNode(TreeNode root, int val, int depth, int currentDepth) {
        if (root == null)
            return;
        if (depth == currentDepth + 1) {
            TreeNode leftNode = new TreeNode(val);
            leftNode.left = root.left;
            leftNode.right = null;
            TreeNode rightNode = new TreeNode(val);
            rightNode.left = null;
            rightNode.right = root.right;
            root.left = leftNode;
            root.right = rightNode;
            return;
        }
        addNode(root.left, val, depth, currentDepth + 1);
        addNode(root.right, val, depth, currentDepth + 1);
    }
}
