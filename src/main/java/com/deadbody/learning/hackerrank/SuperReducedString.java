package com.deadbody.learning.hackerrank;


/**
 * Given a string, recursively remove adjacent duplicate characters from string.
 * The output string should not have any adjacent duplicates
 */
public class SuperReducedString {

    public String superReducedString(String s){
        for(int i=0;i<s.length();i++){
            if(i>0 && s.charAt(i)==s.charAt(i-1)){
                if(i==s.length()-1){
                    if(i-2<0)
                        s="";
                    else
                        s=s.substring(0,i-1);
                }else{
                    s = s.substring(0,i-1) + s.substring(i+1,s.length());
                }
                i= i-2 < s.length()-1 ? i-2: s.length()-1;
            }else if(i+1<s.length() && s.charAt(i)==s.charAt(i+1)){
                if(i==0 && s.length()==2)
                    s="";
                else if(i==0)
                    s=s.substring(i+2,s.length());
                else
                    s=s.substring(0,i)+s.substring(i+2,s.length());
                i= i-1 < s.length()-1 ? i-1: s.length()-1;
            }
        }
        return s.length()==0?"Empty String":s;
    }

}
