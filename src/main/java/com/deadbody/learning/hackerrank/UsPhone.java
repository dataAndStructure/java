package com.deadbody.learning.hackerrank;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsPhone {
    public String find_phone_number(String text) {
        String retVal = "NONE";
        Pattern p = Pattern.compile("\\(\\d{3}\\) \\d{3}-\\d{4}|\\d{3}-\\d{3}-\\d{4}");
        Matcher m = p.matcher(text);

        if(m.find()){
            retVal = m.group();
        }

        return retVal;
    }
}
