package com.deadbody.learning.hackerrank;

import java.util.HashSet;
import java.util.Set;

public class WeightedUniformStrings {

    private static int getWeight(char ch){
        return (int)ch-(int)'a'+1;
    }

    public static String[] weightedUniformStrings(String s, int[] queries) {

        char[] arr=s.toCharArray();

        Set<Integer> values = new HashSet<>();
        char ch='$';
        int weight=0;

        for(char c:arr){
            if(ch==c){
                weight+=getWeight(c);
            }else{
                weight=getWeight(c);
                ch=c;
            }
            if(!values.contains(weight))
                values.add(weight);
        }

        String[] retVal = new String[queries.length];
        for(int i=0;i<queries.length;i++){
            if(values.contains(queries[i])){
                retVal[i]="YES";
            }else{
                retVal[i]="NO";
            }
        }
        return retVal;
    }
}
