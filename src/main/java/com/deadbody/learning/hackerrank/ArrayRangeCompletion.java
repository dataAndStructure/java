package com.deadbody.learning.hackerrank;

public class ArrayRangeCompletion {
    public String solution(int[] array) {
        int[] diff = new int[array.length];

        for(int i=1;i<array.length;i++){
            diff[i-1]=array[i]-array[i-1];
        }
        int rangeFrom=Integer.MAX_VALUE;
        int rangeTo=Integer.MIN_VALUE;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<diff.length-2;i++){
            if(diff[i]==diff[i+1]){
                if(rangeFrom>i){
                    rangeFrom=i;
                }
                rangeTo=i+2;
            }else{
                if(rangeTo-rangeFrom>=2){
                    sb.append(array[rangeFrom]).append("-").append(array[rangeTo]).append(",");
                    i=rangeTo;
                }else{
                    for(int j=rangeFrom;j<=rangeTo;j++){
                        sb.append(array[j]).append(",");
                    }
                    i=rangeTo;
                }
                if(i>rangeTo){
                    sb.append(array[i]).append(",");
                }
            }
        }
        if(rangeTo-rangeFrom>=2){
            sb.append(array[rangeFrom]).append("-").append(array[rangeTo]).append(",");
        }else{
            for(int j=rangeFrom;j<=rangeTo;j++){
                sb.append(array[j]).append(",");
            }
        }
        if(diff[diff.length-2]!=diff[diff.length-3]){
            sb.append(array[array.length-1]).append(",");
        }
        return sb.toString().substring(0,sb.toString().length()-1);
    }

}
