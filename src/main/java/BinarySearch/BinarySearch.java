package BinarySearch;

import java.security.InvalidParameterException;

public class BinarySearch {

    public boolean exists(int[] array, int k) {
        if (array == null)
            throw new InvalidParameterException("Array is null");
        if (array.length == 0)
            throw new InvalidParameterException("Array is Empty");

        if (k < array[0] || k > array[array.length - 1])
            return false;

        return search(array, k, 0, array.length - 1);
    }

    private boolean search(int[] array, int k, int start, int end) {
        int mid = (start + end) / 2;
        if (start > end)
            return false;
        if (array[mid] == k)
            return true;
        else if (array[mid] > k)
            return search(array, k, start, mid - 1);
        else
            return search(array, k, mid + 1, end);
    }

    public static void main(String[] args) {
        BinarySearch binerySearch = new BinarySearch();
        System.out.println(binerySearch.exists(new int[]{1,2,3,4,5,6,7,8,9}, 11));
    }
}
